﻿<div class="page-sub-content">
	<div class="jumbotron">
		<div class="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
			<div class="col-lg-24">
				<div class="row">
					<div class="col-md-18">
						<div class="inner">
							<ol class="breadcrumb">
								<li id="idHome" class="breadcrumb-item"><a href="javascript:void(0)"><i class="fa fa-home"></i> Home</a></li>
								<li class="breadcrumb-item active">Usuarios</li>
							</ol>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid container-fixed-lg form-horizontal">
		<div class="col-lg-24">
			<div class="row">
				<div class="card card-default">
					<div class="card-body">
						<div class="col-sm-24">
							<div class="form-group">
								<jtse-grid id="grd_MU_Usuario"></jtse-grid>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>