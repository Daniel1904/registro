﻿<div class="page-content">
	<div class="panel" style="margin-bottom: 0px">
		<header class="panel-heading">
			<h3 class="panel-title">Gestor de Personas</h3>
			<ol class="breadcrumb">
				<li id="divHome"><a href="javascript:void(0)"><i class="fa fa-dashboard"></i> Home</a></li>
				<li id="divApoyo"><a href="javascript:void(0)"><i class="fa fa-slideshare"></i> Apoyo</a></li>
				<li class="active">Gestor de personas</li>
			</ol>
		</header>
		<div class="panel-body form-horizontal">
			<div class="col-sm-24">
				<jtse-grid id="grd_MP_ejemplo"></jtse-grid>
			</div>
		</div>
	</div>
</div>