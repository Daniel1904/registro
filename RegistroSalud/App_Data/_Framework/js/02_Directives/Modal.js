﻿var $$modal = function (controlName) {
	var ctrl = document.getElementById(controlName);
	var $create = function (o) {
		if (ctrl != undefined) {
			var withId = o.WithId || false;
			var content = o.Content || '';
			var c = [];
			c.push('<div id="');
			c.push(controlName);
			c.push('_Modal" class="modal fade" data-scroll="');
			if (o.WithScrollY) c.push('1');
			else c.push('0');
			c.push('"><div id="');
			c.push(controlName);
			c.push('_ModalDialog" class="modal-dialog modal-');
			c.push(o.Width || 'lg');
			c.push('"')
			if (o.Style != undefined) {
				c.push(' style="');
				c.push(o.Style);
				c.push('"');
			}
			c.push('><div id="');
			c.push(controlName);
			c.push('_Content" class="modal-content"><div id="');
			c.push(controlName);
			c.push('_Header" class="modal-header modal-header-area-bc" style="');
			c.push((o.StyleHeader != undefined && o.StyleHeader != '') ? o.StyleHeader : '');
			c.push('">');
			c.push('<button id="');
			c.push(controlName);
			c.push('_btnClose" ');
			if (o.FnClose && o.FnClose != '') {
				c.push('onclick="$jt[\'');
				c.push(o.Namespace);
				c.push('\'].');
				c.push(o.FnClose);
				if (content == '') {
					c.push('()"');
				}
				else {
					c.push('"');
				}
			}
			else {
				c.push('onclick="$$modal(\'');
				c.push(controlName);
				c.push('\').hide();"');
			}
			c.push(' class="close"><span>×</span></button>');
			c.push('<h4 class="modal-title modal-header-text-c">');
			c.push(o.Title || '');
			c.push('</h4></div><div class="modal-body">');
			if (content == '') {
				c.push(ctrl.innerHTML);
			}
			c.push('</div>');
			if (o.ButtonNames && o.ButtonClass && o.FnEvents && o.Namespace) {
				c.push('<div class="modal-footer modal-footer-area-bc">');
				for (var i = 0; i < o.ButtonNames.length; i++) {
					if (withId) {
						c.push('<button id="');
						c.push(o.ButtonNames[i] || '');
						c.push('" class="btn ');
					}
					else {
						c.push('<button class="btn ');
					}
					c.push(o.ButtonClass[i]);
					c.push('" onclick="$jt[\'');
					c.push(o.Namespace);
					c.push('\'].');
					c.push(o.FnEvents[i]);
					if (content == '') {
						c.push('()">');
					}
					else {
						c.push('">');
					}
					if (!withId) {
						c.push(o.ButtonNames[i]);
					}
					c.push('</button>');
				}
				c.push('</div>');
			}
			//if (o.ButtonClose) {
			//	c.push('<button onclick="$$Modal(\'');
			//	c.push(controlName);
			//	c.push('\').hide();" class="btn btn-default">Close</button>');
			//}
			c.push('</div></div></div>');
			c.push('<div class="modal-backdrop fade"></div>');
			ctrl.innerHTML = c.join('');
			ctrl.classList.add('show');
			if (content == '') {
				$hide();
			}
			else {
				ctrl.children[0].children[0].children[0].children[1].innerHTML = content;
				$show();
			}
			var mh = document.getElementById(controlName + '_Header');
			mh.draggable = true;
			mh.ondragstart = function (event) {
				dragStart(event);
			};
			mh.ondragover = function (event) {
				event.preventDefault();
			};
			var m = document.getElementById(controlName).children[0];
			m.ondragover = function (event) {
				event.preventDefault();
			};
			m.ondrop = function (event) {
				dragDrop(event);
			};
		}
	};
	var dragStart = function (event) {
		var mc = event.target.parentElement.parentElement;
		var ancho = getComputedStyle(mc, null).getPropertyValue("left");
		var alto = getComputedStyle(mc, null).getPropertyValue("top");
		var a = Math.floor(ancho.replace("px", ""));
		var b = Math.floor(alto.replace("px", ""));
		var x = (event.clientX > a ? event.clientX - a : a - event.clientX);
		var y = (event.clientY > b ? event.clientY - b : b - event.clientY);
		var punto = x + "," + y;
		event.dataTransfer.setData("text", punto);
	};
	var dragDrop = function (event) {
		event.preventDefault();
		var x1 = event.clientX;
		var y1 = event.clientY;
		var puntoInicial = event.dataTransfer.getData("text");
		var punto = puntoInicial.split(",");
		var x2 = punto[0] * 1;
		var y2 = punto[1] * 1;
		var mc = event.target.children[0];
		mc.style.left = (x1 - x2) + "px";
		mc.style.top = (y1 - y2) + "px";
	};
	var $show = function (e) {
		if (ctrl != undefined) {
			var error = ctrl.getElementsByClassName("has-error");
			for (var i = 0; i < error.length; i++) {
				error[i].classList.remove("has-error");
				i = i - 1;
			};
			ctrl.style.display = '';
			ctrl = ctrl.children[0];
			ctrl.classList.add('show');
			ctrl.nextElementSibling.classList.add('show');
			setTimeout(function () {
				ctrl.className = 'modal show fade in' + (ctrl.getAttribute('data-scroll') == '1' ? ' modal-scroll' : '');
			}, 300);
			setTimeout(function () {
				ctrl.nextElementSibling.className = 'modal-backdrop show fade in';
			}, 150);
		}
	};
	var $hide = function () {
		if (ctrl != undefined) {
			ctrl = ctrl.children[0];
			ctrl.className = 'modal show fade' + (ctrl.getAttribute('data-scroll') == '1' ? ' modal-scroll' : '');
			ctrl.nextElementSibling.className = 'modal-backdrop show fade';
			setTimeout(function () {
				ctrl.classList.remove('show');
			}, 150);
			setTimeout(function () {
				ctrl.nextElementSibling.classList.remove('show');
			}, 300);
		}
	};
	var $center = function () {
		var modals = document.getElementsByClassName('modal-dialog');
		var cModals = modals.length;
		for (var i = 0; i < cModals; i++) {
			modals[i].style.top = '0px';
			modals[i].style.left = '0px';
		}
	};
	var $setTitle = function (title) {
		ctrl.getElementsByClassName('modal-title')[0].innerHTML = title;
    };
    var $btnClassname = function (posicion) {
        if (posicion == 0) {
            ctrl.getElementsByClassName('btn')[0].classList.remove("hide");
        }
        if (posicion == 1) {
            ctrl.getElementsByClassName('btn')[1].classList.remove("hide");
        }
        if (posicion == 2) {
            ctrl.getElementsByClassName('btn')[2].classList.remove("hide");
        }
        if (posicion == 3) {
            ctrl.getElementsByClassName('btn')[0].classList.add("hide");
        }
        if (posicion == 4) {
            ctrl.getElementsByClassName('btn')[1].classList.add("hide");
        }
        if (posicion == 5) {
            ctrl.getElementsByClassName('btn')[2].classList.add("hide");
        }
    }
	return {
		create: function (d) {
			$create(d);
		},
		show: function () {
			$show();
		},
		hide: function () {
			$hide();
		},
		setTitle: function (t) {
			$setTitle(t);
		},
		center: function () {
			$center();
        },
        btnClassname: function (posicion) {
            $btnClassname(posicion);
        }
	};
};