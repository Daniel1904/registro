﻿var $$grid = function (controlName) {
	var _msgError = {
		m1: 'Control not found',
		m2: 'Header not found or should not be zero',
		m3: 'Namespace not found',
		m4: 'Function not defined'
	};
	var ctrl = document.getElementById(controlName);
	if (ctrl == undefined) {
		console.log(_msgError.m1);
		return;
	}
	var sfx = 'jtse-grid_' + controlName;
	var _ = {};
	if ($jt[sfx]) _ = $jt[sfx];
	var $create = function (o) {
		if (o && (o.headers == undefined || o.headers.length == 0)) {
			console.log(_msgError.m2);
			return;
		}
		if (o.namespace == undefined) {
			console.log(_msgError.m3);
			return;
		}
		var c = [],
			cHeaders = 0,
			eIndex = false,
			eWidths = false,
			eFilter = {
				up: false,
				down: false
			};
		_.namespace = o.namespace || '';
		_.title = o.title || '';
		_.headers = [];
		_.eProperties = false;
		_.properties = [];
		_.typesData = [];
		_.typesFilter = [];
		_.sortHeader = [];
		_.showColumns = [];
		_.showColumnsExport = [];
		_.widths = [];
		_.eWidthPx = false;
		_.widthsPx = [];
		_.scroll = (o.scroll == undefined ? false : true);
		_.scrollHeight = o.scrollHeight || 200;
		_.indexs = [];
		_.data = o.data || [];
		_.matrix = [];
		_.class = o.class || '';
		_.headerStyle = o.headerStyle || '';
		_.rowStyle = o.rowStyle || '';
		_.columnStyle = o.columnStyle || '';
		_.fnRowStyle = o.fnRowStyle || '';
		_.headerClass = o.headerClass || '';
		_.rowClass = o.rowClass || '';
		//_.columnClass = o.columnClass || '';
		_.fnRowEvent = o.fnRowEvent || '';
		_.borderTop = (o.borderTop == undefined ? false : o.borderTop) == true ? true : false;
		_.BtnNewAlterName = (o.btnNew == undefined ? false : o.btnNew) == true ? true : false;
		_.btnNew = (o.btnNew == undefined ? false : o.btnNew) == true ? true : false;
		_.btnEdit = (o.btnEdit == undefined ? false : o.btnEdit) == true ? true : false;
		_.btnDelete = (o.btnDelete == undefined ? false : o.btnDelete) == true ? true : false;
		_.btnDeleteMultiple = (o.btnDeleteMultiple == undefined ? false : o.btnDeleteMultiple) == true ? true : false;
		_.btnRefresh = (o.btnRefresh == undefined ? false : o.btnRefresh) == true ? true : false;
		_.btnExportExcel = (o.btnExportExcel == undefined ? false : o.btnExportExcel) == true ? true : false;
		_.btnExportText = (o.btnExportText == undefined ? false : o.btnExportText) == true ? true : false;
		_.fnBtnNewAlterName = o.fnBtnNewAlterName || '';
		_.fnBtnNew = o.fnBtnNew || '';
		_.fnBtnEdit = o.fnBtnEdit || '';
		_.fnBtnDelete = o.fnBtnDelete || '';
		_.fnBtnDeleteMultiple = o.fnBtnDeleteMultiple || '';
		_.fnBtnRefresh = o.fnBtnRefresh || '';
		_.fnBtnExportExcel = o.fnBtnExportExcel || '';
		_.fnBtnExportText = o.fnBtnExportText || '';
		_.btnOptions = {
			names: [],
			class: [],
			icons: [],
			fnEvents: []
		};
		_.fnExtensions = o.fnExtensions;
		_.exportName = o.exportName || '';
		_.sort = (o.sort == undefined ? true : o.sort) == true ? true : false;
		_.pagination = (o.pagination == undefined ? true : o.pagination) == true ? true : false;
		_.filterPosition = o.filterPosition == '' ? '' : (o.filterPosition || 'UP');
		_.generalFilter = (o.generalFilter == undefined ? true : o.generalFilter) == true ? true : false;
		_.indexCurrentPage = 0;
		_.indexCurrentRange = 0;
		_.indexOrder = 0;
		_.entriesPage = o.entriesPage || 10;
		_.rangePage = o.rangePage || 10;
		_.separator = o.separator || '¦';
		_.language = findLanguage(o.language || '');
		cHeaders = o.headers.length;
		if (o.headers != undefined) {
			_.headers = o.headers.slice();
		}
		if (o.widths != undefined) {
			_.widths = o.widths.slice();
			eWidths = true;
		}
		if (o.widthsPx != undefined) {
			_.widthsPx = o.widthsPx.slice();
			_.eWidthsPx = true;
		}
		if (o.sortHeader != undefined) {
			_.sortHeader = o.sortHeader.slice();
		}
		else {
			for (var i = 0; i < cHeaders; i++) {
				_.sortHeader[i] = true;
			}
		}
		if (o.properties != undefined) {
			_.properties = o.properties.slice();
			_.eProperties = true;
		}
		if (o.indexs != undefined) {
			_.indexs = o.indexs.slice();
			eIndex = true;
		}
		else {
			for (var i = 0; i < cHeaders; i++) {
				_.indexs[i] = i;
			}
		}
		if (o.showColumns != undefined) {
			_.showColumns = o.showColumns.slice();
		}
		else {
			for (var i = 0; i < cHeaders; i++) {
				_.showColumns[i] = true;
			}
		}
		if (o.showColumnsExport != undefined) {
			_.showColumnsExport = o.showColumnsExport.slice();
		}
		else {
			for (var i = 0; i < cHeaders; i++) {
				_.showColumnsExport[i] = true;
			}
		}
		if (o.typesData != undefined) {
			_.typesData = o.typesData.slice();
		}
		else {
			for (var i = 0; i < cHeaders; i++) {
				_.typesData[i] = 'S';
			}
		}
		if (o.typesFilter != undefined) {
			_.typesFilter = o.typesFilter.slice();
		}
		else {
			for (var i = 0; i < cHeaders; i++) {
				_.typesFilter[i] = 'I';
			}
		}
		if (o.btnOptions != undefined) {
			_.btnOptions = o.btnOptions;
		}
		eFilter = setFilterPosition(_.filterPosition);
		$jt[sfx] = _;
		c.push('<div class="col-24 without-padding">');
		if (_.borderTop) {
			c.push('<hr />');
		}
		if (_.title != '') {
			c.push('<div class="col-24">');
			c.push('<h3 id="title_');
			c.push(sfx);
			c.push('" class="grid-title w-100">');
			c.push(_.title);
			c.push('</h3>');
			c.push('</div>');
		}
		c.push('<div class="col-24 without-padding">');
		if (_.btnNew || _.btnDeleteMultiple || _.btnRefresh || _.btnExportExcel || _.btnExportText || (_.btnOptions.names.length > 0 && _.fnOptions.fnEvents.length > 0)) {
			c.push('<div class="col-sm-12">');
			c.push('<div class="grid-bottom">');
			if (_.btnNew && _.fnBtnNew != '') {
				c.push('<button class="btn btn-grid-new" onclick="$jt[\'');
				c.push(_.namespace);
				c.push('\'][\'');
				c.push(_.fnBtnNew);
				c.push('\'](this);">');
				c.push('<i class="fa fa-plus"></i>&nbsp;');
				if (_.BtnNewAlterName && _.fnBtnNewAlterName != '') {
					c.push(_.fnBtnNewAlterName);
				} else {
					c.push('Nuevo');
				}
				c.push('</button>&nbsp;&nbsp;');
			}
			if (_.btnDeleteMultiple && _.fnBtnDeleteMultiple != '') {
				c.push('<button class="btn btn-grid-delete-multiple" onclick="$jt[\'');
				c.push(_.namespace);
				c.push('\'][\'');
				c.push(_.fnBtnDeleteMultiple);
				c.push('\'](this);">');
				c.push('<i class="fa fa-trash-o"></i>&nbsp;');
				c.push('Eliminar');
				c.push('</button>&nbsp;&nbsp;');
			}
			if (_.btnRefresh && _.fnBtnRefresh != '') {
				c.push('<button class="btn btn-grid-refresh" onclick="$jt[\'');
				c.push(_.namespace);
				c.push('\'][\'');
				c.push(_.fnBtnRefresh);
				c.push('\'](this);">');
				c.push('<i class="fa fa-refresh"></i>&nbsp;');
				c.push('Refrescar');
				c.push('</button>&nbsp;&nbsp;');
			}
			if (_.btnExportExcel) {
				c.push('<button class="btn btn-grid-export-excel" onclick="');
				if (_.fnBtnExportExcel != '') {
					c.push('$jt[\'');
					c.push(_.namespace);
					c.push('\'][\'');
					c.push(_.fnBtnExportExcel);
					c.push('\'](this);');
				} else {
					c.push('$$grid(\'');
					c.push(controlName);
					c.push('\').exportTextExcel(1);');
				}
				c.push('">');
				c.push('<i class="fa fa-file-excel-o"></i>&nbsp;');
				c.push('Excel');
				c.push('</button>&nbsp;&nbsp;');
			}
			if (_.btnExportText) {
				c.push('<button class="btn btn-grid-export-text" onclick="');
				if (_.fnBtnExportExcel != '') {
					c.push('$jt[\'');
					c.push(_.namespace);
					c.push('\'][\'');
					c.push(_.fnBtnExportText);
					c.push('\'](this);');
				} else {
					c.push('$$grid(\'');
					c.push(controlName);
					c.push('\').exportTextExcel(0);');
				}
				c.push('">');
				c.push('<i class="fa fa-file-text-o"></i>&nbsp;');
				c.push('Texto');
				c.push('</button>&nbsp;&nbsp;');
			}
			if (_.btnOptions.names.length > 0 && _.btnOptions.fnEvents.length > 0) {
				for (var i = 0; i < _.btnOptions.names.length; i++) {
					c.push('<button class="btn ');
					c.push(_.btnOptions.class[i]);
					c.push('" onclick="$jt[\'');
					c.push(_.namespace);
					c.push('\'][\'');
					c.push(_.btnOptions.fnEvents[i]);
					c.push('\'](this);">');
					c.push('<i class="fa ');
					c.push(_.btnOptions.icons[i]);
					c.push('"></i>&nbsp;');
					c.push(_.btnOptions.names[i]);
					c.push('</button>');
					if (i < _.btnOptions.names.length - 1) {
						c.push('&nbsp;&nbsp;');
					}
				}
			}
			c.push('</div>');
			c.push('</div>');
		}
		if (_.generalFilter) {
			c.push('<div class="col-sm-12">');
			c.push('<div class="grid-bottom">');
			c.push('<input id="generalFilter_');
			c.push(sfx);
			c.push('" type="text" class="form-control" placeholder="');
			c.push(_.language.search);
			c.push('">');
			c.push('</div>');
			c.push('</div>');
		}
		c.push('</div>');
		c.push('<div class="col-24">');
		c.push('<div class="grid-bottom">');
		c.push('<div class="scroll-x w-100 ');
		c.push(_.class);
		c.push('">');
		if (_.scroll) {
			c.push('<div class="grid">');
			c.push('<div class="grid-scroll grid-scroll-header">');
			c.push('<div style="box-sizing: content-box;padding-right: 17px;width:');
			var cWidthsPx = 0;
			for (var i = 0; i < cHeaders; i++) {
				cWidthsPx += _.widthsPx[i];
			}
			c.push(cWidthsPx.toString());
			c.push('px;">');
			c.push('<table class="grid-scrollbar" cellspacing="0" style="margin-left:0px;width:');
			c.push(cWidthsPx.toString());
			c.push('px;">');
			c.push('<thead>');
			c.push('<tr>');
			var htmlHeader = [];
			for (var i = 0; i < cHeaders; i++) {
				if (_.btnEdit && i == 0) {
					htmlHeader.push('<th style="width:15px;');
					if (_.headerStyle != '') {
						htmlHeader.push(_.headerStyle);
					}
					htmlHeader.push('"></th>');
				}
				if (_.showColumns[i]) {
					htmlHeader.push('<th class="');
					if (_.headerClass != '') {
						htmlHeader.push(_.headerClass);
					}
					if (_.sortHeader[i] == true) {
						htmlHeader.push(' sorting');
					}
					if (_.eWidthsPx) {
						htmlHeader.push(' word-ellipsis');
					}
					htmlHeader.push('" name="');
					if (_.sortHeader[i] == true) {
						htmlHeader.push('sort_');
						htmlHeader.push(sfx);
					}
					htmlHeader.push('"');
					if (eIndex) {
						htmlHeader.push(' data-order="');
						htmlHeader.push(_.indexs[i]);
						htmlHeader.push('"');
					}
					htmlHeader.push(' style="width:');
					htmlHeader.push(_.widthsPx[i]);
					htmlHeader.push('px;');
					if (_.headerStyle != '') {
						htmlHeader.push(_.headerStyle);
					}
					htmlHeader.push('">');
					htmlHeader.push(_.headers[i]);
					htmlHeader.push('</th>');
				}
				if (_.btnDelete && i == cHeaders - 1) {
					htmlHeader.push('<th style="width:15px;');
					if (_.headerStyle != '') {
						htmlHeader.push(_.headerStyle);
					}
					htmlHeader.push('"></th>');
				}
			}
			c.push(htmlHeader.join(''));
			c.push('</tr>');
			c.push('</thead>');
			c.push('</table>');
			c.push('</div>');
			c.push('</div>');
			c.push('<div class="grid-scroll grid-scroll-body" style="width:');
			c.push((cWidthsPx + 17).toString());
			c.push('px;">');
			c.push('<table class="grid-scrollbar" cellspacing="0">');
			c.push('<thead>');
			c.push('<tr style="height:0px;visibility:hidden;">');
			c.push(htmlHeader.join(''));
			c.push('</tr>');
			c.push('</thead>');
			c.push('<tbody id="tbody_');
			c.push(sfx);
			c.push('"><tr><td class="text-center" colspan="');
			c.push(cHeaders + (_.btnEdit ? 1 : 0) + (_.btnDelete ? 1 : 0));
			c.push('">');
			c.push(_.language.noDataFound);
			c.push('</td></tr></tbody>');
			c.push('</table>');
			c.push('</div>');
			c.push('</div>');
		}
		else {
			c.push('<table class="grid');
			if (!_.eWidthsPx) {
				c.push(' w-100');
			}
			else {
				c.push(' table-layout');
			}
			c.push('" style="');
			if (_.eWidthsPx) {
				c.push('width:');
				var cont = 0;
				for (var i = 0; i < cHeaders; i++) {
					cont += _.widthsPx[i];
				}
				c.push(cont.toString());
				c.push('px');
			}
			c.push('"><thead><tr>');
			for (var i = 0; i < cHeaders; i++) {
				if (_.btnEdit && i == 0) {
					c.push('<th style="width:2.5%;');
					if (_.headerStyle != '') {
						c.push(_.headerStyle);
					}
					c.push('"></th>');
				}
				if (_.showColumns[i]) {
					c.push('<th class="');
					if (_.headerClass != '') {
						c.push(_.headerClass);
					}
					if (_.sortHeader[i] == true) {
						c.push(' sorting');
					}
					if (_.eWidthsPx) {
						c.push(' word-ellipsis');
					}
					c.push('" name="');
					if (_.sortHeader[i] == true) {
						c.push('sort_');
						c.push(sfx);
					}
					c.push('"');
					if (eIndex) {
						c.push(' data-order="');
						c.push(_.indexs[i]);
						c.push('"');
					}
					c.push(' style="');
					if (eWidths) {
						c.push('width:');
						c.push(_.widths[i]);
						c.push('%;white-space:nowrap;');
					}
					else if (_.eWidthsPx) {
						c.push('width:');
						c.push(_.widthsPx[i]);
						c.push('px;');
					}
					else {
						c.push('width:auto;');
					}
					if (_.headerStyle != '') {
						c.push(_.headerStyle);
					}
					c.push('">');
					c.push(_.headers[i]);
					c.push('</th>');
				}
				if (_.btnDelete && i == cHeaders - 1) {
					c.push('<th style="width:2.5%;');
					if (_.headerStyle != '') {
						c.push(_.headerStyle);
					}
					c.push('"></th>');
				}
			}
			c.push('</tr></thead>');
			if (!eFilter.up && !eFilter.down) {
				c.push(createFilter(cHeaders, true, true));
			}
			if (eFilter.up) {
				c.push(createFilter(cHeaders, eFilter.up));
			}
			c.push('<tbody id="tbody_');
			c.push(sfx);
			c.push('"><tr><td class="text-center" colspan="');
			c.push(cHeaders + (_.btnEdit ? 1 : 0) + (_.btnDelete ? 1 : 0));
			c.push('">');
			c.push(_.language.noDataFound);
			c.push('</td></tr></tbody>');
			if (eFilter.down) {
				c.push(createFilter(cHeaders, eFilter.up));
			}
			c.push('</table>');
		}
		c.push('</div>');
		c.push('</div>');
		c.push('</div>');
		if (_.pagination) {
			c.push('<div class="col-24 without-padding">');
			c.push('<div class="pagination w-100">');
			c.push('<div class="col-12">')
			c.push('<label style="margin-right:4px;display:inline-block;" class="control-label">');
			c.push(_.language.show);
			c.push('</label>');
			c.push('<select id="cboEntriesShow_');
			c.push(sfx);
			c.push('" style="width: auto;display: inline-block;margin-right: 4px;" class="form-control">');
			var c10 = '<option value="10">10</option>',
				c25 = '<option value="25">25</option>',
				c50 = '<option value="50">50</option>',
				c100 = '<option value="100">100</option>';
			var ep = _.entriesPage;
			if (ep != 10 && ep != 25 && ep != 50 && ep != 100) {
				if (ep < 10) {
					c.push('<option value="');
					c.push(ep);
					c.push('" selected>');
					c.push(ep);
					c.push('</option>');
					c.push(c10);
					c.push(c25);
					c.push(c50);
					c.push(c100);
				} else if (ep < 25) {
					c.push(c10);
					c.push('<option value="');
					c.push(ep);
					c.push('" selected>');
					c.push(ep);
					c.push('</option>');
					c.push(c25);
					c.push(c50);
					c.push(c100);
				} else if (ep < 50) {
					c.push(c10);
					c.push(c25);
					c.push('<option value="');
					c.push(ep);
					c.push('" selected>');
					c.push(ep);
					c.push('</option>');
					c.push(c50);
					c.push(c100);
				} else if (ep < 100) {
					c.push(c10);
					c.push(c25);
					c.push(c50);
					c.push('<option value="');
					c.push(ep);
					c.push('" selected>');
					c.push(ep);
					c.push('</option>');
					c.push(c100);
				} else {
					c.push(c10);
					c.push(c25);
					c.push(c50);
					c.push(c100);
					c.push('<option value="');
					c.push(ep);
					c.push('" selected>');
					c.push(ep);
					c.push('</option>');
				}
			}
			else {
				if (ep == 10) {
					c.push('<option value="10" selected>10</option>');
					c.push(c25);
					c.push(c50);
					c.push(c100);
				}
				if (ep == 25) {
					c.push(c10);
					c.push('<option value="25" selected>25</option>');
					c.push(c50);
					c.push(c100);
				}
				if (ep == 50) {
					c.push(c10);
					c.push(c25);
					c.push('<option value="50" selected>50</option>');
					c.push(c100);
				}
				if (ep == 100) {
					c.push(c10);
					c.push(c25);
					c.push(c50);
					c.push('<option value="100" selected>100</option>');
				}
			}
			c.push('</select>');
			c.push('<label class="control-label" style="display: inline-block;">');
			c.push(_.language.showing);
			c.push(' <span id="currentPage_');
			c.push(sfx);
			c.push('">0</span>&nbsp;');
			c.push(_.language.to);
			c.push('&nbsp;<span id="totalPages_');
			c.push(sfx);
			c.push('">0</span>&nbsp;');
			c.push(_.language.of);
			c.push('&nbsp;<span id="totalEntries_');
			c.push(sfx);
			c.push('">0</span>&nbsp;');
			c.push(_.language.entries);
			c.push('</label>');
			c.push('</div>');
			c.push('<div class="text-right col-12"><div id="pagination_');
			c.push(sfx);
			c.push('" class="pagination-pages"></div></div></div></div>');

		} else {
			c.push('<div class="col-24"><div class="pagination-details">');
			c.push('<span>');
			c.push(_.language.showing);
			c.push(' <span id="currentPage_');
			c.push(sfx);
			c.push('" class="hide"></span>');
			c.push('<span id="totalPages_');
			c.push(sfx);
			c.push('" class="hide"></span>');
			c.push('<span id="totalEntries_');
			c.push(sfx);
			c.push('"></span>&nbsp;');
			c.push(_.language.entries);
			c.push('</span></div></div>');
		}
		c.push('</div>');
		ctrl.innerHTML = c.join('');
		ctrl.classList.add('show');
		orderSetup();
		if (_.filterPosition != '' || _.generalFilter) filterSetup();
		if (_.pagination) {
			document.getElementById('cboEntriesShow_' + sfx).onchange = function () {
				$jt[sfx].entriesPage = this.value * 1;
				$showGrid(0);
			};
		}
		if (_.data && _.data.length > 0) {
			createMatrix(_.data);
			$showGrid(0);
		}
	};
	var $setData = function (d) {
		if (d) {
			if ($jt[sfx].indexCurrentPage == 0) {
				$jt[sfx].indexCurrentPage = 0;
				$jt[sfx].indexCurrentRange = 0;
				$jt[sfx].indexOrder = 0;
			}
			$jt[sfx].matrix = [];
			createMatrix(d);
			filterThread();
			$showGrid($jt[sfx].indexCurrentPage);
		}
	};
	var $clearGrid = function () {
		$jt[sfx].indexCurrentPage = 0;
		$jt[sfx].indexCurrentRange = 0;
		$jt[sfx].indexOrder = 0;
		$jt[sfx].matrix = [];
		$showGrid(0);
	};
	var findLanguage = function (obj) {
		if (window[obj] != undefined && obj != '')
			return window[obj];
		else
			return {
				add: 'Agregar',
				export: 'Exportar',
				show: 'Mostrar',
				search: 'Busqueda...',
				noDataFound: 'No hay registros...',
				showing: 'Mostrando',
				to: 'hasta',
				of: 'de',
				entries: 'registros'
			};
	};
	var createFilter = function (nReg, filterUp, hide) {
		var c = [];
		c.push('<');
		c.push(filterUp ? 'thead' : 'tbody');
		if (hide) {
			c.push(' class="hide"');
		}
		c.push('><tr>');
		for (var i = 0; i < nReg; i++) {
			if (_.btnEdit && i == 0) c.push('<td></td>');
			if (_.showColumns[i]) {
				c.push('<td>');
				switch (_.typesFilter[i]) {
					case 'S':
						c.push('<select id="');
						c.push('cbo_');
						c.push(sfx);
						c.push('_');
						c.push(i);
						c.push('"');
						c.push(' name="filter_');
						c.push(sfx);
						c.push('" class="form-control"></select>');
						break;
					case 'I':
						c.push('<input name="filter_');
						c.push(sfx);
						c.push('" type="text" class="form-control lupa" />');
						break;
					default:
						c.push('<input name="filter_');
						c.push(sfx);
						c.push('" style="display:none" />');
						break;
				}
				c.push('</td>');
			}
			if (_.btnDelete && i == nReg - 1) c.push('<td></td>');
		}
		c.push('</tr></');
		c.push(filterUp ? 'thead' : 'tbody');
		c.push('>');
		return c.join('');
	};
	var orderSetup = function () {
		var links = document.getElementsByName("sort_" + sfx);
		var cLinks = links.length;
		var link;
		for (var i = 0; i < cLinks; i++) {
			link = links[i];
			link.onclick = function () {
				sortMatrix(this);
				$showGrid($jt[sfx].indexCurrentPage);
			}
		}
	};
	var order = function (x, y) {
		var indexOrder = $jt[sfx].indexOrder;
		var valX = (isNaN(x[indexOrder]) ? x[indexOrder].toLowerCase() : x[indexOrder]);
		var valY = (isNaN(y[indexOrder]) ? y[indexOrder].toLowerCase() : y[indexOrder]);
		return (($jt[sfx].orderType == 0 ? valX > valY : valX < valY) ? 1 : -1);
	};
	var clearLinks = function () {
		var links = document.getElementsByName("sort_" + sfx);
		var cLinks = links.length;
		var link;
		for (var i = 0; i < cLinks; i++) {
			link = links[i];
			link.classList.add('sorting');
			link.classList.remove('sorting_desc');
			link.classList.remove('sorting_asc');
		}
	};
	var sortMatrix = function (link) {
		$jt[sfx].indexOrder = link.getAttribute("data-order") * 1;
		var field = link.className;
		var posAsc = field.indexOf("sorting_asc");
		var posDesc = field.indexOf("sorting_desc");
		$jt[sfx].orderType = (posAsc == -1 && posDesc == -1 ? 0 : (posAsc > -1 ? 1 : 0));
		clearLinks();
		if ($jt[sfx].orderType == 0) {
			link.classList.add('sorting_asc');
			link.classList.remove('sorting_desc');
			link.classList.remove('sorting');
		}
		else {
			link.classList.add('sorting_desc');
			link.classList.remove('sorting_asc');
			link.classList.remove('sorting');
		}
		$jt[sfx].matrix.sort(order);
	};
	var filterSetup = function () {
		var filters = document.getElementsByName("filter_" + sfx);
		var cFilters = filters.length;
		for (var i = 0; i < cFilters; i++) {
			switch (filters[i].tagName) {
				case 'INPUT':
					filters[i].onkeyup = filter;
					filters[i].onfocus = getFocus;
					filters[i].onblur = lostFocus;
					break;
				case 'SELECT':
					filters[i].onchange = filter;
					break;
			}
		}
		var search = document.getElementById('generalFilter_' + sfx);
		if (search) { search.onkeyup = filter; }
	};
	var getFocus = function (e) {
		this.classList.remove("search-ico");
	};
	var lostFocus = function (e) {
		this.classList.add("search-ico");
	};
	var filter = function (e) {
		clearTimeout($jt[sfx].threads);
		$jt[sfx].threads = setTimeout(function () { filterThread(e); }, 1);
	};
	var filterThread = function (e) {
		var filters = document.getElementsByName("filter_" + sfx);
		var cFilters = filters.length;
		var vFilters = [], exist, eTypesData = false, typesData = [], arr = [];
		$jt[sfx].matrix = [];
		var cEntries = _.data.length;
		var indexs = _.indexs.slice();
		var cFields, fields, field, x = 0, vSearch = '', existSearch;
		var optionDate = { "year": "numeric", "month": "2-digit", "day": "2-digit" };
		var optionDateTime = { "year": "numeric", "month": "2-digit", "day": "2-digit", "hour": "2-digit", "minute": "2-digit", "second": "2-digit" };
		if (_.typesData.length > 0) {
			eTypesData = true;
			typesData = _.typesData.slice();
		}
		var search = document.getElementById('generalFilter_' + sfx);
		if (search != undefined && search.value != '') {
			if (!$config.isIE) {
				vSearch = search.value.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase().trim();
			} {
				vSearch = $frQuitarAcentos(search.value).toLowerCase().trim();
			}
			for (var j = 0; j < cFilters; j++) {
				filters[j].className = 'form-control has-error';
				filters[j].disabled = true;
				if (!$config.isIE)
					vFilters.push({ value: filters[j].value.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase().trim(), type: (filters[j].tagName == 'INPUT' ? 0 : 1) });
				else
					vFilters.push({ value: $frQuitarAcentos(filters[j].value).toLowerCase().trim(), type: (filters[j].tagName == 'INPUT' ? 0 : 1) });
			}
		}
		else {
			for (var j = 0; j < cFilters; j++) {
				filters[j].className = 'form-control ' + (filters[j].value != "" ? "" : "lupa");
				filters[j].disabled = false;
				if (!$config.isIE)
					vFilters.push({ value: filters[j].value.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase().trim(), type: (filters[j].tagName == 'INPUT' ? 0 : 1) });
				else
					vFilters.push({ value: $frQuitarAcentos(filters[j].value).toLowerCase().trim(), type: (filters[j].tagName == 'INPUT' ? 0 : 1) });
			}
		}
		for (var i = 0; i < cEntries; i++) {
			fields = _.data[i];
			cFields = fields.length;
			exist = false;
			existSearch = true;
			if (vSearch != '') {
				for (var j = 0; j < cFilters; j++) {
					filter = vFilters[j];
					if (filter.type == 0 || filter.type == 1) {
						if (eTypesData) {
							switch (typesData[j]) {
								case 'D':
									field = (new Date(fields[indexs[j]])).toLocaleString('es-PE', optionDate);
									break;
								case 'E':
								case 'E_L':
								case 'E_R':
									field = extension($jt[sfx].fnExtensions[j], fields, i);
									break;
								default:
									if (!$config.isIE)
										field = fields[indexs[j]].toString().normalize('NFD').replace(/[\u0300-\u036f]/g, "");
									else
										field = $frQuitarAcentos(fields[indexs[j]].toString());
									break;
							}
						}
						else field = fields[indexs[j]].toString();
						existSearch = (field.toLowerCase().trim().indexOf(vSearch) > -1);
						if (existSearch) {
							exist = true;
							break;
						}
					}
				}
			} else if (!exist) {
				for (var j = 0; j < cFilters; j++) {
					exist = true;
					filter = vFilters[j];
					if (filter.type == 0) {
						if (eTypesData) {
							switch (typesData[j]) {
								case 'D':
									field = (new Date(fields[indexs[j]])).toLocaleString('es-PE', optionDate);
									break;
								default:
									if (!$config.isIE)
										field = fields[indexs[j]].toString().normalize('NFD').replace(/[\u0300-\u036f]/g, "");
									else
										field = $frQuitarAcentos(fields[indexs[j]].toString());
									break;
							}
						}
						else field = fields[indexs[j]].toString();
						exist = exist && (field.toLowerCase().trim().indexOf(filter.value) > -1);
					}
					else exist = exist && (filter.value == "" || fields[indexs[j]].toLowerCase().trim() == filter.value.toLowerCase().trim());
					if (!exist) break;
				}
			}
			if (exist && existSearch) {
				arr[x] = [];
				arr[x] = fields.slice();
				x++;
			}
		}
		$jt[sfx].matrix = arr.slice();
		if (e) {
			$jt[sfx].indexCurrentPage = 0;
			$jt[sfx].indexCurrentRange = 0;
		}
		pagination($jt[sfx].indexCurrentPage);
	};
	var createMatrix = function (data) {
		$jt[sfx].totalRegistros = data.length;
		$jt[sfx].entriesPage = ($jt[sfx].pagination == true ? $jt[sfx].entriesPage : data.length);

		var arr = [];
		if (data.length > 0) {
			$jt[sfx].data = data;
		}
		else {
			$jt[sfx].data = [];
			$showGrid(0);
			return;
		}
		var separator = _.separator;
		var cEntries = $jt[sfx].data.length;
		var cColumns = $jt[sfx].data[0].split(separator).length;
		var columns, date, typesData, eTypesData = false;
		if (_.typesData.length > 0) {
			eTypesData = true;
			typesData = _.typesData.slice();
		}
		for (var i = 0; i < cEntries; i++) {
			columns = $jt[sfx].data[i].split(separator);
			arr[i] = [];
			for (var j = 0; j < cColumns; j++) {
				if (eTypesData) {
					switch (typesData[j]) {
						case 'N':
						case 'DE':
							arr[i][j] = columns[j] * 1;
							break;
						case 'DT':
						case 'D':
							if (columns[j] != "") {
								date = columns[j].split('/');
								arr[i][j] = Date.UTC(date[2], (date[1] * 1) - 1, date[0], 5, 0, 0, 0);
							}
							else arr[i][j] = 0;
							break;
						case 'B':
							arr[i][j] = (columns[j] == 'true' ? true : false);
							break;
						default:
							arr[i][j] = columns[j].toString();
							break;
					}
				}
				else arr[i][j] = columns[j].toString();
			}
		}
		$jt[sfx].data = arr.slice();
		$jt[sfx].matrix = arr.slice();
		arr = [];
	};
	var $showGrid = function (indexPage) {
		var c = [];
		var separator = _.separator;
		$jt[sfx].indexCurrentPage = indexPage;
		var cEntries = $jt[sfx].matrix.length;
		if (cEntries > 0) {
			var start = indexPage * $jt[sfx].entriesPage;
			var end = start + $jt[sfx].entriesPage;
			document.getElementById('currentPage_' + sfx).innerHTML = start + 1;
			document.getElementById('totalPages_' + sfx).innerHTML = (end <= cEntries ? end : cEntries);
			document.getElementById('totalEntries_' + sfx).innerHTML = cEntries;
			var cColumns = $jt[sfx].matrix[0].length;
			var typesData, eTypesData = false, columns;
			if ($jt[sfx].typesData.length > 0) {
				eTypesData = true;
				typesData = $jt[sfx].typesData.slice();
			}
			var optionDate = {
				"year": "numeric", "month": "2-digit", "day": "2-digit"
			};
			var optionDateTime = {
				"year": "numeric", "month": "2-digit", "day": "2-digit", "hour": "2-digit", "minute": "2-digit", "second": "2-digit"
			};
			for (var i = start; i < end; i++) {
				if (i < cEntries) {
					columns = $jt[sfx].matrix[i];
					c.push('<tr ');
					if (_.rowStyle != '') {
						c.push(' style="');
						c.push(_.rowStyle);
						c.push('"');
					}
					else if (_.fnRowStyle != '') {
						c.push(extension(_.fnRowStyle, columns, i));
					}
					if (_.fnRowEvent != '') {
						c.push(' onclick="$$grid(\'');
						c.push(controlName);
						c.push('\').fnAction(\'');
						c.push(_.namespace);
						c.push('\',\'');
						c.push(_.fnRowEvent);
						c.push('\',');
						c.push(i);
						c.push(');"');
					}
					c.push('>');
					if (_.btnEdit) {
						c.push('<td class="text-center"><i class="fa fa-pencil btn-grid-edit hand" onclick="$$grid(\'');
						c.push(controlName);
						c.push('\').fnAction(\'');
						c.push(_.namespace);
						c.push('\',\'');
						c.push(_.fnBtnEdit);
						c.push('\',');
						c.push(i);
						c.push(');"></i></td>');
					}
					for (var j = 0; j < cColumns; j++) {
						if (_.showColumns[j]) {
							c.push('<td ');
							/*if (_.columStyle != '') {
								if (_.columStyle.split(",")[j]) {
									c.push(extension(_.columStyle.split(",")[j], columns, i));
								}
							}*/
							c.push(' class="');
                            /*if (_.eWidthsPx) {
                                c.push(' word-break');
                            }*/
							if (eTypesData) {
								switch (typesData[j]) {
									case 'N':
										c.push(' text-right">');
										c.push(columns[j]);
										break;
									case 'D':
										c.push(' text-center">');
										if (columns[j] == 0) c.push('');
										else c.push($frFormatoFecha((new Date(columns[j])).toLocaleString('es-PE', optionDate)));

										break;
									case 'DE':
										c.push(' text-right">');
										c.push($fnDecimal(columns[j]));
										break;
									case 'DT':
										c.push(' text-center">');
										if (columns[j] == 0) c.push('');
										else c.push((new Date(columns[j])).toLocaleString('es-PE', optionDateTime));
										break;
									case 'S_C':
									case 'N_C':
									case 'B':
										c.push(' text-center">');
										c.push(columns[j]);
										break;
									case 'E':
										c.push(' text-center">');
										c.push(extension($jt[sfx].fnExtensions[j], columns, i));
										break;
									case 'E_L':
										c.push(' text-left">');
										c.push(extension($jt[sfx].fnExtensions[j], columns, i));
										break;
									case 'E_R':
										c.push(' text-right">');
										c.push(extension($jt[sfx].fnExtensions[j], columns, i));
										break;
									default:
										c.push(' text-left">');
										c.push(columns[j]);
										break;
								}
							}
							else {
								c.push(' text-left">');
								c.push(columns[j]);
							}
							c.push('</td>');
						}
					}
					if (_.btnDelete) {
						c.push('<td class="text-center"><i class="fa fa-trash-o btn-grid-delete hand" onclick="$$grid(\'');
						c.push(controlName);
						c.push('\').fnAction(\'');
						c.push(_.namespace);
						c.push('\',\'');
						c.push(_.fnBtnDelete);
						c.push('\',');
						c.push(i);
						c.push(');"></i></td>');
					}
					c.push('</tr>');
				}
			}
		}
		else {
			c.push('<tr><td class="text-center" colspan="');
			c.push(_.headers.length + (_.btnEdit ? 1 : 0) + (_.btnDelete ? 1 : 0));
			c.push('">');
			c.push(_.language.noDataFound);
			c.push('</td></tr>');
			document.getElementById('currentPage_' + sfx).innerHTML = 0;
			document.getElementById('totalPages_' + sfx).innerHTML = 0;
			document.getElementById('totalEntries_' + sfx).innerHTML = 0;
		}
		document.getElementById('tbody_' + sfx).innerHTML = c.join('');
		if (_.pagination) {
			createPagination();
		}
		/*if ($jt[sfx].totalRegistros >= $jt[sfx].ShowSearch || $jt[sfx].ShowSearch == undefined) {
			if (document.getElementById(sfx).getElementsByTagName("thead")[1]) {
				document.getElementById(sfx).getElementsByTagName("thead")[1].classList.remove("hide");
			}
		}
		else {
			if (document.getElementById(sfx).getElementsByTagName("thead")[1]) {
				document.getElementById(sfx).getElementsByTagName("thead")[1].classList.add("hide");
			}
		}
		*/
	};
	var selectedCurrentPage = function () {
		var page = document.getElementById("goPage_" + sfx + $jt[sfx].indexCurrentPage.toString());
		if (page) page.classList.add('active');
	};
	var createPagination = function () {
		var cEntries = $jt[sfx].matrix.length;
		var indexLastPage = Math.floor(cEntries / $jt[sfx].entriesPage);
		if (cEntries % $jt[sfx].entriesPage == 0) indexLastPage--;
		var indexLastRange = Math.floor(cEntries / ($jt[sfx].rangePage * $jt[sfx].entriesPage));
		if (cEntries % ($jt[sfx].rangePage * $jt[sfx].entriesPage) == 0) indexLastRange--;
		var content = "";
		var start = $jt[sfx].indexCurrentRange * $jt[sfx].rangePage;
		var end = start + $jt[sfx].rangePage;
		if ($jt[sfx].indexCurrentRange > 0 && cEntries > ($jt[sfx].rangePage * $jt[sfx].entriesPage)) {
			content += "<span class='pagination-button' onclick='$$grid(\"";
			content += controlName;
			content += "\").pagination(-1);' title='Ir al primer grupo de páginas'>&lt;&lt;</span>";
			content += "<span class='pagination-button' onclick='$$grid(\"";
			content += controlName;
			content += "\").pagination(-2);' title='Ir al anterior grupo de páginas'>&lt;</span>";
		}

		for (var i = start; i < end; i += 1) {
			if (i <= indexLastPage) {
				content += "<span onclick='$$grid(\"";
				content += controlName;
				content += "\").pagination(";
				content += i;
				content += ");'  title='Ir a la pagina ";
				content += (i + 1).toString();
				content += "' id='goPage_";
				content += sfx;
				content += i.toString();
				content += "' class='pagination-button' >";
				content += (i + 1).toString();
				content += "</span>";
			} else break;
		}
		if ($jt[sfx].indexCurrentRange < indexLastRange && cEntries > ($jt[sfx].rangePage * $jt[sfx].entriesPage)) {
			content += "<span class='pagination-button' onclick='$$grid(\"";
			content += controlName;
			content += "\").pagination(-3);' title='Ir al siguiente grupo de páginas'>&gt;</span>";
			content += "<span class='pagination-button' onclick='$$grid(\"";
			content += controlName;
			content += "\").pagination(-4);' title='Ir al último grupo de páginas'>&gt;&gt;</span>";
		}

		if (cEntries <= $jt[sfx].entriesPage) {
			document.getElementById('pagination_' + sfx).innerHTML = "";
		}
		else {
			document.getElementById('pagination_' + sfx).innerHTML = content;
			selectedCurrentPage();
		}
	};
	var pagination = function (indexPage) {
		var cEntries = $jt[sfx].matrix.length;
		var isRange = (indexPage < 0);
		if (isRange) {
			var indexLastPage = Math.floor(cEntries / $jt[sfx].entriesPage);
			if (cEntries % $jt[sfx].entriesPage == 0) indexLastPage--;
			var indexLastRange = Math.floor(cEntries / ($jt[sfx].rangePage * $jt[sfx].entriesPage));
			if (cEntries % ($jt[sfx].rangePage * $jt[sfx].entriesPage) == 0) indexLastRange--;
			switch (indexPage) {
				case -1:
					indexPage = 0;
					$jt[sfx].indexCurrentRange = 0;
					break;
				case -2:
					if ($jt[sfx].indexCurrentRange > 0) {
						$jt[sfx].indexCurrentRange--;
						indexPage = $jt[sfx].indexCurrentRange * $jt[sfx].rangePage;
					}
					break;
				case -3:
					if ($jt[sfx].indexCurrentRange < indexLastRange) {
						$jt[sfx].indexCurrentRange++;
						indexPage = $jt[sfx].indexCurrentRange * $jt[sfx].rangePage;
					}
					break;
				case -4:
					indexPage = indexLastPage;
					$jt[sfx].indexCurrentRange = indexLastRange;
					break;
			}
		}
		$jt[sfx].indexCurrentPage = indexPage;
		$showGrid(indexPage);
	};
	var $exportTextExcel = function (type) {
		var fullName;
		if ($jt[sfx].matrix.length > 0) {
			var cEntries = $jt[sfx].matrix.length;
			var nFields = $jt[sfx].headers.length;
			var showColumnsExport = $jt[sfx].showColumnsExport.slice();
			var c = [], eTypesData = false, typesData;
			var fileName = (_.exportName != '' ? _.exportName : (type == 0 ? 'CsvExport' : 'ExcelExport'));
			if (type == 0) {
				var h = [];
				for (var i = 0; i < nFields; i++) {
					if (showColumnsExport[i]) {
						if ($jt[sfx].headers[i] != "") {
							h.push($jt[sfx].headers[i]);
						}
					}
				}
				c.push(h.join(','));
				c.push('\r\n');
				for (var i = 0; i < cEntries; i++) {
					h = [];
					for (var j = 0; j < nFields; j++) {
						if (showColumnsExport[j]) {
							if ($jt[sfx].headers[j] != "") {//Ray
								h.push($jt[sfx].matrix[i][j]);
							}
						}
					}
					c.push(h.join(','));
					if (i < cEntries - 1) c.push('\r\n');
				}
				var formBlob = new Blob([c.join('')], {
					encoding: "UTF-8", type: 'text/plain;charset=UTF-8'
				});
				fullName = fileName + '.txt';
			}
			else {
				if ($jt[sfx].typesData.length > 0) {
					eTypesData = true;
					typesData = $jt[sfx].typesData;
				}
				var optionDate = { "year": "numeric", "month": "2-digit", "day": "2-digit" };
				var optionDateTime = { "year": "numeric", "month": "2-digit", "day": "2-digit", "hour": "2-digit", "minute": "2-digit", "second": "2-digit" };
				c.push('<html><head><meta charset="UTF-8"><style>');
				c.push('.number_2{ mso-number-format:"0\.00"} .string{ mso-number-format:"\\@"} ');
				c.push('</style></head><body><table><thead><tr>');
				for (var i = 0; i < nFields; i++) {
					if (showColumnsExport[i]) {
						if ($jt[sfx].headers[i] != "") {
							c.push('<th>');
							if ($jt[sfx].headers[i] != '') {
								c.push($jt[sfx].headers[i]);
							}
							c.push('</th>');
						}
					}
				}
				c.push('</tr></thead><tbody>');
				for (var i = 0; i < cEntries; i++) {
					c.push('<tr>');
					for (var j = 0; j < nFields; j++) {
						if (showColumnsExport[j]) {
							if ($jt[sfx].headers[j] != "") {
								c.push('<td style="text-align:');
								if (eTypesData) {
									switch (typesData[j]) {
										case 'N':
											c.push('right" class="number_2">');
											c.push($jt[sfx].matrix[i][j].toFixed(2));
											break;
										case 'D':
											c.push('center" class="string">');
											if ($jt[sfx].matrix[i][j] == 0) c.push('');
											else c.push((new Date($jt[sfx].matrix[i][j])).toLocaleString('es-PE', optionDate));
											break;
										case 'DT':
											c.push('center" class="string">');
											if ($jt[sfx].matrix[i][j] == 0) c.push('');
											else c.push((new Date($jt[sfx].matrix[i][j])).toLocaleString('es-PE', optionDate));
											break;
										case 'S_C':
										case 'N_C':
										case 'B':
											c.push('left" class="string">');
											c.push($jt[sfx].matrix[i][j]);
											break;
										case 'E':
											c.push(' class= "text-center">');
											c.push(extension($jt[sfx].fnExtensions[j], $jt[sfx].matrix[i], i));
											break;
										case 'E_L':
											c.push(' class= "text-left">');
											c.push(extension($jt[sfx].fnExtensions[j], $jt[sfx].matrix[i], i));
											break;
										case 'E_R':
											c.push(' class= "text-right">');
											c.push(extension($jt[sfx].fnExtensions[j], $jt[sfx].matrix[i], i));
											break;
										default:
											c.push('left" class="string">');
											c.push($jt[sfx].matrix[i][j]);
											break;
									}
								}
								else {
									c.push('left" class="string">');
									c.push($jt[sfx].matrix[i][j]);
								}
								c.push('</td>');
							}
						}
					}
					c.push('</tr>');
				}
				c.push('</tbody></table></body></html>');
				var formBlob = new Blob([c.join('')], {
					encoding: "UTF-8", type: 'application/vnd.ms-excel;charset=UTF-8'
				});
				fullName = fileName + '.xls';
			}
			if (navigator.appName == 'Microsoft Internet Explorer' || !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv 11/))) {
				navigator.msSaveOrOpenBlob(formBlob, fullName);
			}
			else {
				var a = document.createElement('A');
				a.download = fullName;
				a.href = window.URL.createObjectURL(formBlob);
				document.body.appendChild(a);
				a.click();
				document.body.removeChild(a);
			}
		}
		else {
			$alert.show(_.language.noDataFound);
		}
	};
	var extension = function (fn, row, rowId) {
		if ($jt[_.namespace][fn] != undefined) {
			if (_.eProperties) {
				var obj = {};
				for (var i = 0; i < _.properties.length; i++) {
					obj[_.properties[i]] = row[i];
				}
				return $jt[_.namespace][fn](obj, rowId);
			}
			else {
				return $jt[_.namespace][fn](row[rowId], rowId);
			}
		}
		else { console.log('function ' + fn + _msgError.m4); }
		//return $jt[_.namespace][extension](columns, index);
	};
	var $filter = function (o) {
		var filters = document.getElementsByName("filter" + sfx);
		var cFilters = filters.length;
		for (var i = 0; i < cFilters; i++) {
			if (i == o.index) filters[i].value = o.value;
			else filters[i].value = '';
		}
	};
	var $selected = function (o) {
		var el = document.getElementById(sfx).getElementsByClassName("table-selected-tr")[0];
		if (el) {
			el.classList.remove("table-selected-tr");
		}
		if (o) {
			o.parentElement.parentElement.classList.add("table-selected-tr");
		}
	};
	var setFilterPosition = function (position) {
		var eFilter = {
			up: false,
			down: false
		};
		switch (position) {
			case 'UP':
				eFilter.up = true;
				break;
			case 'DOWN':
				eFilter.down = true;
				break;
		}
		return eFilter;
	};
	return {
		create: function (d) {
			$create(d);
		},
		setData: function (d) {
			$setData(d);
		},
		showGrid: function (i) {
			$showGrid(i);
		},
		clearGrid: function () {
			$clearGrid();
		},
		pagination: function (indexPage) {
			pagination(indexPage);
		},
		setMatrixRow: function (i, n, v) {
			$jt[sfx].matrix[i][n] = v;
		},
		setMatrix: function (matrix) {
			$jt[sfx].matrix = matrix.slice();
			$showGrid(0);
		},
		setDataMatrix: function (data) {
			$jt[sfx].matrix = data.slice();
			$jt[sfx].data = data.slice();
			$showGrid(0);
		},
		getData: function () {
			return $jt[sfx].matrix;
		},
		fnAction: function (namespace, fn, rowId) {
			if ($jt[namespace][fn] != undefined) {
				if (_.eProperties) {
					var obj = {};
					for (var i = 0; i < _.properties.length; i++) {
						obj[_.properties[i]] = $jt[sfx].matrix[rowId][i];
					}
					$jt[namespace][fn](obj, rowId);
				}
				else {
					$jt[namespace][fn]($jt[sfx].matrix[rowId], rowId);
				}
			}
			else { console.log('function ' + fn + _msgError.m4); }
		},
		filter: function (o) {
			$filter(o);
		},
		selected: function (o) {
			$selected(o);
		},
		exportTextExcel: function (o) {
			$exportTextExcel(o);
		}
	};
};