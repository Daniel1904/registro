﻿var $fnConvertStringToHex = function (str) {
    var hex = '';
    for (var i = 0; i < str.length; i++) {
        hex += str.charCodeAt(i).toString(16);
    }
    return hex;
};
var $fnConvertHexToString = function (hex) {
    var hex = hex.toString();
    var str = '';
    for (var i = 0; i < hex.length; i += 2) {
        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    }
    return str;
};
var $fnMD5 = function (d) {
    return hex_md5(d);
};
var $fnSetStorage = function (name, value) {
    window.localStorage.setItem(name, value);
};
var $fnGetStorage = function (name) {
    return window.localStorage.getItem(name) || '';
};
var $fnPadLeft = function (cadena, cantidad, textoAgregar) {
    return Array(cantidad - String(cadena).length + 1).join(textoAgregar || '0') + cadena;
};
var $fnDateNow = function () {
    var cero = function (valor) {
        var num = valor;
        if ((valor * 1) < 10) {
            num = '0' + valor;
        }
        return num;
    };
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    return cero(dd) + "/" + cero(mm) + "/" + yyyy;
};
var $fnSetCombo = function (obj) {
    //TODO: controlId : Identificador del Control
    //TODO: data : Data en Array
    //TODO: type : Tipo de Combo 'S' / Seleccionar |  'T' / Todos | 'E' / Específico
    //TODO: value : Valor Inicial
    //TODO: description : Descripción Inicial
    //TODO: separator : Separador de Campos
    //TODO: valuePosition : Índice del valor
    //TODO: descriptionPosition : Índice de la descripción
    if (obj) {
        var data = obj.data || [];
        var ctrl = obj.controlId || '';
        var tipo = obj.type;
        var valorInicial = obj.value || '';
        var descripcionIncial = obj.description || '';
        var s = obj.separator || '¦';
        var pv = obj.valuePosition || 0;
        var pd = obj.descriptionPosition || 1;
        var c = [], v = '', d = '';
        if (tipo) {
            switch (tipo) {
                case 'S':
                    v = $controls.combo.valorInicial1;
                    d = $controls.combo.descripcionInicial1;
                    break;
                case 'T':
                    v = $controls.combo.valorInicial2;
                    d = $controls.combo.descripcionInicial2;
                    break;
                case 'E':
                    v = _valorInicial;
                    d = _descripcionIncial;
                    break;
                default:
                    break;
            }
            if (d != '') {
                c.push('<option value="');
                c.push(v);
                c.push('">');
                c.push(d);
                c.push('</option>');
            }
        }

        if (data != null) {
            var n = data.length;
            var campos;
            for (var i = 0; i < n; i++) {
                campos = data[i].split(s);
                c.push('<option value="');
                c.push(campos[pv]);
                c.push('">');
                c.push(campos[pd]);
                c.push('</option>');
            }
        }
        if (document.getElementById(ctrl)) {
            document.getElementById(ctrl).innerHTML = c.join('');
        }
        else {
            console.log($msg.error.control);
        }
    }
    else console.log($msg.error.control);
};
var $fnFindText = function (text, list, indexCompare, indexResult) {
    indexCompare = indexCompare || 0;
    indexResult = indexResult || 1;
    var field = '';
    var value = '';
    for (var i = 0; i < list.length; i++) {
        field = list[i].split('¦');
        if (field[indexCompare] === text) {
            value = field[indexResult];
            break;
        }
    }
    return value;
};
var $fnValidarCampos = function (obj) {
    var c, qs, type, e, r, ctrl, entre, containers;
    entre = false;
    if (obj) {
        var o = obj;
        if (o.containers) {
            qs = document.querySelectorAll('[data-container]');
            containers = o.containers;
        }
    }
    var buscarContenedor = function (name) {
        var obj = {};
        for (var i = 0; i < qs.length; i++) {
            if (qs[i].getAttribute('data-container') == name) {
                obj = qs[i];
            }
        }
        return obj;
    };
	var getCaretPosition = function (ctrl) {
		// IE < 9 Support
		if (document.selection) {
			ctrl.focus();
			var range = document.selection.createRange();
			var rangelen = range.text.length;
			range.moveStart('character', -ctrl.value.length);
			var start = range.text.length - rangelen;
			return { 'start': start, 'end': start + rangelen };
		}
		// IE >=9 and other browsers
		else if (ctrl.selectionStart || ctrl.selectionStart == '0') {
			return { 'start': ctrl.selectionStart, 'end': ctrl.selectionEnd };
		} else {
			return { 'start': 0, 'end': 0 };
		}
	};

    var Numerico = function (e) {
        var key;
        if (window.event) // IE
        {
            key = e.keyCode;
        }
        else if (e.which) // Netscape/Firefox/Opera
        {
            key = e.which;
        }
        return (key <= 13 || (key >= 48 && key <= 57) || key == 44 || key == 8);
    };
    var FormatoNumerico = function (valor) {
        var num = valor.replace(/\,/g, '');
        if (!isNaN(num)) {
            num = num.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g, '$1,');
            num = num.split('').reverse().join('').replace(/^[\,]/, '');
            valor = (num == "") ? "0" : num;
        }
        else {
            valor = "0";
        }
        return valor;
    }
    var Decimal = function (el, evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        var number = el.value.split('.');
        if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        //just one dot (thanks ddlab)
        if (number.length > 1 && charCode == 46) {
            return false;
        }
        //get the carat position
        var caratPos = getCaretPosition(el).start;
        var dotPos = el.value.indexOf(".");
        var decimales = ((el.getAttribute("data-digitos")) ? el.getAttribute("data-digitos") : "2") * 1;

        if (caratPos > dotPos && dotPos > -1 && (number[1].length > decimales - 1)) {
            return false;
        }
        return true;
    };
    var Cadena = function (el, evt) {
        var exito = true;
        var key;
        if (window.event) // IE
        {
            key = evt.keyCode;
        }
        else if (evt.which) // Netscape/Firefox/Opera
        {
            key = evt.which;
        }
        var CharLocked = $config.charLocked;
        var dato = el.value + String.fromCharCode(key);
        for (var p = 0; p < CharLocked.length; p++) {
            if (dato.indexOf(CharLocked.substring(p, p + 1)) > -1) {
				exito = false;
                break;
            }
        }

        return exito;
    };
    var Paste = function (el, e, drop) {
        var dato = "";
        var _lenght;
        if (drop) {
            dato = e.dataTransfer.getData("Text");
        }
        else {
            if (e.clipboardData) {
                dato = e.clipboardData.getData("text/plain");
            }
            else if (window.clipboardData) {
                dato = window.clipboardData.getData("Text");
            }
        }
        _lenght = el.value.length + dato.length;
        setTimeout(function () {
            var CharLocked = $charLocked;
            var caracter = "";
            var reg = "";
            for (var q = 0; q < CharLocked.length; q++) {
                caracter = CharLocked.substring(q, q + 1);
                if (caracter == "|") el.value = el.value.replace(/\|/g, '');
                else el.value = el.value.replace(new RegExp(caracter, 'g'), '');
            }
            if (el.className.indexOf('validate-number') > -1) {
                el.value = el.value.replace(/[^0-9]+/g, '');
            }
            if (el.className.indexOf('validate-decimal') > -1) {
                el.value = dato.replace(/[^0-9\.]+/g, '');
            }
            if (el.className.indexOf('validate-date') > -1) {
                if (fnIsDate(dato) == false) {
                    el.value = "";
                    $alert.show($msg.ErrorInDate);
                }
                else {
                    el.value = dato;
                }
            }
            if (el.maxLength > -1) {
                if ((_lenght) > el.maxLength) {
                    $alert.show($msg.TextCut.replace("[0]", el.maxLength));
                }
            }
            if (el.tagName.toUpperCase() == "TEXTAREA") {
                el.nextSibling.innerHTML = $msg.MissingCharacters.replace("[0]", ((el.maxLength > -1) ? el.maxLength - el.value.length : ""));
                if ((el.maxLength - el.value.length) < 21) {
                    el.nextSibling.style.color = "red";
                } else { el.nextSibling.style.color = ""; }
            }
            if (typeof el.onkeyup == "function") {
                el.focus();
                el.onkeyup();
            }
        }, 0);
    };
    var NumMaxMin = function (el, e) {
        var key;
        if (window.event) // IE
        {
            key = e.keyCode;
        }
        else if (e.which) // Netscape/Firefox/Opera
        {
            key = e.which;
        }
        var valor = el.value; //+ String.fromCharCode(key);
        var valorFin = "";
        var ok = false;
        var pos = getCaretPosition(el).start;
        for (var b = 0; b < valor.length + 1; b++) {
            if (pos == b && ok != true) {
                valorFin += String.fromCharCode(key);
                b = b - 1;
                ok = true;
            } else {
                valorFin += valor.substring(b, b + 1);
            }
        }

        if (b == 0) { valorFin = String.fromCharCode(key) }
        //|| isNaN(parseInt(valorFin*1))
        var min = ((el.getAttribute("data-min")) ? el.getAttribute("data-min") : "0") * 1;
        var max = ((el.getAttribute("data-max")) ? el.getAttribute("data-max") : "99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999") * 1;
        if (Numerico(e) != false) {
            if (key != 8 && key != 13 && key != 37 && key != 39) {
                if (parseInt(valorFin * 1) < min) {
                    return false;
                }
                else if (parseInt(valorFin * 1) > max) {
                    return false;
                }
            }
        }
        else {
            return false;
        }
    };
    var DecimalMaxMin = function (el, e) {
        var key;
        var _return = true;
        if (window.event) // IE
        {
            key = e.keyCode;
        }
        else if (e.which) // Netscape/Firefox/Opera
        {
            key = e.which;
        }

        var valor = el.value;
        var valorFin = "";
        var ok = false;
        var pos = getCaretPosition(el).start;
        var posEnd = getCaretPosition(el).end;



        for (var b = 0; b < valor.length + 1; b++) {
            if (pos == b && ok != true) {
                valorFin += String.fromCharCode(key);
                b = b - 1;
                ok = true;
            } else {
                valorFin += valor.substring(b, b + 1);
            }
        }

        if (b == 0) { valorFin = String.fromCharCode(key) }
        var decimales = ((el.getAttribute("data-digitos")) ? el.getAttribute("data-digitos") : 2) * 1;
        var min = parseFloat((el.getAttribute("data-min")) ? el.getAttribute("data-min") : "0");
        var dec = (el.getAttribute("data-max")) ? el.getAttribute("data-max") : ("9999999999999999999." + "99999999".substring(0, decimales));
        var max = parseFloat(dec);
        if (Decimal(el, e) != false) {
            if (key != 46 && key != 8 && key != 13 && key != 37 && key != 39) {
                if (parseFloat($frBorrarComas(valorFin) * 1) < min || isNaN(parseFloat($frBorrarComas(valorFin) * 1))) {
                    _return = false;
                }
                else if (parseFloat($frBorrarComas(valorFin) * 1) > max) {
                    _return = false;
                }
            }
        }
        else {
            _return = false;
        }
        if (pos == 0 && posEnd == el.value.length) {
            el.value = "";
        }
        return _return
    };
    var chekDate = function (el) {
        if (el.value != "") {
            var vregexNaix = "";
            if ($controls.datepicker.format == "dd/mm/yyyy" || $controls.datepicker.format == "dd-mm-yyyy") {
                vregexNaix = /^(0[1-9]|[1-2]\d|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/;
                var d = el.value.substring(0, 2) * 1;
                var m = el.value.substring(3, 5) * 1;
                var y = el.value.substring(6, 10) * 1;
            }
            if ($controls.datepicker.format == "mm/dd/yyyy" || $controls.datepicker.format == "mm-dd-yyyy") {
                vregexNaix = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
                var m = el.value.substring(0, 2) * 1;
                var d = el.value.substring(3, 5) * 1;
                var y = el.value.substring(6, 10) * 1;
            }

            var _date = new Date(y, m, d);


            if (vregexNaix.test(el.value)) {

                var min = ((el.getAttribute("date-min") != null) ? el.getAttribute("date-min") : "1900-01-01");
                var max = ((el.getAttribute("date-max") != null) ? el.getAttribute("date-max") : "");

                var _datemin = new Date(min);
                var _datemax;
                if (max != "") {
                    _datemax = new Date(max);
                }
                if (max == "") {
                    if (_date < _datemin) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
                else {
                    if (_date < _datemin || _date > _datemax) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
            }
            else {
                return false;
            }
        } else { return true }

    };
	//var prueba = function () {
	//	this.value = this.value + " %";
	//};
    var changeTextArea = function (el) {
        if (el.tagName.toUpperCase() == "TEXTAREA") {
            el.nextSibling.innerHTML = $msg.MissingCharacters.replace("[0]", ((el.maxLength > -1) ? el.maxLength - el.value.length : ""));
            if ((el.maxLength - el.value.length) < 21) {
                el.nextSibling.style.color = "red";
            } else { el.nextSibling.style.color = ""; }
        }
    }
    var formatoCampos = function () {
        for (var a = 0; a < ((obj) ? containers.length : 1); a++) {
            c = (obj) ? buscarContenedor(containers[a]) : document;
            r = c.getElementsByClassName('form-control');
            for (var b = 0; b < r.length; b++) {
                ctrl = r[b];
                type = ctrl.className;
                if (ctrl.tagName.toUpperCase() == "INPUT") {
                    if (type.indexOf('validate-') > -1) {
                        if (type.indexOf('validate-number') > -1) {
                            if (ctrl.value == "") {
                                ctrl.value = "0";
                            }
							ctrl.onkeypress = function (evt) {
								if (NumMaxMin(this, event) == false) {
									event.preventDefault();
								}
							};
							ctrl.onblur = function (evt) {
								if (this.value == "") {
									this.value = "0";
								}

							};
							ctrl.onfocus = function () {
								this.selectionStart = 0;
								this.selectionEnd = this.value.length;
								this.focus();
							};
                        }
                        if (type.indexOf('validate-decimal') > -1) {
                            if (ctrl.value == "") {
                                var decimales = ((ctrl.getAttribute("data-digitos")) ? ctrl.getAttribute("data-digitos") : "2") * 1;
                                ctrl.value = "0." + "00000000".substring(0, decimales);
                            }
                            if (type.indexOf('percent') > -1) {
                                if (ctrl.value.indexOf(" %") == -1) {
                                    ctrl.value = ctrl.value + " %";
                                }
                            }
							ctrl.onkeypress = function (evt) {
								if (DecimalMaxMin(this, event) == false) {
									event.preventDefault();
								}
							};
                            ctrl.onblur = function () {
                                var decimales = ((this.getAttribute("data-digitos")) ? this.getAttribute("data-digitos") : "2") * 1;

                                if (this.value != "") {
                                    this.value = $frDecimal(this.value, decimales);
                                }
                                else {
                                    this.value = "0." + "00000000".substring(0, decimales);
                                }
                                if (this.className.indexOf('percent') > -1) {
                                    this.value = this.value.replace(" %", "") + " %";
                                }
                            }
							ctrl.onfocus = function () {
								if (this.className.indexOf('percent') > -1) {
									this.value = this.value.replace(" %", "");
								}
								this.selectionStart = 0;
								this.selectionEnd = this.value.length;
								this.focus();

							};
                        }
                        if (type.indexOf('validate-web') > -1) {
							ctrl.onblur = function () {
								$frWeb(this);
							};
                            if (typeof ctrl.onkeypress == "function") {
                                //fnCreateEvent(ctrl, "keypress", function (evt) {
                                //	if (Cadena(this, evt) == false) {
                                //		evt.preventDefault();
                                //	}
                                //});
                            }
                            else {
								ctrl.onkeypress = function (evt) {
									if (Cadena(this, evt) == false) {
										evt.preventDefault();
									}
								};
                            }
                        }
                        if (ctrl.className.indexOf("validate-mail") > -1) {
							ctrl.onblur = function () {
								this.value = this.value.trim();
								$frMail(this);
							};
							ctrl.onkeypress = function (evt) {
								if (Cadena(this, evt) == false) {
									evt.preventDefault();
								}
							};
                        }
                        if (type.indexOf('validate-date') > -1) {
                            $$datepicker(ctrl.id).create();
							ctrl.onblur = function () {
								if (chekDate(this) == false) {
									$alert.show($msg.ErrorInDate);
									this.value = "";
									this.parentElement.classList.add('has-error');
								}
								else {
									this.parentElement.classList.remove('has-error');
								}
							};
                        }
                    }
                    else {
						ctrl.onkeypress = function (evt) {
							if (Cadena(this, evt) == false) {
								evt.preventDefault();
							}
						};
                    }
					ctrl.onpaste = function (evt) {
						Paste(this, evt);
					};
					ctrl.ondrop = function (evt) {
						Paste(this, evt, true);
					};
                }
                if (ctrl.tagName.toUpperCase() == "TEXTAREA") {
                    if (!ctrl.parentNode.getElementsByTagName("SPAN")[0]) {
                        var newItem = document.createElement("span");
                        newItem.innerHTML = $msg.MissingCharacters.replace("[0]", ctrl.maxLength);
                        if ((ctrl.maxLength - ctrl.value.length) < 21) {
                            newItem.style.color = "red";
                        }
                        ctrl.parentNode.insertBefore(newItem, ctrl.nextSibling);
                    }
                    ctrl.nextSibling.innerHTML = $msg.MissingCharacters.replace("[0]", ((ctrl.maxLength > -1) ? ctrl.maxLength - ctrl.value.length : ""));
					ctrl.onkeyup = function (event) {
						if (event) {
							if (Cadena(this, event) == false) {
								event.preventDefault();
							}
							else {
								if (this.tagName.toUpperCase() == "TEXTAREA") {
									this.nextSibling.innerHTML = $msg.MissingCharacters.replace("[0]", ((this.maxLength > -1) ? this.maxLength - this.value.length : ""));
									if ((this.maxLength - this.value.length) < 21) {
										this.nextSibling.style.color = "red";
									} else { this.nextSibling.style.color = ""; }
								}
							}
						}
					};
					ctrl.onkeypress = function (evt) {
						if (Cadena(this, evt) == false) {
							evt.preventDefault();
						}
					};
					ctrl.onpaste = function (evt) {
						Paste(this, event);
					};
					ctrl.ondrop = function (evt) {
						Paste(this, evt, true);
					};
                }

            }
        }
    };
    formatoCampos();
};
var $fnValidarForm = function (obj) {
    var o = obj, result = 0, percent = "";
    if (o.containers != undefined) {
        var tabs = o.Tabs;
        var containers = o.containers;
        var c, qs, r, ctrl, e;
        var vm, vw, ctrlVM, ctrlVW, validations = {};
        qs = document.querySelectorAll('[data-container]');
        var buscarContenedor = function (name) {
            var obj = {};
            for (var i = 0; i < qs.length; i++) {
                if (qs[i].getAttribute('data-container') == name) {
                    obj = qs[i];
                }
            }
            return obj;
        };
        var posicionarValidacion = function (obj, contenedor) {
            var objCurrent = obj;
            while (true) {
                obj = obj.parentElement;
                if (obj.getAttribute('data-container') == contenedor) break;
                if (obj.id) {
                    if (obj.id.indexOf('-Tabs-') > -1) {
                        if (document.querySelector('[data-tab="' + obj.id + '"]')) {
                            document.querySelector('[data-tab="' + obj.id + '"]').click();
                            var viewRoot = document.getElementsByTagName('view-root')[0].children[0];
                            viewRoot.scrollTop = 0;
                            viewRoot.scrollTop = objCurrent.getBoundingClientRect().top - objCurrent.getBoundingClientRect().height - 50;
                        }
                    }
                }
            }
        };
        var validarCampos = function (reset) {
            for (var a = 0; a < containers.length; a++) {
                c = buscarContenedor(containers[a]);
                validations[containers[a]] = [];
                if (!reset) {
                    vm = c.getElementsByClassName('validate-mail');
                    for (var d = 0; d < vm.length; d++) {
                        ctrlVM = vm[d];
                        if (ctrlVM.className.indexOf("validate-mail") > -1) {
                            if (ctrlVM.value != '') {
                                if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(ctrlVM.value) == false) {
                                    ctrlVM.parentElement.classList.add('has-error');
                                    result++;
                                    validations[containers[a]].push(ctrlVM);
                                }
                                else ctrlVM.parentElement.classList.remove('has-error');
                            }
                        }
                    }
                    vw = c.getElementsByClassName('validate-web');

                    for (var e = 0; e < vw.length; e++) {
                        ctrlVW = vw[e];
                        if (ctrlVW.className.indexOf("validate-web") > -1) {
                            if (ctrlVW.value != '') {
                                var strExpReg = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
                                if (!strExpReg.test(ctrlVW.value)) {
                                    ctrlVW.parentElement.classList.add('has-error');
                                    result++;
                                    validations[containers[a]].push(ctrlVM);
                                }
                                else ctrlVW.parentElement.classList.remove('has-error');
                            }
                        }
                    }
                    vw = c.getElementsByClassName('validate-decimal');
                    for (var e = 0; e < vw.length; e++) {
                        ctrlVW = vw[e];
                        if (ctrlVW.classList.contains('has-error')) {
                            result++;
                            validations[containers[a]].push(ctrlVM);
                        }
                        else {
                            ctrlVW.classList.remove('has-error');
                            if (ctrlVW.className.indexOf("percent") > -1) {
                                percent += "¦" + ctrlVW.id;
                            }
                        }
                    }
                } else {
                    vm = c.getElementsByClassName('validate-mail');
                    for (var d = 0; d < vm.length; d++) {
                        ctrlVM = vm[d];
                        ctrlVM.parentElement.classList.remove('has-error');

                    }
                    vm = c.getElementsByClassName('validate-web');
                    for (var d = 0; d < vm.length; d++) {
                        ctrlVM = vm[d];
                        ctrlVM.parentElement.classList.remove('has-error');

                    }
                    vm = c.getElementsByClassName('validate-decimal');
                    for (var d = 0; d < vm.length; d++) {
                        ctrlVM = vm[d];
                        ctrlVM.parentElement.classList.remove('has-error');

                    }
                }
                r = c.getElementsByClassName('required');
                for (var b = 0; b < r.length; b++) {
                    ctrl = r[b];
                    if (!reset) {
                        switch (ctrl.tagName) {
                            case 'INPUT':
                                if (ctrl.type == 'text') {
                                    if (ctrl.value.trim() == '') {
                                        ctrl.parentElement.classList.add('has-error');
                                        result++;
                                        validations[containers[a]].push(ctrl.parentElement);
                                    }
                                    else {
                                        if (ctrl.className.indexOf("validate-decimal") > -1) {
                                            var dato = ctrl.value.replace(/./g, "");
                                            dato = ctrl.value.replace(/,/g, "") * 1;
                                            if (dato == 0) {
                                                ctrl.parentElement.classList.add('has-error');
                                                result++;
                                                validations[containers[a]].push(ctrl.parentElement);
                                            }
                                            else {
                                                ctrl.parentElement.classList.remove('has-error');
                                            }
                                        }
                                        else {
                                            if (ctrl.className.indexOf("validate-web") == -1 && ctrl.className.indexOf("validate-mail") == -1) {
                                                ctrl.parentElement.classList.remove('has-error');
                                            }
                                        }
                                    }
                                }
                                break;
                            case 'TEXTAREA':
                                if (ctrl.value == '') {
                                    ctrl.parentElement.classList.add('has-error');
                                    result++;
                                    validations[containers[a]].push(ctrl);
                                }
                                else ctrl.parentElement.classList.remove('has-error');
                                break;
                            case 'SELECT':
                                if (ctrl.value == '0' || ctrl.value == '') {
                                    ctrl.parentElement.classList.add('has-error');
                                    result++;
                                    validations[containers[a]].push(ctrl);
                                }
                                else ctrl.parentElement.classList.remove('has-error');
                                break;
                            case 'DIV':
                                if (ctrl.classList.contains('jtse-tags')) {
                                    if (ctrl.getAttribute('role-tags') == 'multiselect') {
                                        if ($$multiselect(ctrl.id).getValues() == '') {
                                            ctrl.classList.add('has-error');
                                            result++;
                                            validations[containers[a]].push(ctrl);
                                        }
                                        else ctrl.classList.remove('has-error');
                                    }
                                    if (ctrl.getAttribute('role-tags') == 'multitag') {
                                        if ($$multitag(ctrl.id).getValues() == '') {
                                            ctrl.classList.add('has-error');
                                            result++;
                                            validations[containers[a]].push(ctrl);
                                        }
                                        else ctrl.classList.remove('has-error');
                                    }
                                }
                                break;
                        }
                    }
                    else {
                        switch (ctrl.tagName) {
                            case 'INPUT':
                                ctrl.parentElement.classList.remove('has-error');
                            case 'TEXTAREA':
                                ctrl.parentElement.classList.remove('has-error');
                            case 'SELECT':
                                ctrl.parentElement.classList.remove('has-error');
                                break;
                            case 'DIV':
                                ctrl.classList.remove('has-error');
                                break;
                        }
                    }
                }
                if (validations[containers[a]].length > 0) {
                    posicionarValidacion(validations[containers[a]][0], containers[a]);
                }
                else {
                    if (percent != "") {
                        percent = percent.substring(1, percent.length);
                        var objs = percent.split("¦");
                        for (var b = 0; b < objs.length; b++) {
                            document.getElementById(objs[b]).value = document.getElementById(objs[b]).value.replace(" %", "");
                        }
                    }
                }
            }
        };
        validarCampos(false);
    }
    return {
        resultado: result,
        resetear: function () {
            validarCampos(true);
        }
    };
};
var $fnIsMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
    },
    any: function () {
        return ($fnIsMobile.Android() || $fnIsMobile.BlackBerry() || $fnIsMobile.iOS() || $fnIsMobile.Opera() || $fnIsMobile.Windows());
    }
};
var $fnSetPermission = function (permission, reset) {
    if (!reset) {
        $global.permission = {
            insert: Boolean(parseInt(permission[0])),
            edit: Boolean(parseInt(permission[1])),
            delete: Boolean(parseInt(permission[2])),
            //print: Boolean(parseInt(permission[3])),
            excel: Boolean(parseInt(permission[3]))
            //abort: Boolean(parseInt(permission[5]))
        };
    }
    else {
        $global.permission = {
            insert: false,
            edit: false,
            delete: false,
            //print: false,
            excel: false
            //abort: false
        };
    }
};

