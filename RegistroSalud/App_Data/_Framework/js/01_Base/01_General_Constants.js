﻿"use strict";
//TODO: GENERAL CONSTANTS
var $jtse, $app, $jt = [], $s = [], $d = [], define, $_ = [],
    $appName = "AppWeb_Sunquyux",
    $appVersion = "",
    $urlBase = "",
    $urlError = "",
    $config = {
        token: '',
        charLocked: "",
        isEncoded: false,
        isDeveloper: false,
        isIE: true,
        isAD: false,
        timeoutXHR: 0,
        loadingText: 'Cargando...',
        others: ''
    },
    $global = {
        user: {
            name: '',
            codigo: ''
        },
        permission: {
            insert: false,
            edit: false,
            delete: false,
            //print: false,
            excel: false
            //abort: false
        },
        roles: {},
        lists: {
        },
        layout: {
            title: ''
        }
    },
    $modules = {
        general: 'Modules/00_General/',
        Cliente: 'Modules/Cliente/',
        Invoice: 'Modules/Invoice/',
        Producto: 'Modules/Producto/',

    },
    $msg = {
        error: {
            framework: 'Se ha producido un error en el Framework',
            server: 'Se ha producido un error en el servidor',
            page: 'Se ha producido un error al cargar la página',
            control: 'Se ha producido un error en el control'
        },
        response: {
            success: 'Se registró satisfactoriamente',
            error: 'No se registró satisfactoriamente',
            successUpdate: 'Se actualizó satisfactoriamente',
            errorUpdate: 'No se actualizó satisfactoriamente',
            successDelete: 'Se eliminó satisfactoriamente',
            errorDelete: 'No se eliminó satisfactoriamente',
            errorSession: 'Las credenciales ingresadas son incorrectas',
            infoGridDetalle: 'Ingrese productos al detalle',
            infoDuplicate: 'Producto ya esta registrado',
            successSession: 'Bienvenido'


        }
    },
    $controls = {
        grid: {
            headerStyle: 'background-color:rgb(70,130,180);color:#fff'
        },
        combo: {
            valorInicial1: '',
            descripcionInicial1: 'Seleccione',
            valorInicial2: '',
            descripcionInicial2: 'Todos'
        },
        datepicker: {
            format: 'dd/mm/yyyy',
            datePickerDefault: {
                WeekDayLabels: ['Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa', 'Do'],
                ShortMonthLabels: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sept', 'Oct', 'Nov', 'Dic'],
                SingleMonthLabels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
            }
        }
    };

var $loading, $alert, $tooltip;
