﻿var $fnSqx_vLd = function (frm) { //function vLd(frm)
	var frms = document.getElementsByClassName(frm), c = 0;
	if (frms != null) {
		var n = frms.length, cnt, at, msg, cnterror = null;
		for (var i = 0; i < n; i++) {
			cnt = frms[i];
			at = cnt.getAttribute("data-vl");
			if (at != null) {
				msg = $fnSqx_vI(cnt, at, true);
				if (msg) {
					c++;
					if (c == 1) { cnterror = cnt; }
				}
			} else {
				msg = $fnSqx_vI(cnt, "", false);
				if (msg) {
					c++;
					if (c == 1) { cnterror = cnt; }
				}
			}
		}
	}
	if (cnterror != null) { cnterror.focus(); }
	return (c > 0 ? false : true);
};
var $fnSqx_vI = function (elemento, mensaje, obligatorio) {//function vI(elemento, mensaje, obligatorio)
	var error = false;
	if (obligatorio == undefined) {
		obligatorio = true;
	}
	var valor = $fnSqx_vInp(elemento, mensaje, obligatorio);
	if (valor != "") {
		error = true;
		if (!elemento.classList.contains("error")) {
			elemento.classList.add("error");
		}
		if (elemento.nextElementSibling != null) {
			if (!elemento.nextElementSibling.classList.contains("label-error")) {
				var div = "<div class='label-error'>";
				div += valor + "</div>";
				elemento.insertAdjacentHTML("afterend", div);
			} else {
				elemento.nextElementSibling.innerHTML = valor;
			}
		} else {
			var div = "<div class='label-error' title='" + valor + "'>";
			div += valor + "</div>";
			elemento.insertAdjacentHTML("afterend", div);
		}
	} else {
		elemento.classList.remove("error");
		if (elemento.nextElementSibling != null) {
			if (elemento.nextElementSibling.classList.contains("label-error")) {
				elemento.nextElementSibling.remove();
			}
		}
	}
	return error;
};
var $fnSqx_vInp = function (obj, Mensaje, Obligatorio) {//var vInp = function (obj, Mensaje, Obligatorio)
	if (obj != null) {
		if (Obligatorio) {
			if (obj.nodeName == "DIV") {
				if (obj.classList.contains("js-ms")) {
					var lis = obj.querySelector("#js-ms-ul" + obj.id);
					var vls = "";
					if (lis) {
						var nlis = lis.children.length;
						for (var j = 0; j < nlis; j += 1) {
							vls += lis.children[j].getAttribute("data-v");
							vls += ",";
						}
						vls = vls.substring(0, vls.length - 1);
					}
					if (vls.replace(/^\s+|\s+$/g, "").length == 0) {
						return "Seleccione " + Mensaje;
					}
				}
			} else {
				if (obj.value.replace(/^\s+|\s+$/g, "").length == 0) {
					return (obj.nodeName == "SELECT" ? "Seleccione " : 'Ingrese ') + Mensaje;
				}
				if (obj.getAttribute("data-nd") != null) {
					if (obj.value.trim().length != obj.getAttribute("maxlength")) {
						return Mensaje + " inválido.";
					}
				}
				if (obj.classList.contains("js-vl-correo")) {
					var re = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/
					if (!re.test(obj.value.toLowerCase().trim())) {
						return Mensaje + ' inválido';
					}
				}
				if (obj.classList.contains("js-vl-url")) {
					var re = /^(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})$/
					if (!re.test(obj.value)) {
						return Mensaje + ' inválido';
					}
				}
				if (obj.classList.contains("js-ruc")) {
					if (!(obj.value.trim().length == 11 || obj.value.trim().length == 8)) {
						return Mensaje + ' inválido';
					}
				}
				if (obj.classList.contains("js-vl-nd")) {
					if (!isNaN(obj.value.trim())) {

						if ((obj.value.trim() * 1) <= 0)
							return Mensaje + ' menor o igual acero';
					} else {
						return Mensaje + ' no es un número';
					}

				}
				if (obj.classList.contains("text-date-inp")) {

					if (!validaFecha(obj.value)) {
						return Mensaje + ' inválido';
					}
				}
			}
		}
		if (obj.nodeName == "DIV") {
			console.log();
		} else {
			if (obj.value.replace(/^\s+|\s+$/g, "").length > 0) {
				if (obj.value.match(/([\<])([^\>]{1,})*([\>])/i) != null) {
					return Mensaje + ' No debe contener etiquetas html: <etiqueta>';
				}
			}
		}
	}
	return "";
};
var $fnSqx_limpiarFrm = function (n) {//function limpiarFrm(n)
	var doc = document,
		frm = doc.getElementsByClassName(n),
		n = frm.length, cnt;
	for (var i = 0; i < n; i++) {
		cnt = frm[i];
		switch (cnt.type) {
			case "checkbox":
				cnt.checked = false;
				break;
			case "select-one":
				if (cnt.firstElementChild) {
					cnt.value = cnt.firstElementChild.value;
				} else {
					cnt.value = "";
				}
				break;
			case "textarea":
			case "text":
			case "number":
			case "color":
			case "date":
			case "time":
				if (cnt.classList.contains("text-control-inp")) {
					cnt.value = "";
					cnt.setAttribute("data-v", "");
				} else {
					cnt.value = "";
				}
				break;
		}
		cnt.classList.remove("error");
		if (cnt.nextElementSibling != null) {
			if (cnt.nextElementSibling.classList.contains("label-error")) {
				cnt.nextElementSibling.remove();
			}
		}
	}
};

function DateFormat(d, m, a, frm) {
	var date = new Date();
	if (d != undefined && m != undefined && a != undefined) {
		date = new Date(a, m, d);
	}
	var options = {
		year: "numeric", month: "2-digit", day: "2-digit"
		//TODO: weekday: "long", year: "numeric", month: "short",
		//TODO: day: "numeric", hour: "2-digit", minute: "2-digit"
	};
	var fecha;
	if (frm != undefined) {
		switch (frm) {
			case 1://yyyy-MM-dd
				fecha = date.getFullYear() + "-" + (date.getMonth() + 1 > 9 ? (date.getMonth() + 1) : ("0" + (date.getMonth() + 1))) + "-" + (date.getDate() > 9 ? date.getDate() : ("0" + date.getDate()))
				break;
		}

	} else {
		fecha = date.toLocaleDateString("es-PE", options);
	}

	//TODO: if (isIE || isEdge) {
	//TODO: 	fecha = fecha.replace(/\u200E/g, '');
	//TODO: }


	return fecha;
}
function DatePicker(obj) {
	function crearControl() {

		var contenedor = document.getElementById(obj.contenedor);
		if (contenedor) {
			if (obj.format) {
				contenedor.setAttribute("data-format", obj.format);
			} else {
				contenedor.setAttribute("data-format", "DD/MM/YYYY");
			}
			contenedor.style.position = "relative";
			var html = '';
			if (obj.placeholder) {
				html += "<input placeholder='" + obj.placeholder + "' id='" + obj.id + "' type='text' class='text-none form-control text-date-inp " + obj.form + " " + (obj.requerido ? "required" : "") + "'  " + (obj.requerido ? "validate-date" : "") + " " + (obj.index ? "data-index='" + obj.index + "'" : "") + "/>";
			} else {
				html += "<input placeholder='dd/mm/yyyy' id='" + obj.id + "' type='text' class='text-none form-control text-date-inp " + obj.form + " " + (obj.requerido ? "required" : "") + "'  " + (obj.requerido ? "validate-date" : "") + " " + (obj.index ? "data-index='" + obj.index + "'" : "") + "/>";
			}
			html += "<i class='fa fa-calendar text-control text-control-date hand'></i>";
			contenedor.innerHTML = html;
			var picker = document.querySelector(".picker");
			if (!picker) {

				html = "<div class='picker-wrap'><div class='picker-drop-mask'></div><div class='picker'><div class='picker-head'>";
				html += "<span  class='picker-prev'>&#10094;</span><span  data-f='' class='picker-title' ></span><span  class='picker-next'>&#10095;</span>";
				html += "</div><div class='picker-body'><table id='tbPicker'></table></div></div></div>";

				var body = document.getElementsByTagName("body")[0];
				body.insertAdjacentHTML("beforeend", html);
			}
			init();
		}
	}

	function init() {
		var divContenido = document.querySelector(".picker-wrap"),
			divpicker = document.querySelector(".picker"),
			spnAnterior = document.querySelector(".picker-prev"),
			spnTitulo = document.querySelector(".picker-title"),
			spnsiguiente = document.querySelector(".picker-next"),
			tbPicker = document.getElementById("tbPicker"),
			divMask = document.querySelector(".picker-drop-mask"),
			esActivo = false,
			opcion = 2,
			parametros = {
				aMeses: ["", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
				aDiaSemana: ["Lun.", "Mar.", "Mie.", "Jue.", "Vie.", "Sab.", "Dom."],
				aMesesCorto: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
				anioMin: 1900,
				anioMax: 2030
			},
			inpFecha = document.getElementById(obj.id),
			iconfecha = inpFecha.nextElementSibling;
		var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
		var idioma = (obj.idioma ? obj.idioma : 'es-PE');


		if (inpFecha) {

			iconfecha.onclick = inpFecha.onclick = function (e) {
				if (divContenido.style.display != "none" && divContenido.style.display != "") {
					divContenido.style.display = "none";
					divMask.style.display = "none";
					divContenido.style.overflow = "hidden";
					divpicker.style.cssText = "transform:translateY(-170px);transition:transform 0.1s ease-out";
					return false;

				}
				esActivo = true;
				var control = (e.target || e.srcElement);
				var inp = (control.type == "text" ? control : (control.previousElementSibling.classList.contains("label-error") ? control.previousElementSibling.previousElementSibling : control.previousElementSibling));
				clickpicker(inp);
				eventos();
				opcion = 2;
				var cor = inp.getBoundingClientRect();
				divContenido.style.cssText = "display:block";
				divMask.style.display = "block";
				var corwrap = divContenido.getBoundingClientRect();
				var hventana = window.innerHeight, wventana = window.innerWidth;
				var top = cor.top + cor.height;
				if ((hventana - top) < corwrap.height) {
					top = cor.top - corwrap.height;
				}
				var valorFecha = inp.value.trim();
				if (valorFecha != "" && validaFecha(valorFecha)) {
					fechaActual(valorFecha, 0);
				} else {
					var fecha = new Date();
					fechaActual(fecha.toLocaleDateString(), 0);
				}
				divContenido.style.cssText = "display:block;position:absolute;top:" + (top) + "px;left:" + (cor.left) + "px;overflow:hidden;Z-INDEX: 10000;";

				divpicker.style.transform = "transform:translateY(-170px)";
				divpicker.style.cssText = "transition:transform 0.2s ease-out";
				requestAnimationFrame(function () {
					divpicker.style.transform = "translateY(0px)";
				});
				divpicker.addEventListener("transitionend", habilitarpicker);
				e.stopPropagation();
			};
		}
		function habilitarpicker() {
			divContenido.style.overflow = "visible";
			divpicker.removeEventListener("transitionend", habilitarpicker);
		}
		function eventos() {

			spnAnterior.onclick = function () {
				esActivo = false;
				if (opcion == 2) {
					var mesanio = spnTitulo.getAttribute("data-f");
					var fecha = "1/" + mesanio.replace("-", "/");
					fechaActual(fecha, 1);
				} else if (opcion == 1) {
					var anio = spnTitulo.getAttribute("data-f").split("-")[1] * 1 - 1;
					if (anio < parametros.anioMin || anio > parametros.anioMax) {
						return false
					}
					fechaActual(null, -1, anio);
				} else {
					var mesanio = spnTitulo.getAttribute("data-f").split("-");
					fechaActual(null, -1, (mesanio[0] * 1 - 1));
				}

			};
			spnTitulo.onclick = function (e) {

				opcion <= 0 ? (opcion = 0) : opcion--;
				fechaActual(null, -1);
				e.preventDefault();
				e.stopPropagation();

			};
			spnsiguiente.onclick = function () {
				esActivo = false;
				if (opcion == 2) {
					var mesanio = spnTitulo.getAttribute("data-f");
					var fecha = "1/" + mesanio.replace("-", "/");
					fechaActual(fecha, 2);
				} else if (opcion == 1) {
					var anio = spnTitulo.getAttribute("data-f").split("-")[1] * 1 + 1;
					if (anio < parametros.anioMin || anio > parametros.anioMax) {
						return false
					}
					fechaActual(null, -1, anio);
				} else {
					var mesanio = spnTitulo.getAttribute("data-f").split("-");
					fechaActual(null, -1, (mesanio[1] * 1 + 1));
				}
			};
			divMask.onclick = function () {
				cerraPicker();
			};
		}
		function cerraPicker() {

			divMask.style.display = "none";
			divContenido.style.overflow = "hidden";
			divpicker.style.cssText = "transform:translateY(-170px);transition:transform 0.1s ease-out";
			divpicker.addEventListener("transitionend", cerraPickertransition);
			esActivo = false;

		}
		var clickpicker = function (inp) {
			tbPicker.onclick = function (e) {
				var td = e.target || e.srcElement;
				if (td.nodeName == "TD") {
					var fecha = td.getAttribute("data-d");
					if (fecha) {
						if (opcion == 2) {
							inp.value = fecha;
							inp.focus();
							divContenido.style.overflow = "hidden";
							divpicker.style.cssText = "transform:translateY(-170px);transition:transform 0.1s ease-out";
							divpicker.addEventListener("transitionend", cerraPickertransition);
							esActivo = false;
						} else if (opcion == 1) {
							opcion = 2;
							fechaActual(fecha, 0);
						} else {
							opcion = 1;
							fechaActual(null, -1, fecha * 1);
						}
					}

				}
				e.stopPropagation();
			};
		};
		function cerraPickertransition() {
			divContenido.style.cssText = "display:none";
			esActivo = false;
			divpicker.removeEventListener("transitionend", cerraPickertransition);
		}
		function fechaActual(fecha, tipo, anio) {
			var objFech = {
				diaInicioMesAnterior: undefined,
				diaFinMesAnterior: undefined,
				mesAnterior: undefined,
				anioAnterior: undefined,
				diaActual: undefined,
				mesActual: undefined,
				anioActual: undefined,
				diasMesActual: undefined,
				mesSiguiente: undefined,
				anioSiguiente: undefined,
				opcion: opcion,
				numfilas: 5
			};
			if (tipo != undefined && tipo != -1) {
				var aFecha = [], fecha;
				switch (tipo) {
					case 0:
						aFecha = fecha.split("/");
						var dia = aFecha[0];
						var mes = aFecha[1];
						var anio = aFecha[2];
						objFech.diaActual = parseInt(dia);
						objFech.mesActual = parseInt(mes);
						objFech.anioActual = parseInt(anio);

						if (objFech.anioActual < parametros.anioMin || objFech.anioActual > parametros.anioMax) {
							return false;
						}
						break;
					case 1:
						aFecha = fecha.split("/");
						var mes = aFecha[1];
						var anio = aFecha[2];

						if (parseInt(aFecha[1]) == 1) {
							objFech.diaActual = parseInt(1);
							objFech.mesActual = parseInt(12);
							objFech.anioActual = parseInt(anio) - 1;
						} else {
							objFech.diaActual = parseInt(1);
							objFech.mesActual = parseInt(mes) - 1;
							objFech.anioActual = parseInt(anio);
						}
						if (objFech.anioActual < parametros.anioMin || objFech.anioActual > parametros.anioMax) {
							return false;
						}
						break;

					case 2:
						aFecha = fecha.split("/");
						var mes = aFecha[1];
						var anio = aFecha[2];
						if (parseInt(aFecha[1]) == 12) {
							objFech.diaActual = parseInt(1);
							objFech.mesActual = parseInt(1);
							objFech.anioActual = parseInt(anio) + 1;
						} else {
							objFech.diaActual = parseInt(1);
							objFech.mesActual = parseInt(mes) + 1;
							objFech.anioActual = parseInt(anio);
						}
						break;
				}

				fecha = new Date(objFech.anioActual, objFech.mesActual - 1, 1);
				var esBisiesto = diaFeb(objFech.anioActual);
				var adiaMes = DaysArray(12, esBisiesto);
				objFech.diasMesActual = adiaMes[objFech.mesActual];
				var posdia = fecha.getDay();
				posdia == 0 ? (posdia = 6) : posdia--;
				if (posdia == 0) {
					objFech.diaInicioMesAnterior = 1;
				} else {
					if (objFech.mesActual == 1) {
						esBisiesto = diaFeb(objFech.anioActual - 1);
						adiaMes = DaysArray(12, esBisiesto);

						objFech.diaInicioMesAnterior = adiaMes[12] - (posdia - 1);
						objFech.diaFinMesAnterior = adiaMes[12];
						objFech.mesAnterior = 12;
						objFech.anioAnterior = objFech.anioActual - 1;
					} else {
						objFech.diaInicioMesAnterior = adiaMes[objFech.mesActual - 1] - (posdia - 1);
						objFech.diaFinMesAnterior = adiaMes[objFech.mesActual - 1];
						objFech.mesAnterior = objFech.mesActual - 1;
						objFech.anioAnterior = objFech.anioActual;
					}
				}
				objFech.mesSiguiente = (objFech.mesActual + 1) > 12 ? 1 : objFech.mesActual + 1;
				objFech.anioSiguiente = (objFech.mesActual + 1) > 12 ? (objFech.anioActual + 1) : objFech.anioActual;
				objFech.numfilas = ((objFech.diasMesActual) + (objFech.diaFinMesAnterior - objFech.diaInicioMesAnterior) + 1) > 35 ? 6 : 5;
				spnTitulo.setAttribute("data-f", (objFech.mesActual + "-" + objFech.anioActual));
				spnTitulo.innerHTML = (parametros.aMeses[objFech.mesActual] + "-" + objFech.anioActual);
			} else {
				objFech.numfilas = 4;
				if (opcion == 1) {
					var mesanio = spnTitulo.getAttribute("data-f").split("-");
					objFech.anioActual = anio == undefined ? (mesanio[1] * 1) : anio;

					spnTitulo.setAttribute("data-f", (1 + "-" + (anio == undefined ? objFech.anioActual : anio)));
					spnTitulo.innerHTML = (anio == undefined ? objFech.anioActual : anio);

				} else {
					var aniov;
					if ((spnTitulo.innerHTML.indexOf("-") > -1) && opcion == 0 && anio == undefined) {
						return false;
					}
					if (anio == undefined) {
						aniov = Math.floor((spnTitulo.innerHTML * 1 / 10)) * 10;
					} else {
						aniov = Math.floor((anio * 1 / 10)) * 10;
					}
					objFech.anioActual = aniov;

					spnTitulo.setAttribute("data-f", (aniov + "-" + (aniov + 11)));
					spnTitulo.innerHTML = aniov + "-" + (aniov + 11);
				}
			}
			llenarMes(objFech);
		}

		function llenarMes(obj) {
			var html = "", numcol = obj.opcion == 2 ? 7 : 3;
			var cd = obj.diaInicioMesAnterior, esPrimero = true, esMesSig = false, cmes = 0, fechatitle;
			if (obj.opcion == 2) {
				html += "<thead><tr>";
				for (var i = 0; i < parametros.aDiaSemana.length; i++) {
					html += "<td>";
					html += parametros.aDiaSemana[i];
					html += "</td>";
				}
				html += "</tr></thead>";
			}
			for (var i = 0; i < obj.numfilas; i++) {
				html += "<tr>";
				for (var j = 0; j < numcol; j++) {
					html += "<td data-d='";
					if (obj.opcion == 2) {

						if (i == 0) {
							if (cd <= obj.diaFinMesAnterior && esPrimero) {
								fechatitle = new Date(obj.anioAnterior, (obj.mesAnterior - 1), cd);
								html += (cd > 9 ? cd : "0" + cd);
								html += "/";
								html += (obj.mesAnterior > 9 ? obj.mesAnterior : "0" + obj.mesAnterior);
								html += "/";
								html += obj.anioAnterior;
								html += "' style='color:#6d6666b5' title='" + fechatitle.toLocaleDateString(idioma, options) + "' >";
							} else {
								if (esPrimero) {
									cd = 1;
									esPrimero = false;
								}
								fechatitle = new Date(obj.anioActual, (obj.mesActual - 1), cd);
								html += (cd > 9 ? cd : "0" + cd);
								html += "/";
								html += (obj.mesActual > 9 ? obj.mesActual : "0" + obj.mesActual);
								html += "/";
								html += obj.anioActual;
								html += "' title='" + fechatitle.toLocaleDateString(idioma, options) + "'";
								html += ((cd == obj.diaActual) ? " class='dia' >" : ">");
							}
							html += cd;
							html += "</td>";
							cd++;

						} else {
							if (i == obj.numfilas - 1 && cd < 7) {
								fechatitle = new Date(obj.anioSiguiente, (obj.mesSiguiente - 1), cd);
								html += (cd > 9 ? cd : "0" + cd);
								html += "/";
								html += (obj.mesSiguiente > 9 ? obj.mesSiguiente : "0" + obj.mesSiguiente);
								html += "/";
								html += obj.anioSiguiente;
								html += "' title='" + fechatitle.toLocaleDateString(idioma, options) + "'" + (esMesSig ? " style='color:#6d6666b5'" : "") + ">";

							} else {
								fechatitle = new Date(obj.anioActual, (obj.mesActual - 1), cd);
								html += (cd > 9 ? cd : "0" + cd);
								html += "/";
								html += (obj.mesActual > 9 ? obj.mesActual : "0" + obj.mesActual);
								html += "/";
								html += obj.anioActual;
								html += "' title='" + fechatitle.toLocaleDateString(idioma, options) + "'";
								html += ((cd == obj.diaActual && !esMesSig) ? "class='dia' >" : (esMesSig ? " style='color:#6d6666b5'>" : ">"));
							}
							html += cd;
							html += "</td>";
							cd++;
							if (i != 0) {
								if (cd > obj.diasMesActual) {
									cd = 1;
									esMesSig = true;
								}
							}
						}
					} else {
						if (obj.opcion == 1) {
							html += "01";
							html += "/";
							html += ((cmes + 1) > 9 ? (cmes + 1) : "0" + (cmes + 1));
							html += "/";
							html += obj.anioActual;
							html += "' >";
							html += parametros.aMesesCorto[cmes];
							html += "</td>";
							cmes++;
						} else {
							html += obj.anioActual;
							html += "' >";
							html += obj.anioActual;
							html += "</td>";
							obj.anioActual++;
						}
					}
				}
				html += "</tr>";
			}
			tbPicker.innerHTML = html;
		}
	}

	return { crear: function () { return crearControl(); } };
}
function diaFeb(anio) {
	return (((anio % 4 == 0) && ((!(anio % 100 == 0)) || (anio % 400 == 0))) ? 29 : 28);
}
function DaysArray(n, b) {
	var ar = [];
	for (var i = 1; i <= n; i++) {
		ar[i] = 31;
		if (i == 4 || i == 6 || i == 9 || i == 11) { ar[i] = 30; }
		if (i == 2) { b ? (ar[i] = 28) : (ar[i] = 29); }
	}
	return ar;
}

function validaFecha(fecha) {
	var dtCh = "/";
	var minYear = 1900;
	var maxYear = 2100;
	function esEntero(s) {
		var i, c;
		for (i = 0; i < s.length; i++) {
			c = s.charAt(i);
			if (((c < "0") || (c > "9"))) { return false; }
		}
		return true;
	}
	function stripCharsInBag(s, bag) {
		var i, c;
		var returnString = "";
		for (i = 0; i < s.length; i++) {
			c = s.charAt(i);
			if (bag.indexOf(c) == -1) { returnString += c; }
		}
		return returnString;
	}
	function diaFeb(anio) {
		return (((anio % 4 == 0) && ((!(anio % 100 == 0)) || (anio % 400 == 0))) ? 29 : 28);
	}
	function DaysArray(n) {
		var ar = [];
		for (var i = 1; i <= n; i++) {
			ar[i] = 31;
			if (i == 4 || i == 6 || i == 9 || i == 11) { ar[i] = 30 }
			if (i == 2) { ar[i] = 29 }
		}
		return ar;
	}
	function esFecha(dtStr) {
		var diasmes = DaysArray(12),
			pos1 = dtStr.indexOf(dtCh),
			pos2 = dtStr.indexOf(dtCh, pos1 + 1),
			strdia = dtStr.substring(0, pos1),
			strmes = dtStr.substring(pos1 + 1, pos2),
			stranio = dtStr.substring(pos2 + 1);
		var strYr = stranio;
		if (strdia.charAt(0) == "0" && strdia.length > 1) { strdia = strdia.substring(1); }
		if (strmes.charAt(0) == "0" && strmes.length > 1) { strmes = strmes.substring(1); }
		for (var i = 1; i <= 3; i++) {
			if (strYr.charAt(0) == "0" && strYr.length > 1) { strYr = strYr.substring(1); }
		}
		var mes = parseInt(strmes),
			dia = parseInt(strdia),
			anio = parseInt(strYr);
		if (pos1 == -1 || pos2 == -1) {
			return false;
		}
		if (strmes.length < 1 || mes < 1 || mes > 12) {
			return false;
		}
		if (strdia.length < 1 || dia < 1 || dia > 31 || (mes == 2 && dia > diaFeb(anio)) || dia > diasmes[mes]) {
			return false;
		}
		if (stranio.length != 4 || anio == 0 || anio < minYear || anio > maxYear) {
			return false;
		}
		if (dtStr.indexOf(dtCh, pos2 + 1) != -1 || esEntero(stripCharsInBag(dtStr, dtCh)) == false) {
			return false;
		}
		return true;
	}
	if (esFecha(fecha)) {
		return true;
	} else {
		return false;
	}
}

//TODO: UBIGEO
function fnllenarUbigeo(l, c, ps, d, p, ds) {
	var contenido = "";
	var n = l.length, obj;
	var cnt = contenido = "<option value=''>Seleccione</option>";
	for (var i = 0; i < n; i++) {
		obj = l[i].split("¦");
		if (ps != undefined && d == undefined && p == undefined && ds == undefined) {
			if (obj[1] == "00" && obj[2] == "00" && obj[3] == "00") {
				cnt += contenido = "<option value='" + obj[0] + "'>" + obj[4] + "</option>";
			}
		}
		else if (ps != undefined && d != undefined && p == undefined && ds == undefined) {
			if (obj[0] == ps && obj[1] != "00" && obj[2] == "00" && obj[3] == "00") {
				cnt += contenido = "<option value='" + obj[1] + "'>" + obj[4] + "</option>";
			}
		} else if (ps != undefined && d != undefined && p != undefined && ds == undefined) {
			if (ps == obj[0] && d == obj[1] && obj[2] != "00" && obj[3] == "00") {
				cnt += contenido = "<option value='" + obj[2] + "'>" + obj[4] + "</option>";
			}
		} else {
			if (ps == obj[0] && obj[1] == d && obj[2] == p && obj[3] != "00") {
				cnt += contenido = "<option value='" + obj[3] + "'>" + obj[4] + "</option>";
			}
		}
	}
	document.getElementById(c).innerHTML = cnt;
}

var servicioFile = function (obj) {
	var inicio = 0,
		archivo = obj.file,
		size = 1024 * (obj.mbEnvio == undefined ? 20 : obj.mbEnvio),
		longitud = archivo.size;
	var fin = size,
		blob = new Blob([archivo]),
		bloque = blob.slice(inicio, fin),
		archivoGrabar = (obj.ruta == undefined ? archivo.name : obj.ruta),
		estado = "";

	var upload = function (obj) {
		return new Promise(function (resolve, reject) {
			var req = new XMLHttpRequest();
			var ur = document.getElementById("ur").value;
			obj.type = (obj.type ? obj.type : "Post");
			obj.responseType = (obj.responseType ? obj.responseType.toLowerCase() : "text");
			req.open(obj.type, ur + obj.url, true);
			req.setRequestHeader("archivo", archivoGrabar);

			req.onload = function () {
				if (req.status == 200) {
					switch (obj.responseType) {
						case "":
						case "document":
						case "text":
							resolve(req.response);
							break;
						case "blob":
							resolve(req.response);
							break;
						case "arraybuffer":
							resolve(req.response);
							break;
						case "json":
							resolve(JSON.parse(req.response));
							break;
					}
				}
				else {
					reject(req.statusText);
				}
			};
			req.onerror = function () {
				Console.log("Network Error");
			};
			//TODO: if (obj.type.toLowerCase() == "post") { var frm = new FormData(); frm.append("data", obj.data); obj.data = frm; }
			req.send((bloque ? bloque : null));
		});
	};
	function resultado(d) {
		if (d != "") {
			inicio = fin;
			fin = inicio + size;
			if (inicio < longitud) {
				bloque = blob.slice(inicio, fin);
				upload(obj).then(resultado);
			}
			else {
				estado = "1";
			}
		} else {
			estado = "-1";
		}
	}
	return new Promise(function (resolve, reject) {
		upload(obj).then(resultado);

		var hilo = setInterval(function () {

			if (estado == "1") {
				clearInterval(hilo);
				resolve(estado);
			}
			if (estado == "-1") {
				clearInterval(hilo);
				reject(estado);
			}

		}, 100);

	});
};
//TODO: TABS DE MASIVOS
function mostrarTabs(actual, ultab) {
	var tabs = document.getElementById(ultab);
	var listaTabs = tabs.getElementsByTagName("li");
	var contenido;
	var data_tab, data_tab_actual;
	for (var i = 0; i < listaTabs.length; i++) {
		data_tab = listaTabs[i].getAttribute("data-tab");
		data_tab_actual = actual.getAttribute("data-tab");
		contenido = document.getElementById(data_tab);
		if (data_tab == data_tab_actual) {
			listaTabs[i].className = "tab-link current";
			contenido.className = "tab-content current";
		}
		else {
			if (listaTabs[i].className.indexOf("bloqueado") > -1) listaTabs[i].className = "tab-link bloqueado";
			else listaTabs[i].className = "tab-link";
			contenido.className = "tab-content";
		}
	}
}

//GETDATA

function getData(fo) {
	var doc = document,
		frm = doc.getElementsByClassName(fo),
		n = frm.length, cnt, str = "";

	for (var i = 0; i < n; i += 1) {
		cnt = frm[i];
		if (cnt.nodeName == "DIV") {
			if (cnt.classList.contains("js-ms")) {
				var lis = cnt.querySelector("#js-ms-ul" + cnt.id);
				if (lis) {
					var nlis = lis.children.length, vls = "";
					for (var j = 0; j < nlis; j += 1) {
						vls += lis.children[j].getAttribute("data-v");
						vls += ",";
					}
					vls = vls.substring(0, vls.length - 1);
				}
				str += vls + "¦";
			}
		} else if (cnt.nodeName == "IMG") {
			str += cnt.src + "¦";
		} else {
			switch (cnt.type) {
				case "checkbox":
					str += (cnt.checked ? "1¦" : "0¦");
					break;
				case "select-one":
					str += (cnt.value == "" ? "¦" : (cnt.value + "¦"));
					break;
				case "textarea":
				case "text":
				case "number":
				case "color":
				case "date":
					if (cnt.classList.contains("text-control-inp")) {
						str += cnt.getAttribute("data-v") + "¦";
					} else if (cnt.classList.contains("js-vl-url")) {
						str += ((cnt.value.trim().toLowerCase().indexOf("http:") == -1 || cnt.value.trim().toLowerCase().indexOf("https:") == -1) ? ("http://" + cnt.value.trim()) : cnt.value.trim()) + "¦";
					} else if (cnt.classList.contains("input-label")) {
						str += (cnt.value.trim().replace(/\,|(\$.|S\/.)/ig, "")) + "¦";
					} else {
						str += (cnt.value.trim().replace(/(¬|¦)/ig, "")) + "¦";
					}
					break;
			}
		}
	}
	str = str.substring(0, str.length - 1);
	return str;
}

//INPUTLIST AUN FALTA HACER ALGUNOS CAMBIOS (NO FUNCIONA CORRECTAMENTE)

function InputList(obj) {

	var objs = [];
	function crearControl() {

		var contenedor = document.getElementById(obj.contenedor);
		if (contenedor) {
			contenedor.style.position = "relative";

			var html = "";

			switch (obj.type) {

				case "text":
					html += "<input id='inp" + obj.id + "' type='text' disabled class='form-control text-control-inp " + obj.form + "'  " + (obj.requerido ? "data-vl='" + obj.mensaje + "'" : "") + " data-v='' " + (obj.index ? "data-index='" + obj.index + "'" : "") + "/>";
					break;
				case "textarea":
					html += "<textarea id='inp" + obj.id + "' disabled class='form-control text-control-inp " + obj.form + "'  " + (obj.requerido ? "data-vl='" + obj.mensaje + "'" : "") + " data-v='' " + (obj.index ? "data-index='" + obj.index + "'" : "") + "></textarea>";
					break;
				default:
					html += "<input id='inp" + obj.id + "' type='text' disabled class='form-control text-control-inp " + obj.form + "'  " + (obj.requerido ? "data-vl='" + obj.mensaje + "'" : "") + " data-v='' " + (obj.index ? "data-index='" + obj.index + "'" : "") + "/>";
					break;
			}

			html += "<i id='is" + obj.id + "' class='fa fa-search text-popup text-control text-control-search default-primary-color text-primary-color' data-ev='" + obj.id + "' ></i>";
			html += "<i  id='it" + obj.id + "'  class='fa fa-trash-o text-popup text-control text-control-trash default-primary-color text-primary-color' data-ev='" + obj.id + "' ></i>"
			contenedor.innerHTML = html;
			if (buscarObj(obj.id) == -1) {
				objs.push(obj);
			}
			init(obj);
		}
	}
	function buscarObj(id) {
		var pos = -1;
		var n = objs.length, ob;
		for (var i = 0; i < n; i += 1) {
			ob = objs[i];
			if (id == ob.id) {
				pos = i;
				break;
			}
		}
		return pos;
	}
	function eventTr(row) {
		var inp = document.getElementById("inp" + this.id);
		inp.value = row[1];
		inp.setAttribute("data-v", row[0]);
		$$modal('viewPopup' + this.id).hide();
		//crearPopup().hide("viewPopup" + this.id);
	}
	function init(obj) {

		var doc = document,
			is = doc.getElementById("is" + obj.id),
			it = doc.getElementById("it" + obj.id);


		it.onclick = function () {
			var ev = this.getAttribute("data-ev");
			var cnt = document.getElementById("inp" + ev);
			if (cnt) {
				cnt.value = "";
				cnt.setAttribute("data-v", '');
			}
		}
		var evt = function () {
			var body = document.getElementsByTagName("body")[0];
			var ev = this.getAttribute("data-ev");
			var pos = buscarObj(ev);
			var o = objs[pos];

			$$modal(ev).create({
				WithScrollY: true,
				Width: 'lm',
				Title: o.titulo,
				ButtonNames: ['', ''],
				ButtonClass: ['', ''],
				FnEvents: ['', ''],
				Namespace: ''
			});

			//crearPopup({ contenedor: ev, ancho: o.ancho, esPopup: true, view: o.view, titulo: o.titulo }).crear();


			var popup = document.getElementsByTagName("viewPopup" + ev);

			if (popup) {
				var input = this.previousElementSibling.getAttribute("data-op");
				if (input) {
					o.arg.opcional = input;
				}
				o.arg.tabla.eventRow = (o.event && typeof o.event == "function" ? o.event.bind(o) : eventTr.bind(o));
				var module = o.module;
				//app.getPagina(o.view, 1, o.arg, true);
				//goSubPage(o.view, 'view-Main', { service: { idIndiceRegistroAtras: window.idRegistro } });
				module.goSubPage(o.view, 'view-Main', o.arg, true);
				$$modal("viewPopup" + ev).show();
				//crearPopup().show("viewPopup" + ev);
			}
		}
		is.onclick = evt;
	}
	return { crear: function () { return crearControl(); } }
}

//GRAFICOS

function Graficos(alto, ancho, tipo, cont, par) {

	this.alto = (alto == undefined ? 300 : alto);
	this.ancho = (ancho == undefined ? 400 : ancho);
	this.type = tipo;
	this.idcont = cont;
	this.graphWidth = 0;
	this.graphHeight = 0;
	this.coords = [];
	this.scale = {};
	this.max = 0;
	this.min = 0;
	this.data = [];
	this.par = par == undefined ? false : true;
	this.color = ["#2c59e2", "#249432", "#62a083", "#cdae5e", "#daed0b", "#8fc2db", "#22195b"];
	this.properties = {
		gutterLeft: 60,
		gutterRight: 30,
		gutterTop: 15,
		gutterBottom: 55,
		backgroundGrid: true,
		backgroundGridColor: '#ddd',
		strokestyle: 'rgba(0,0,0,0)',
		hmargin: 3,
		vmargin: 3,
		yaxis: true,
		yaxisTickmarksLength: 3,
		yaxisScale: true,
		yaxisLabels: null,
		yaxisLabelsOffsetx: 0,
		yaxisLabelsOffsety: 0,
		yaxisLabelsCount: 7,
		yaxisUnitsPre: '',
		yaxisUnitsPost: '',
		yaxisRound: false,
		yaxisMax: null,
		yaxisMin: 0,
		xaxisLabels: null,
		xaxisLabelsOffsetx: 0,
		xaxisLabelsOffsety: 0,
		vmarginTop: 0,
		vmarginBottom: 0,
		variant3dOffsetx: 0,
		variant3dOffsety: 0

	};
	this.dibujar = function (data) {
		data = this.crearMatriz(data);
		var contenido = "<svg class='svg-gr'  xmlns='http://www.w3.org/2000/svg' width='80%' height='70%' viewBox='0 0 " + this.ancho + " " + this.alto + "'>";//+ this.ancho + " + this.alto + "
		contenido += "<g class='nvd3 nv-wrap nv-lineChart'>";
		contenido += "<g>";
		switch (this.type) {
			case "Pie":
				var ri = -Math.PI / 2;
				var rotateAnimation = 1;
				var r = (this.ancho / 4);
				var cx = ((this.ancho / 2) - r) + 20;
				var cy = (this.alto) / 2;
				var segmentAngle, cmd, reg, rf, largeArc, startX, startY, endX, endY, color;
				var Tl = this.obtenerMayorValorY(data, true);
				var af = [], ap = [];
				var nR = data.length, strLeyenda = "<g transform ='translate(" + (this.ancho - 230) + ",30)'>";
				if (Tl.ymax != Tl.ymin) {
					for (var i = 0; i < nR; i += 1) {
						reg = data[i];
						segmentAngle = rotateAnimation * ((reg[1] / Tl.ymax) * (Math.PI * 2));
						rf = ri + segmentAngle;

						largeArc = (((rf - ri) % (Math.PI * 2)) > Math.PI ? 1 : 0);
						startX = cx + Math.cos(ri) * r;
						startY = cy + Math.sin(ri) * r;
						endX = cx + Math.cos(rf) * r;
						endY = cy + Math.sin(rf) * r;
						color = (reg[2] ? reg[2] : (this.color[i] ? this.color[i] : '#' + Math.floor(Math.random() * 16777215).toString(16)));
						cmd = [
							'M', startX, startY,//Mover puntero
							'A', r, r, 0, largeArc, 1, endX, endY,//Dibujar arco
							'L', cx, cy,//Mover al centro
							'Z'//Cerrar ruta
						];
						contenido += "<g><path id='circle" + (this.idcont + i) + "' class='grafico' data-v='";
						contenido += reg.join("¦");
						contenido += "' style='stroke-width:2; stroke:White; fill:";
						contenido += color;
						contenido += "' d='";
						contenido += cmd.join(' ');
						contenido += "'></path>";

						contenido += "<path id='sombra" + (this.idcont + i) + "' style='stroke-width:0; stroke:" + color + ";stroke-opacity:0.4; fill:none' d='";
						contenido += "M" + startX + "," + startY + " A " + r + "," + r + " 0 " + largeArc + ",1" + endX + "," + endY + "'>";
						contenido += "<set attributeName='stroke-width' attributeType='XML' to='15' begin='circle" + (this.idcont + i) + ".mouseover; sombra" + (this.idcont + i) + ".mouseover;valor" + (this.idcont + i) + ".mouseover' end='circle" + (this.idcont + i) + ".mouseleave; sombra" + (this.idcont + i) + ".mouseleave;valor" + (this.idcont + i) + ".mouseleave'></set>";
						contenido += "</path></g>";

						var radio = (1.5 * r) / 2;
						var ac = ri + (rf - ri) / 2;

						startX = cx + Math.cos(ac) * radio;
						startY = cy + Math.sin(ac) * radio;

						contenido += "<text id='valor" + i + "' x='" + startX + "' y='" + (startY) + "' fill='#fff' text-anchor='middle' dominant-baseline='middle'>" + ((reg[1] / Tl.ymax) * 100).toFixed(2) + "</text>"

						strLeyenda += "<rect x='0' y='" + (i * 30) + "' width='20' height='20' fill='" + color + "'></rect><text x='30' y='" + (i * 30 + 18) + "'>" + reg[0] + "</text>"

						ri += segmentAngle;
					}
				} else {
					reg = data[0];
					strLeyenda += "<rect x='0' y='" + (0) + "' width='20' height='20' fill='" + color + "'></rect><text x='30' y='" + (18) + "'>" + reg[0] + "</text>";
					contenido += "<circle class='grafico' data-v='" + reg.join("¦") + "'  cx='" + cx + "' cy='" + cy + "' r='" + r + "' fill='blue'></circle><g>";
				}
				contenido += strLeyenda + "</g>";
				break;
			case "Columna":
			case "Barra":
				this.data = data;
				contenido += this.draw();
				break;
			case "Linea":
				this.data = data;
				contenido += this.draw();
				break;

		}
		contenido += "</g>";
		contenido += "</g>";
		contenido += "</svg>";
		var divcolumnaAgrupada = document.getElementById(this.idcont);
		divcolumnaAgrupada.innerHTML = contenido;

		var path = document.getElementsByClassName("ptline");
		if (path != null) {
			var n = path.length;

			for (var i = 0; i < n; i++) {
				var length = path[i].getTotalLength();
				// Clear any previous transition
				path[i].style.transition = path[i].style.WebkitTransition = 'none';
				// Set up the starting positions
				path[i].style.strokeDasharray = length + ' ' + length;
				path[i].style.strokeDashoffset = length;
				// Trigger a layout so styles are calculated & the browser
				// picks up the starting position before animating
				path[i].getBoundingClientRect();
				// Define our transition
				path[i].style.transition = path[i].style.WebkitTransition = 'stroke-dashoffset 2s ease-in-out';
				// Go!
				path[i].style.strokeDashoffset = '0';
			}
		}
	};
	this.crearMatriz = function (data) {
		var nreg = data.length, mt = [], cmp, ncmp;
		for (var i = 0; i < nreg; i += 1) {
			cmp = data[i].split("¦");
			ncmp = cmp.length;
			mt[i] = [];
			for (var j = 0; j < ncmp; j += 1) {

				if (isNaN(cmp[j])) { mt[i][j] = cmp[j]; }
				else { mt[i][j] = cmp[j] * 1; }
			}
		}
		return mt;
	};
	this.draw = function () {
		var str = "";
		var prop = this.properties;
		prop.variant3dOffsetx = 0;
		prop.variant3dOffsety = 0;

		this.coords = [];
		this.graphWidth = this.ancho - prop.gutterLeft - prop.gutterRight;
		this.graphHeight = this.alto - prop.gutterTop - prop.gutterBottom;
		var xy = this.obtenerMayorValorY(this.data, false);
		this.scale = this.getScale({
			object: this,
			numlabels: prop.yaxisLabelsCount,
			unitsPre: prop.yaxisUnitsPre,
			unitsPost: prop.yaxisUnitsPost,
			max: xy.ymax,
			min: xy.ymin,
			round: prop.yaxisRound,
		});
		this.max = this.scale.max;
		this.min = this.scale.min;
		prop.yaxisMax = this.scale.max;
		prop.yaxisMin = this.scale.min;

		str += this.drawBackground(this);//cadena
		if (this.type == "Linea") {
			str += this.drawLine();
		} else {
			str += this.drawBars();
		}
		str += this.drawXAxis(this);
		str += this.drawYAxis(this);
		return str;
	};
	this.drawYAxis = function (obj) {
		var prop = obj.properties, x;
		if (obj.type == 'Columna') {
			x = obj.getYCoord(prop.xaxisMin > 0 ? prop.xaxisMin : 0);
			if (prop.xaxisMin < 0 && prop.xaxisMax <= 0) {
				x = obj.getYCoord(prop.xaxisMax);
			}
		} else {
			x = prop.gutterLeft;
		}

		var str = "";
		//str += "<path d='M" + x + " " + (prop.gutterTop) + " L" + (x + 0.001) + " " + (obj.alto - prop.gutterBottom) + "' stroke-linecap='square' stroke='black' fill='black'></path>";
		str += "<g class='nv-y nv-axis'>";
		str += "<g class='nvd3 nv-wrap nv-axis'>";
		str += "<g>";
		if (this.type != "Columna") {
			var segment = (obj.alto - prop.gutterTop - prop.gutterBottom) / prop.yaxisLabelsCount;
			for (var i = 0; i < obj.scale.labels.length; ++i) {
				var y = obj.alto - prop.gutterBottom - (segment * i) - segment;
				str += "<g class='tick major'>";
				str += "<line x2='510' y2='0'></line>";
				str += "<text x='" + (prop.gutterLeft - 7 - (prop.yaxis ? (prop.yaxisTickmarksLength - 3) : 0) + prop.yaxisLabelsOffsetx) + "' y='" + (y + prop.yaxisLabelsOffsety) + "' text-anchor='end' style='fill:rgba(87,87,87,.5);font-family:montserrat;font-size:12px;font-weight:400'   >" + obj.scale.labels[i] + "</text>";
				str += "</g>";
			}
		} else {
			prop.yaxisLabels = obj.data;
			for (var i = 0; i < prop.yaxisLabels.length; ++i) {
				var segment = (obj.graphHeight - (prop.vmarginTop || 0) - (prop.vmarginBottom || 0)) / prop.yaxisLabels.length
					, y = prop.gutterTop + (prop.vmarginTop || 0) + (segment * i) + (segment / 2) + prop.yaxisLabelsOffsety
					, x = prop.gutterLeft - 7 - (prop.yaxisLinewidth || 1) + prop.yaxisLabelsOffsetx
					, halign = 'right';
				if (obj.type === 'Columna' && !prop.yaxisLabelsSpecific) {
					var segment = (obj.graphHeight - (prop.vmarginTop || 0) - (prop.vmarginBottom || 0)) / (prop.yaxisLabels.length);
					y = prop.gutterTop + (prop.vmarginTop || 0) + (segment * i) + (segment / 2) + prop.yaxisLabelsOffsetx;
				} else {
					var segment = (obj.graphHeight - (prop.vmarginTop || 0) - (prop.vmarginBottom || 0)) / (prop.yaxisLabels.length - 1);
					y = obj.height - prop.gutterBottom - (segment * i) + prop.yaxisLabelsOffsetx;
				}
				str += "<text x='" + x + "' y='" + y + "'  dominant-baseline='middle' text-anchor='end' font-size='10pt' font-weight='100' font-family='sans-serif'>" + prop.yaxisLabels[i][0] + "</text>";
			}

		}
		str += "</g>";
		str += "</g>";
		str += "</g>";
		return str;
	}
	this.drawXAxis = function (obj) {
		var prop = obj.properties;
		prop.xaxisLabels = obj.data;
		var str = "";
		var y = obj.type == 'Columna' ? (obj.alto - prop.gutterBottom) : obj.getYCoord(obj.scale.min < 0 && obj.scale.max < 0 ? obj.scale.max : (obj.scale.min > 0 && obj.scale.max > 0 ? obj.scale.min : 0));
		//str += "<path d='M" + prop.gutterLeft + " " + (y + 0.01) + " L" + (obj.ancho - prop.gutterRight) + " " + y + "' stroke-linecap='square' stroke='black' fill='black'></path>";
		str += "<g class='nv-x nv-axis'>";
		str += "<g class='nvd3 nv-wrap nv-axis'>";
		str += "<g>";
		if (this.type != "Columna") {
			var segment = (obj.ancho - prop.gutterLeft - prop.gutterRight) / prop.xaxisLabels.length;
			for (var i = 0; i < prop.xaxisLabels.length; ++i) {
				var x = prop.gutterLeft + (segment / 2) + (i * segment);
				if (obj.scale.max <= 0 && obj.scale.min < obj.scale.max) {
					var y = prop.gutterTop - (10) - (prop.xaxisLinewidth || 1) + prop.xaxisLabelsOffsety;
					var valign = 'bottom';
				} else {
					var y = obj.alto - prop.gutterBottom + (10) + (prop.xaxisLinewidth || 1) + prop.xaxisLabelsOffsety;
					var valign = 'hanging';
				}
				var reg = prop.xaxisLabels[i];

				str += "<g class='tick major' style='opacity: 1;'>";
				str += "<line y2='-251' x2='0' style='display: none;'></line>";
				str += "<text x='" + (x + prop.xaxisLabelsOffsetx) + "' y='" + y + "' dominant-baseline='" + valign + "' " + (prop.xaxisLabels.length >= 10 ? ("transform = 'translate(0,0) rotate(-50 " + (x + prop.xaxisLabelsOffsetx) + " " + (y + 35) + ")'") : " ") + "   text-anchor='middle' style='fill:rgba(87,87,87,.5);font-family:montserrat;font-size:12px;font-weight:400'>" + reg[0] + "</text>";
				str += "</g>";
			}
		} else {
			var segment = obj.graphWidth / obj.scale.labels.length;// prop.xaxisLabelsCount;
			for (var i = 0; i < obj.scale.labels.length; ++i) {
				var x = prop.gutterLeft + (segment * i) + segment + prop.xaxisLabelsOffsetx;

				str += "<text x='" + x + "' y='" + ((obj.alto - prop.gutterBottom) + (prop.xaxis ? prop.xaxisTickmarksLength + 6 : 10) + (prop.xaxisLinewidth || 1) + prop.xaxisLabelsOffsety) + "' dominant-baseline='middle' text-anchor='middle' font-size='10pt' font-weight='100' font-family='sans-serif'>" + obj.scale.labels[i] + "</text>";
			}
		}
		str += "</g>";
		str += "</g>";
		str += "</g>";
		return str;
	};
	this.drawBars = function () {
		var prop = this.properties;
		var y = this.getYCoord(0);
		var ma = Math;
		var bars = [], str = "";
		var esPar = this.par, x2, y2, height2, width2;
		for (var i = 0; i < this.data.length; ++i) {
			if (this.type == "Barra") {
				var outerSegment = this.graphWidth / this.data.length
					, height = (ma.abs(this.data[i][1]) - ma.abs(this.scale.min)) / (ma.abs(this.scale.max) - ma.abs(this.scale.min)) * this.graphHeight
					, width = (this.graphWidth / this.data.length) - prop.hmargin - prop.hmargin
					, x = prop.gutterLeft + prop.hmargin + (outerSegment * i);
				if (this.scale.min >= 0 && this.scale.max > 0) {
					y = this.getYCoord(this.scale.min) - height;
				} else if (this.scale.min < 0 && this.scale.max > 0) {
					height = (ma.abs(this.data[i][1]) / (this.scale.max - this.scale.min)) * this.graphHeight;
					y = this.getYCoord(0) - height;
					if (this.data[i][1] < 0) {
						y = this.getYCoord(0);
					}
				} else if (this.scale.min < 0 && this.scale.max < 0) {
					height = (ma.abs(this.data[i][1]) - ma.abs(this.scale.max)) / (ma.abs(this.scale.min) - ma.abs(this.scale.max)) * this.graphHeight;
					y = prop.gutterTop;
				}

				if (esPar) {
					height2 = (ma.abs(this.data[i][2]) - ma.abs(this.scale.min)) / (ma.abs(this.scale.max) - ma.abs(this.scale.min)) * this.graphHeight
						, x2 = prop.gutterLeft + prop.hmargin + (outerSegment * i);

					if (this.scale.min >= 0 && this.scale.max > 0) {
						y2 = this.getYCoord(this.scale.min) - height2;
					} else if (this.scale.min < 0 && this.scale.max > 0) {
						height2 = (ma.abs(this.data[i][2]) / (this.scale.max - this.scale.min)) * this.graphHeight;
						y2 = this.getYCoord(0) - height2;
						if (this.data[i][2] < 0) {
							y2 = this.getYCoord(0);
						}
					} else if (this.scale.min < 0 && this.scale.max < 0) {
						height2 = (ma.abs(this.data[i][2]) - ma.abs(this.scale.max)) / (ma.abs(this.scale.min) - ma.abs(this.scale.max)) * this.graphHeight;
						y2 = prop.gutterTop;
					}
				}
			} else {
				var outerSegment = (this.graphHeight - prop.vmarginTop - prop.vmarginBottom) / this.data.length
					, width = this.getWidth(this.data[i][1])
					, height = ((this.graphHeight - prop.vmarginTop - prop.vmarginBottom) / this.data.length) - prop.vmargin - prop.vmargin
					, x = this.getYCoord((this.scale.min < 0 && this.scale.max < 0) || (this.scale.min > 0 && this.scale.max > 0) ? this.scale.min : 0) - (this.data[i][1] < 0 ? width : 0)
					, y = prop.gutterTop + prop.vmarginTop + prop.vmargin + (outerSegment * i);
				if (this.scale.min < 0 && this.scale.max < 0) {
					x = this.ancho - prop.gutterRight - width;
				}
				if (esPar) {
					width2 = this.getWidth(this.data[i][2])
						, height2 = ((this.graphHeight - prop.vmarginTop - prop.vmarginBottom) / this.data.length) - prop.vmargin - prop.vmargin
						, x2 = this.getYCoord((this.scale.min < 0 && this.scale.max < 0) || (this.scale.min > 0 && this.scale.max > 0) ? this.scale.min : 0) - (this.data[i][2] < 0 ? width : 0)
						, y2 = prop.gutterTop + prop.vmarginTop + prop.vmargin + (outerSegment * i);
					if (this.scale.min < 0 && this.scale.max < 0) {
						x2 = this.ancho - prop.gutterRight - width2;
					}
				}
			}
			if (!esPar) {
				str = "<rect class='grafico' x='" + x + "' y='" + y + "' width='" + width + "' height='" + height + "' storke='1' fill='" + (this.type == "Barra" ? this.data[i][2] : this.data[i][2]) + "' data-t='" + this.type + "' data-v='" + this.data[i].join("¦") + "'>";
				if (this.type == "Barra") {
					str += "<animate attributeType='XML' attributeName='width' from='0' to='" + (width) + "' dur='0.5s' fill='freeze'></animate>";
					// str += "<animate attributeType='XML' attributeName='y' from='" + y + "' to='" + (y - height) + "' dur='0.5s' fill='freeze'></animate>";
				} else {
					str += "<animate attributeType='XML' attributeName='width' from='0' to='" + width + "' dur='0.5s' fill='freeze'></animate>";
				}

				str += "</rect>";
				bars.push(str);
				this.coords.push({
					object: str,
					x: x,
					y: y - (this.data[i][1] > 0 ? height : 0),
					width: width,
					height: height
				});
			} else {
				if (this.type == "Barra") {
					var anchorelativo = width / 2;
					str = "<rect class='grafico' x='" + x + "' y='" + y + "' width='" + anchorelativo + "' height='" + height + "' storke='1' fill='" + (this.type == "Barra" ? this.data[i][2] : this.data[i][2]) + "' data-t='" + this.type + "' data-v='" + this.data[i].join("¦") + "'>";
					str += "<animate attributeType='XML' attributeName='width' from='0' to='" + (anchorelativo) + "' dur='0.5s' fill='freeze'></animate>";
					str += "</rect>";

					str += "<rect class='grafico' x='" + (x + anchorelativo) + "' y='" + y2 + "' width='" + anchorelativo + "' height='" + height2 + "' storke='1' fill='#f90909' data-t='" + this.type + "2' data-v='" + this.data[i].join("¦") + "'>";
					str += "<animate attributeType='XML' attributeName='width' from='0' to='" + (anchorelativo) + "' dur='0.5s' fill='freeze'></animate>";
					str += "</rect>";
				} else {
					var altorelativo = height / 2;
					str = "<rect class='grafico' x='" + x + "' y='" + y + "' width='" + width + "' height='" + altorelativo + "' storke='1' fill='" + (this.type == "Barra" ? this.data[i][2] : this.data[i][2]) + "' data-t='" + this.type + "' data-v='" + this.data[i].join("¦") + "'>";
					str += "<animate attributeType='XML' attributeName='width' from='0' to='" + width + "' dur='0.5s' fill='freeze'></animate>";
					str += "</rect>";
					str += "<rect class='grafico' x='" + x2 + "' y='" + (y + altorelativo) + "' width='" + width2 + "' height='" + altorelativo + "' storke='1' fill='#f90909' data-t='" + this.type + "2' data-v='" + this.data[i].join("¦") + "'>";
					str += "<animate attributeType='XML' attributeName='width' from='0' to='" + (width2) + "' dur='0.5s' fill='freeze'></animate>";
					str += "</rect>";
				}



				bars.push(str);

			}
		}
		return bars.join(" ");
	}
	this.drawLine = function () {
		var prop = this.properties;
		var y = this.getYCoord(0);
		var ma = Math;
		var lines = [], lines2 = [], str = "";
		var circles = [], circles2 = [];
		var espar = this.par, y2 = 0, height2 = 0;
		for (var i = 0; i < this.data.length; ++i) {

			var outerSegment = this.graphWidth / this.data.length
				, height = (ma.abs(this.data[i][1]) - ma.abs(this.scale.min)) / (ma.abs(this.scale.max) - ma.abs(this.scale.min)) * this.graphHeight
				, width = (this.graphWidth / this.data.length) - prop.hmargin - prop.hmargin
				, x = prop.gutterLeft + prop.hmargin + (outerSegment * i);
			if (espar) {
				height2 = (ma.abs(this.data[i][2]) - ma.abs(this.scale.min)) / (ma.abs(this.scale.max) - ma.abs(this.scale.min)) * this.graphHeight;
			}

			if (this.scale.min >= 0 && this.scale.max > 0) {
				y = this.getYCoord(this.scale.min) - height;
				if (espar) {
					y2 = this.getYCoord(this.scale.min) - height2;
				}
			} else if (this.scale.min < 0 && this.scale.max > 0) {
				height = (ma.abs(this.data[i][1]) / (this.scale.max - this.scale.min)) * this.graphHeight;
				y = this.getYCoord(0) - height;
				if (this.data[i][1] < 0) {
					y = this.getYCoord(0);
				}
				if (espar) {
					height2 = (ma.abs(this.data[i][2]) / (this.scale.max - this.scale.min)) * this.graphHeight;
					y2 = this.getYCoord(0) - height2;
					if (this.data[i][2] < 0) {
						y2 = this.getYCoord(0);
					}
				}

			} else if (this.scale.min < 0 && this.scale.max < 0) {
				height = (ma.abs(this.data[i][1]) - ma.abs(this.scale.max)) / (ma.abs(this.scale.min) - ma.abs(this.scale.max)) * this.graphHeight;
				y = prop.gutterTop;
				if (espar) {
					height2 = (ma.abs(this.data[i][2]) - ma.abs(this.scale.max)) / (ma.abs(this.scale.min) - ma.abs(this.scale.max)) * this.graphHeight;
					y2 = prop.gutterTop;
				}
			}

			//str = (x + (width / 2)) + "," + y;
			str = (i == 0 ? "M" : " L") + (x + (width / 2)) + " " + y;
			lines.push(str);

			str = "<circle class='grafico' style='stroke-width: 2px;stroke: #0b62a4;fill-opacity: 0;stroke-opacity: 0;' cx='" + (x + (width / 2)) + "' cy='" + y + "' r='3' fill='white' data-t='" + this.type + "' data-v='" + this.data[i].join("¦") + "'>";
			str += "<animate attributeType='xml' attributeName='r' from='0' to='3' dur='0.3s' fill='freeze' begin='0.5s'></animate>";
			str += "</circle>";
			circles.push(str);
			if (espar) {
				str = (i == 0 ? "M" : " L") + (x + (width / 2)) + " " + y2;
				lines2.push(str);

				str = "<circle class='grafico' style='stroke-width: 2px;stroke: rgb(243, 89, 88);fill-opacity: 0;stroke-opacity: 0;' cx='" + (x + (width / 2)) + "' cy='" + y2 + "' r='3' fill='white' data-t='" + this.type + "2' data-v='" + this.data[i].join("¦") + "'>";
				str += "<animate attributeType='xml' attributeName='r' from='0' to='3' dur='0.3s' fill='freeze' begin='0.5s'></animate>";
				str += "</circle>";
				circles2.push(str);
			}

		}
		str += "<g class='nv-linesWrap'>";
		str += "<g class='nvd3 nv-wrap nv-line'>";
		str += "<g clip-path>";
		str += "<g class='nv-groups'>";
		str += "<g class='nv-group nv-series-0' style='stroke-opacity: 1; fill-opacity: 0.5; fill: rgb(29, 187, 153); stroke: rgb(29, 187, 153);'>";
		str += "<path class='nv-line ptline' d='" + lines.join("") + "'></path>";//style='stroke-opacity: 1;fill-opacity: 0.1;fill:#0b62a4;stroke:#0b62a4;stroke-width:1;stroke-linejoin:round;stroke-linecap:round;'
		str += "</g>";
		//str = "<polyline id='line'  points='" + lines.join(" ") + "' style='fill:none;stroke:#424eb2;stroke-width:3;stroke-linejoin:round;stroke-linecap:round' >";
		//str += "<animate attributeType='CSS' attributeName='stroke-width' from='0' to='3' dur='0.5s' fill='freeze'></animate>";
		//str+="</polyline>";
		str += "<g>" + circles.join("") + "</g>";
		if (espar) {
			str += "<g class='nv-group nv-series-0' style='stroke-opacity: 1; fill-opacity: 0.5; fill: rgb(243, 89, 88); stroke: rgb(243, 89, 88);'>";
			str += "<path class='nv-line ptline' d='" + lines2.join("") + "' ></path>";//style='stroke-opacity: 1;fill-opacity: 0.1;fill:rgb(243, 89, 88);stroke:rgb(243, 89, 88);stroke-width:1;stroke-linejoin:round;stroke-linecap:round;'
			str += "<g>" + circles2.join("") + "</g>";
			str += "</g>";
		}
		str += "</g>";
		str += "</g>";
		str += "</g>";
		str += "</g>";
		return str;
	};
	this.getYCoord = function (value) {
		var prop = this.properties;
		if (value > this.scale.max) {
			return null;
		}
		var y;
		if (value < this.scale.min) {
			return null;
		}
		if (this.type != "Columna") {
			y = ((value - this.scale.min) / (this.scale.max - this.scale.min));
			y *= (this.alto - this.properties.gutterTop - this.properties.gutterBottom);
			y = this.alto - this.properties.gutterBottom - y;
		} else {
			y = ((value - this.scale.min) / (this.scale.max - this.scale.min));
			y *= this.graphWidth;
			y += prop.gutterLeft;
		}

		return y;

	}
	this.getWidth = function (value) {
		var ma = Math;
		if (this.scale.max <= 0 && this.scale.min < this.scale.max) {
			var x1 = this.getYCoord(this.scale.max);
			var x2 = this.getYCoord(value);
		} else if (this.scale.min > 0 && this.scale.max > this.scale.min) {
			var x1 = this.getYCoord(this.scale.min);
			var x2 = this.getYCoord(value);
		} else {
			var x1 = this.getYCoord(0);
			var x2 = this.getYCoord(value);
		}
		return ma.abs(x1 - x2);
	}
	this.drawBackground = function (obj) {
		var prop = obj.properties;
		var parts = [];
		var count = prop.yaxisLabelsCount;
		for (var i = 0; i <= count; ++i) {
			parts.push("M" + (prop.gutterLeft + prop.variant3dOffsetx) + " " + (prop.gutterTop + (obj.graphHeight / count) * i - prop.variant3dOffsety) + " L" + (obj.ancho - prop.gutterRight + prop.variant3dOffsetx) + " " + (prop.gutterTop + (obj.graphHeight / count) * i - prop.variant3dOffsety));
		}
		parts.push("M" + (prop.gutterLeft + prop.variant3dOffsetx) + " " + (obj.alto - prop.gutterBottom - prop.variant3dOffsety) + " L" + (obj.ancho - prop.gutterRight + prop.variant3dOffsetx) + " " + (obj.alto - prop.gutterBottom - prop.variant3dOffsety));
		if (this.type != "Columna") {
			count = obj.data.length;
		} else {
			count = obj.scale.labels.length || 10;
		}

		for (var i = 0; i <= count; ++i) {
			parts.push("M" + (prop.gutterLeft + ((obj.graphWidth / count) * i) + prop.variant3dOffsetx) + " " + (prop.gutterTop - prop.variant3dOffsety) + " L" + (prop.gutterLeft + ((obj.graphWidth / count) * i) + prop.variant3dOffsetx) + " " + (obj.alto - prop.gutterBottom - prop.variant3dOffsety));
		}
		parts.push("M" + (prop.gutterLeft + prop.variant3dOffsetx) + " " + (prop.gutterTop - prop.variant3dOffsety) + " L" + (obj.ancho - prop.gutterRight + prop.variant3dOffsetx) + " " + (prop.gutterTop - prop.variant3dOffsety) + " L" + (obj.ancho - prop.gutterRight + prop.variant3dOffsetx) + " " + (obj.alto - prop.gutterBottom - prop.variant3dOffsety) + " L" + (prop.gutterLeft + prop.variant3dOffsetx) + " " + (obj.alto - prop.gutterBottom - prop.variant3dOffsety) + " z");

		return "<path d='" + parts.join(" ") + "' stroke-opacity= '.5' fill='none' stroke='#e5e5e5' shape-rendering='crispEdges'></path>";
	}
	this.getScale = function (opt) {
		var obj = opt.object
			, numlabels = opt.numlabels
			, unitsPre = opt.unitsPre
			, unitsPost = opt.unitsPost
			, max = Number(opt.max)
			, min = Number(opt.min)
			, originalMax = max
			, round = opt.round
			, scale = {
				max: 1,
				labels: [],
				values: []
			};
		if (max === 0 && min === 0) {
			var max = 1;
			for (var i = 0; i < numlabels; ++i) {
				var label = ((((max - min) / numlabels) * (i + 1)) + min).toFixed(0);
				scale.labels.push(unitsPre + label + unitsPost);
				scale.values.push(parseFloat(label))
			}
		} else {
			var ma = Math;
			max = ma.ceil(max);
			var interval = ma.pow(10, ma.max(1, Number(String(Number(max) - Number(min)).length - 1)));
			var topValue = interval;
			while (topValue < max) {
				topValue += (interval / 2);
			}
			if (Number(originalMax) > Number(topValue)) {
				topValue += (interval / 2);
			}
			if (max <= 10) {
				topValue = (Number(originalMax) <= 5 ? 5 : 10);
			}
			if (obj && typeof (round) == 'boolean' && round) {
				topValue = 10 * interval;
			}
			scale.max = topValue;
			for (var i = 0; i < numlabels; ++i) {
				var label = ((((i + 1) / numlabels) * (topValue - min)) + min).toFixed(0);

				scale.labels.push(label);
				scale.values.push(((((i + 1) / numlabels) * (topValue - min)) + min).toFixed(0));
			}
		}

		scale.unitsPre = unitsPre;
		scale.unitsPost = unitsPost;
		scale.numlabels = numlabels;
		scale.round = Boolean(round);
		scale.min = min;
		for (var i = 0; i < scale.values.length; ++i) {
			scale.values[i] = parseFloat(scale.values[i]);
		}
		return scale;

	}
	this.obtenerMayorValorY = function (d, esP) {
		var nReg = d.length;
		var vMax = 0, vMin = 0, campos, vy = 0, vy2 = 0, esPrimero = true, t = 0;
		for (var i = nReg; i--;) {
			campos = d[i];
			vy = campos[1] * 1;

			if (!esP) {
				if (!this.par) {
					if (esPrimero) {
						vMin = 0;
					}
					if (vy > vMax) {
						vMax = vy;
					}
					if (vy < vMin) {
						vMin = vy;
					}
				} else {
					vy2 = campos[2] * 1;
					if (esPrimero) {
						vMin = 0;
					}
					vy = (vy > vy2 ? vy : vy2);
					if (vy > vMax) {
						vMax = vy;
					}
					vy = (vy < vy2 ? vy : vy2);
					if (vy < vMin) {
						vMin = vy;
					}
				}
			} else {
				if (vy > vMax) {
					vMax = vy;
				}
				t += vy;
			}
			esPrimero = false;
		}
		if (esP) { vMin = vMax, vMax = t }
		return { ymax: vMax, ymin: vMin };
	}
}

//CONFIGURARTOOLS

function configurarTooltip(tip) {
	var tip = document.getElementById(tip);
	var tool = document.getElementsByClassName("grafico");
	if (tool != null && tool.length > 0) {
		var n = tool.length;
		var ob;
		for (var i = n; i--;) {
			ob = tool[i];

			ob.onmouseout = function (e) {
				tip.style.display = "none";
				var g = e.target;

			}
			ob.onmousemove = function (e) {
				tip.style.display = "inline";
				var g = e.target;
				var tipo = g.getAttribute("data-t");
				var top = e.pageY + "px";
				var left = e.pageX + "px";
				//g.style.cssText += "strokeWidth:3;stroke:#a7e1d2";
				//g.style.stroke = "#a7e1d2";
				//g.style.strokeWidth = 3;


				tip.style.cssText = "top:" + top + ";left:" + left;
				var d = e.target.getAttribute("data-v");
				var c = d.split("¦");
				var t = e.target.getAttribute("data-t");
				//var c = e.currentTarget.id;
				//var campos = lista[c].split("|");
				if (t == "Linea" || t == "Linea2") {
					tip.innerHTML = c[0] + "<br/>";
					tip.innerHTML += "Total Ventas: " + c[1];
					tip.innerHTML += "<br/>Total Compras: " + c[2];
				} else if (["Barra2", "Columna2"].indexOf(t) > -1) {
					tip.innerHTML = c[0] + "<br/>" + c[2];
				} else {
					tip.innerHTML = c[0] + "<br/>" + c[1];
				}
			};
		}
	}
}

function fnllenarCombo(lista, nombreCombo, item, esSel, indexdtv) {
	var contenido = "";
	var n = lista.length;
	var campos = "";
	if (item != undefined && item != "") {
		contenido = "<option value=''>" + item + "</option>";
	} else {
		if (esSel) { contenido = "<option value=''>Seleccione</option>"; }
	}
	if (n > 0) {
		for (var x = 0; x < n; x++) {
			campos = lista[x].split("¦");
			contenido += "<option value='" + campos[0] + "' " + ((indexdtv != undefined && !isNaN(indexdtv)) ? ("data-v='" + campos[indexdtv * 1] + "'") : "") + ">" + campos[1] + "</option>";

		}
	}
	var cbo = document.getElementById(nombreCombo);
	if (cbo != null) {
		cbo.innerHTML = contenido;
	}
}

//function focused() {
//	var form = document.getElementById("focus"), event, div;
//	form.addEventListener("focus", function (e) {
//		event = e.target || e.srcElement;
//		div = event.parentNode;
//		if (!div.classList.contains('focused')) {
//			div.classList.add('focused');
//		}
//	}, true);
//	form.addEventListener("blur", function (e) {
//		event = e.target || e.srcElement;
//		div = event.parentNode;
//		if (div.classList.contains('focused')) {
//			div.classList.remove('focused');
//		}
//	}, true);
//}


//VALIDAR ENTRADAS

function vE() {
	var doc = document, sls = doc.getElementsByClassName("sl"), sns = doc.getElementsByClassName("sn"),
		nsl = sls.length, nsn = sns.length, sn, sl;
	var ltrs = "áéíóúabcdefghijklmnñopqrstuvwxyz";
	var movile = /Android|webOS|iPhone|iPad|BlackBerry|Windows Phone|Opera Mini|IEMobile|Mobile/i;
	var esMovile = false;
	if (movile.test(navigator.userAgent)) {
		esMovile = true;
	}

	for (var i = nsn; i--;) {
		sn = sns[i];
		if (esMovile) { sn.setAttribute("type", "number"); }
		sn.addEventListener("keypress", function (e) {
			var key = e.which || e.keyCode
			if ((key >= 48 && key <= 57) || (key == 8)) {
				var vl = e.target;
				var rango = vl.getAttribute("data-rango");
				if (rango != null) {
					var parametros = rango.split("-");
					var menor = parametros[0] * 1;
					var mayor = parametros[1] * 1;
					var valor = (vl.value.trim() + String.fromCharCode(key)) * 1;
					if (valor < menor || valor > mayor) {
						e.preventDefault();
						return false;
					}
				}
				return true;
			} else {
				e.preventDefault();
				return false
			};
			e.stopPropagation();

		})
	}
	for (var j = nsl; j--;) {
		sl = sls[j];
		sl.addEventListener("keypress", function (e) {
			var key = e.which || e.keyCode
			if ((key == 8) || (key == 13) || (key == 32)) {
				return true;
			} else {
				var lt = String.fromCharCode(key).toLowerCase();
				if (ltrs.indexOf(lt) == -1) {
					e.preventDefault();
					return false;
				}
			}
			e.stopPropagation();
		});
	}
	var snd = doc.getElementsByClassName("snd"), nsnd = snd.length, sn;
	for (var i = nsnd; i--;) {
		sn = snd[i];
		if (esMovile) { sn.setAttribute("type", "number"); }
		sn.addEventListener("keypress", function (e) {
			var key = e.which || e.keyCode
			var vl = e.target;
			if ((key >= 48 && key <= 57) || (key == 8) || (key == 46) || (key == 45)) {
				if (key == 46 && vl.value == "") {
					e.preventDefault();
					return false;
				} else {
					var rango = vl.getAttribute("data-rango");
					if (rango != null) {
						var parametros = rango.split("-");
						var menor = parametros[0] * 1;
						var mayor = parametros[1] * 1;
						var valor = (vl.value.trim() + String.fromCharCode(key)) * 1;
						if (valor < menor || valor > mayor) {
							e.preventDefault();
							return false;
						}
					}
					var av = vl.value.split(".");
					if (key == 46 && av.length >= 2) {
						e.preventDefault();
						return false;
					}
					var av = vl.value.split("-");
					if (key == 45 && av.length >= 2) {
						e.preventDefault();
						return false;
					}
				}
				return true;
			} else {
				e.preventDefault();
				return false
			};
			e.stopPropagation();
		});
	}
}

function FormatNumber(number, fraccion, d, s) {
	var format;
	fraccion = (fraccion == undefined ? 2 : fraccion);
	d = (d == undefined ? true : d);
	s = s || "";

	if (isNaN(number)) {
		format = (!d ? s : "") + "0.00" + (d ? s : "");
	} else {
		format = (!d ? s : "") + (number * 1).toLocaleString('en-US', { minimumFractionDigits: fraccion, maximumFractionDigits: fraccion }) + (d ? s : "");
	}
	return format;
}