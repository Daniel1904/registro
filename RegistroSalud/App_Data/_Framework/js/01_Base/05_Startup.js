﻿//TODO: START CONFIG
window.onload = function () {
    var $initControls = function () {
        $loading = $$loading('_loading');
        $alert = $$alert('_alert');
        $loading.create();
        $alert.create();
    };
    var $paths = function () {
        $app.setPage({ key: "Login", auth: 0, base: $modules.general, controller: "@" });
        $app.setPage({ key: "Layout", auth: 1, base: $modules.general, controller: "@" });
        $app.setPage({ key: "DashboardHome", auth: 1, base: $modules.general, controller: "@" });
        $app.setPage({ key: "Cliente", auth: 1, base: $modules.Cliente, controller: "@" });
        $app.setPage({ key: "Invoice", auth: 1, base: $modules.Invoice, controller: "@" });
        $app.setPage({ key: "Producto", auth: 1, base: $modules.Producto, controller: "@" });
    };
    $initControls();
    //TODO: Cargar Módulos
    $app = $jtse.module($appName);
    //TODO: Cargar valores iniciales (Root y Versión)
    $app.setRoot();
    $urlBase = $app.getRoot();
    $paths();
    $app.init();
};
