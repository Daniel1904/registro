﻿using PUNTOVENTA.SUNQUYUX_AppWeb.Controllers.Core;
using HARDCODE.Core.Entities;
using HARDCODE.Core.Helpers;
using System.Threading.Tasks;
using System.Web.Mvc;
using io = System.IO;
using System;

namespace RegistroSalud.Controllers.Modules._01_Invoice
{
    public class Proyecto001Controller : BaseController
    {
        DataSQL oDataSQL = null;
        public Proyecto001Controller()
        {
            oDataSQL = new DataSQL();
        }
        [HttpGet]
        public async Task<string> Proyecto_Listar()
        {
            StatusResponse response = new StatusResponse();
            response = await oDataSQL.ExecuteCommand("uspProductoListar");
            return Ok(response);
        }
        [HttpPost]
        public async Task<string> Proyecto_Grabar(string data)
        {
            StatusResponse response = new StatusResponse();
            response = await oDataSQL.ExecuteCommand("uspProsuctoGrabar", data);
            return Ok(response);
        }
        [HttpGet]
        public async Task<string> Cliente_Listar()
        {
            StatusResponse response = new StatusResponse();
            response = await oDataSQL.ExecuteCommand("uspClienteListar");
            return Ok(response);
        }
        [HttpPost]
        public async Task<string> Cliente_Grabar(string data)
        {
            StatusResponse response = new StatusResponse();
            response = await oDataSQL.ExecuteCommand("uspClienteGrabar", data);
            return Ok(response);
        }
        [HttpGet]
        public async Task<string> Invoice_Listar()
        {
            StatusResponse response = new StatusResponse();
            response = await oDataSQL.ExecuteCommand("uspInvoiceListar");
            return Ok(response);
        }
        [HttpGet]
        public async Task<string> Invoice_Listas()
        {
            StatusResponse response = new StatusResponse();
            response = await oDataSQL.ExecuteCommand("uspInvoiceListas");
            return Ok(response);
        }
        [HttpPost]
        public async Task<string> Invoice_Grabar(string data)
        {
            StatusResponse response = new StatusResponse();
            response = await oDataSQL.ExecuteCommand("uspInvoiceGrabar", data);
            return Ok(response);
        }
        [HttpPost]
        public async Task<string> Invoice_Consultar(string data)
        {
            StatusResponse response = new StatusResponse();
            response = await oDataSQL.ExecuteCommand("uspInvoiceConsultar", data);
            return Ok(response);
        }
        [HttpPost]
        public async Task<string> PDFSeguridadIncidente(string data, string Archivo = "")
        {
            string rpta = "";
            StatusResponse response = new StatusResponse();
            await Task.Run(async () =>
            {
                if (response.Data != "")
                {
                    try
                    {
                        response = await oDataSQL.ExecuteCommand("uspInvoiceConsultar", data);
                        string[] listas = (response.Data != "" ? response.Data.Split('¯') : new string[] { });
                        string[] cabecera = (listas[0] != "" ? listas[0].Split('¦') : new string[] { });
                        string[] detalle = (listas[1] != "" ? listas[1].Split('¬') : new string[] { });


                        string html = "";
                        string rutalogo = Server.MapPath("~/Complementos/img/Screenshot_3.png");
                        html += "<!DOCTYPE html>";
                        html += "";
                        html += "<html>";
                        html += "<head>";
                        html += "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
                        html += "    <title>Ronaldo</title>";
                        html += "</head>";
                        html += "";
                        html += "<body style='font-family:Arial'>";
                        html += "    <table style='width:100%;border-collapse:collapse'>";
                        html += "        <tr>";
                        html += "            <td style='width:50%'>";
                        html += "                <div>";
                        html += "                    <div style='text-align:left;font-size:25px;font-family:Arial'>";
                        html += "                        Stanford Plumbing y Heating";
                        html += "                    </div>";
                        html += "                    <div style='text-align:left;font-size:12px;font-weight:lighter;padding-top:6px'>";
                        html += "                        123 Madison drive, Seattle,WA, 7829Q";
                        html += "                    </div>";
                        html += "                    <div style='text-align:left;padding-top:6px;font-size:12px'>";
                        html += "                        <u>www.plumbingstanford.com</u>";
                        html += "                    </div>";
                        html += "                    <div style='text-align:left;padding-top:6px;font-size:12px'>";
                        html += "                        990-120-4560.";
                        html += "                    </div>";
                        html += "                </div>";
                        html += "            </td>";
                        html += "            <td style='width:50%'>";
                        html += "                <div style='border-radius:10px;text-align:center;font-size:20px;font-weight:bold'>";
                        html += "                    <div style='padding-top:0px'>";
                        html += "";
                        html += "                    </div>";
                        html += "                    <div style='padding-top:0px'>";
                        html += "                        <img src='"+rutalogo+"' style='width:120px' />";
                        html += "                    </div>";
                        html += "                    <div style='padding-top:0px'>";
                        html += "                    </div>";
                        html += "                </div>";
                        html += "            </td>";
                        html += "        </tr>";
                        html += "    </table>";
                        html += "    <div style='width:100%;color:#ffffff'>s</div>";
                        html += "    <table style='width:100%;border-collapse:collapse'>";
                        html += "        <tr>";
                        html += "            <td style='width:40%;border-bottom:2px solid;font-size:12px;border-color:#d8d8d8'>";
                        html += "                <div style='color:#35498d'>BILL TO</div>";
                        html += "            </td>";
                        html += "            <td style='width:30%'></td>";
                        html += "            <td style='width:15%;font-size:12px;font-weight:bold'>";
                        html += "                Invoice No:";
                        html += "            </td>";
                        html += "            <td style='width:15%;font-size:12px'>";
                        html +=                 cabecera[0];
                        html += "            </td>";
                        html += "        </tr>";
                        html += "        <tr>";
                        html += "            <td style='width:40%;font-size:12px;padding-top:5px'>";
                        html += cabecera[1]                ;
                        html += "            </td>";
                        html += "            <td style='width:30%'></td>";
                        html += "            <td style='width:15%;font-size:12px;font-weight:bold;padding-top:5px'>";
                        html += "                Invoice Date:";
                        html += "            </td>";
                        html += "            <td style='width:15%;font-size:12px;padding-top:5px'>";
                        html += cabecera[2];
                        html += "            </td>";
                        html += "        </tr>";
                        html += "        <tr>";
                        html += "            <td style='width:40%;font-size:12px;padding-top:5px'>";
                        html += cabecera[3];
                        html += "            </td>";
                        html += "            <td style='width:30%'></td>";
                        html += "            <td style='width:15%;font-size:12px;font-weight:bold;padding-top:5px'>";
                        html += "                Due Date:";
                        html += "            </td>";
                        html += "            <td style='width:15%;font-size:12px;padding-top:5px'>";
                        html += cabecera[4];
                        html += "            </td>";
                        html += "        </tr>";
                        html += "        <tr>";
                        html += "            <td style='width:40%;font-size:12px;padding-top:5px'>";
                        html += cabecera[5];
                        html += "            </td>";
                        html += "            <td style='width:30%'></td>";
                        html += "            <td style='width:15%;font-size:12px;font-weight:bold;padding-top:5px'>";
                        html += "            </td>";
                        html += "            <td style='width:15%;font-size:12px;padding-top:5px'>";
                        html += "            </td>";
                        html += "        </tr>";
                        html += "        <tr>";
                        html += "            <td style='width:40%;font-size:12px;padding-top:5px'>";
                        html += cabecera[6];
                        html += "            </td>";
                        html += "            <td style='width:30%'></td>";
                        html += "            <td style='width:15%;font-size:12px;font-weight:bold;padding-top:5px'>";
                        html += "            </td>";
                        html += "            <td style='width:15%;font-size:12px;padding-top:5px'>";
                        html += "            </td>";
                        html += "        </tr>";
                        html += "    </table>";
                        html += "    <div style='width:100%;color:#ffffff'>s</div>";
                        html += "    <table style='width:100%;border-collapse:collapse'>";
                        html += "<thead>";
                        html += "        <tr>";
                        html += "            <td style='width:40%;border-top:2px solid;border-left:2px solid;border-bottom:2px solid;border-color:#c7c1c1;font-size:12px;font-weight:bold;padding:5px;text-align:center'>";
                        html += "                DESCRIPTION";
                        html += "            </td>";
                        html += "            <td style='width:20%;border-bottom:2px solid;border-top:2px solid;border-color:#c7c1c1;font-size:12px;font-weight:bold;padding:5px;text-align:center'>";
                        html += "                QTY / HR";
                        html += "            </td>";
                        html += "            <td style='width:20%;border-bottom:2px solid;border-top:2px solid;border-color:#c7c1c1;font-size:12px;font-weight:bold;padding:5px;text-align:center'>";
                        html += "                UNIT PRICE";
                        html += "            </td>";
                        html += "            <td style='width:20%;border-bottom:2px solid;border-top:2px solid;border-right:2px solid;border-color:#c7c1c1;font-size:12px;font-weight:bold;padding:5px;text-align:center'>";
                        html += "                TOTAL";
                        html += "            </td>";
                        html += "        </tr>";
                        html += "</thead>";
                        html += "<tbody >";
                        if (detalle.Length > 0)
                        {
                            string[] campos;
                            for (int i = 0; i < detalle.Length; i++)
                            {
                                campos = detalle[i].Split('¦');
                                html += "        <tr>";
                                html += "            <td style='border-left:2px solid;border-bottom:2px solid;border-right:2px solid;border-color:#d8d8d8;font-size:11px;text-align:left;padding:5px;background-color:#e5e2e2'>";
                                html += campos[1];
                                html += "            </td>";
                                html += "            <td style='border-left:2px solid;border-bottom:2px solid;border-right:2px solid;border-color:#d8d8d8;font-size:11px;text-align:center;padding:5px;background-color:#e5e2e2'>";
                                html += campos[2];
                                html += "            </td>";
                                html += "            <td style='border-left:2px solid;border-bottom:2px solid;border-right:2px solid;border-color:#d8d8d8;font-size:11px;text-align:right;padding:5px;background-color:#e5e2e2'>";
                                html += campos[3];
                                html += "            </td>";
                                html += "            <td style='border-left:2px solid;border-bottom:2px solid;border-right:2px solid;border-color:#d8d8d8;font-size:11px;text-align:right;padding:5px;background-color:#e5e2e2'>";
                                html += campos[4];
                                html += "            </td>";
                                html += "        </tr>";
                            }
                        }

                        html += "</tbody>";
                        html += "    </table>";
                        html += "    <table style='width:100%;border-collapse:collapse'>";
                        html += "        <tr>";
                        html += "            <td rowspan='7' style='width:50%'>";
                        html += "                <div style='font-size:12px;text-align:center;padding:5px'>Thank you for your business!</div>";
                        html += "            </td>";
                        html += "        </tr>";
                        html += "        <tr>";
                        html += "            <td style='width:30%;font-size:11px;font-weight:bold;text-align:right;padding:5px'>SUBTOTAL</td>";
                        html += "            <td style='width:20%;font-size:11px;text-align:right;border-bottom:2px solid;padding:5px;border-color:#e5e2e2'>"+cabecera[7]+"</td>";
                        html += "        </tr>";
                        html += "        <tr>";
                        html += "            <td style='width:30%;font-size:11px;font-weight:bold;text-align:right;padding:5px'>DISCOUNT</td>";
                        html += "            <td style='width:20%;font-size:11px;text-align:right;border-bottom:2px solid;padding:5px;border-color:#e5e2e2'>"+cabecera[8]+"</td>";
                        html += "        </tr>";
                        html += "        <tr>";
                        html += "            <td style='width:30%;font-size:11px;font-weight:bold;text-align:right;padding:5px'>SUBTOTAL LESS DISCOUNT</td>";
                        html += "            <td style='width:15%;font-size:11px;text-align:right;border-bottom:2px solid;padding:5px;border-color:#e5e2e2'>"+cabecera[9]+"</td>";
                        html += "        </tr>";
                        html += "        <tr>";
                        html += "            <td style='width:30%;font-size:11px;font-weight:bold;text-align:right;padding:5px'>TAX RATE</td>";
                        html += "            <td style='width:20%;font-size:11px;text-align:right;border-bottom:2px solid;padding:5px;border-color:#e5e2e2'>"+cabecera[10]+"</td>";
                        html += "        </tr>";
                        html += "        <tr>";
                        html += "            <td style='width:30%;font-size:11px;font-weight:bold;text-align:right;padding:5px;border-bottom:2px solid;border-color:#808080'>TOTAL TAX</td>";
                        html += "            <td style='width:20%;font-size:11px;text-align:right;border-bottom:2px solid;padding:5px;border-color:#808080'>"+cabecera[11]+"</td>";
                        html += "        </tr>";
                        html += "        <tr>";
                        html += "            <td style='width:35%;font-size:12px;font-weight:bold;text-align:right;padding:5px'>BALANCE DUE</td>";
                        html += "            <td style='width:15%;font-size:12px;font-weight:bold;text-align:right;border-bottom:2px solid;padding:5px;border-color:#a3a3a3'>"+cabecera[12]+"</td>";
                        html += "        </tr>";
                        html += "    </table>";
                        html += "    <div style='width:100%;color:#ffffff'>s</div>";
                        html += "    <table style='width:100%;border-collapse:collapse'>";
                        html += "        <tr>";
                        html += "            <td style='width:100%'>";
                        html += "                <div>";
                        html += "                    <div style='width:40%;text-align:left;font-size:11px;font-family:Arial;border-bottom:2px solid'>";
                        html += "                        Terms y Instructions";
                        html += "                    </div>";
                        html += "                    <div style='text-align:left;font-size:10px;font-weight:lighter;padding-top:6px;color:#808080'>";
                        html += "                        Please pay within 20 days by paypal (bob@stanfordplumbing.com)";
                        html += "                    </div>";
                        html += "                    <div style='text-align:left;padding-top:6px;font-size:10px;font-weight:lighter;color:#808080'>";
                        html += "                        Installed products have 5 year warranty.";
                        html += "                    </div>";
                        html += "                </div>";
                        html += "            </td>";
                        html += "        </tr>";
                        html += "    </table>";
                        html += "</body>";
                        html += "</html> ";
                        html += "";


                        var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
                        htmlToPdf.Orientation = NReco.PdfGenerator.PageOrientation.Portrait;
                        var pdfBytes = htmlToPdf.GeneratePdf(html);
                        if (String.IsNullOrEmpty(Archivo))
                        {
                            rpta = Convert.ToBase64String(pdfBytes);
                        }
                        else
                        {
                            rpta = Archivo;
                            io.File.WriteAllBytes(rpta, pdfBytes);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Source);
                    }
                }
                response.Data = rpta;
                response.Success = true;

            });
            return Ok(response);
        }
    }
        
}