﻿using HARDCODE.Core.Entities;
using HARDCODE.Core.Helpers;
using PUNTOVENTA.SUNQUYUX_AppWeb.Controllers.Core;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PUNTOVENTA.SUNQUYUX_AppWeb.Controllers.Modules._00_General
{
    public class GeneralController : BaseController
	{
		DataSQL oDataSQL = null;
		public GeneralController()
		{
			oDataSQL = new DataSQL();
		}
		// GENERAL - Layout
		[HttpGet]
		public async Task<string> UserChat_Listar()
		{
			StatusResponse response = new StatusResponse();
			response = await oDataSQL.ExecuteCommand("uspUserChat_List");
			return Ok(response);
		}
		[HttpGet]
		public async Task<string> LineChart_Listar()
		{
			StatusResponse response = new StatusResponse();
			response = await oDataSQL.ExecuteCommand("usp_General_LineChart");
			return Ok(response);
		}
	}
}