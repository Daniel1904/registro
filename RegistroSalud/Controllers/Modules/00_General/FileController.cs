﻿using System.Threading.Tasks;
using System.Web.Mvc;
using io = System.IO;

namespace PUNTOVENTA.SUNQUYUX_AppWeb.Controllers.Modules._00_General
{
    public class FileController : Controller
    {
		public async Task<string> FileCategoriaProducto()
		{
			string rpta = "";
			await Task.Run(() =>
			{
				if (Request.InputStream != null)
				{
					string hdarchivo = Request.Headers["archivo"];
					string ruta = Server.MapPath("~/FileGeneral/CategoriaProducto/");

					string archivo = io.Path.Combine(ruta, hdarchivo);
					io.Stream flujo = Request.InputStream;
					using (io.FileStream fs = new io.FileStream(archivo, io.FileMode.Append, io.FileAccess.Write))
					{
						flujo.CopyTo(fs);
						rpta = "OK";
					}
				}
			});
			return rpta;
		}

		public async Task<string> FileProducto()
		{
			string rpta = "";
			await Task.Run(() =>
			{
				if (Request.InputStream != null)
				{
					string hdarchivo = Request.Headers["archivo"];
					string ruta = Server.MapPath("~/FileGeneral/Producto/");

					string archivo = io.Path.Combine(ruta, hdarchivo);
					io.Stream flujo = Request.InputStream;
					using (io.FileStream fs = new io.FileStream(archivo, io.FileMode.Append, io.FileAccess.Write))
					{
						flujo.CopyTo(fs);
						rpta = "OK";
					}
				}
			});
			return rpta;
		}
	}
}