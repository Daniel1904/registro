﻿using System.Threading.Tasks;
using System.Web.Mvc;
using HARDCODE.Core.Entities;
using HARDCODE.Core.Helpers;

namespace PUNTOVENTA.SUNQUYUX_AppWeb.Controllers.Core
{
	public class SeguridadController : BaseController
	{
		DataSQL oDataSQL = null;
		public SeguridadController()
		{
			oDataSQL = new DataSQL();
		}

		[HttpPost]
		public async Task<string> validadLogin(string data)
		{
			StatusResponse response = new StatusResponse();
			response = await oDataSQL.ExecuteCommand("uspUsuarioValidarLogin", data);
			return Ok(response);
		}

		public async Task<string> IsAuthenticated(string data)
		{
			StatusResponse response = new StatusResponse();
			response = await oDataSQL.ExecuteCommand("uspUsuarioValidarToken", data);
			return Ok(response);
		}
		public async Task<string> listaMenu(string data)
		{
			StatusResponse response = new StatusResponse();
			response = await oDataSQL.ExecuteCommand("uspMenu_Listar", data);
			return Ok(response);
		}

		public async Task<string> EliminarGenerico(string data)
		{
			StatusResponse response = new StatusResponse();
			response = await oDataSQL.ExecuteCommand("uspEliminarGenericoGrabar", data);
			return Ok(response);
		}
	}
}

