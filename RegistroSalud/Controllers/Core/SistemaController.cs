﻿using System.Web.Mvc;
using System.Web.Script.Serialization;
using HARDCODE.Core.Entities;
using HARDCODE.Core.Helpers;

namespace PUNTOVENTA.SUNQUYUX_AppWeb.Controllers.Core
{
    public class SistemaController : Controller
    {
        public ActionResult Inicio()
        {
            enStartup oStartup = new enStartup();
            oStartup.root = Url.Content("~");
            oStartup.versionApp = AppSettings.Get("_VersionApp");
            oStartup.isEncoded = AppSettings.Get("_IsEncoded") == "" ? true : false;
            oStartup.isDeveloper = AppSettings.Get("_IsDeveloper") == "true" ? true : false;
            oStartup.isAD = AppSettings.Get("_IsDomainActiveDirectory") == "true" ? true : false;
            oStartup.charLocked = AppSettings.Get("_CharLocked");
            oStartup.others = AppSettings.Get("RUC");
            ViewBag.Script = Encrypt.Run(new JavaScriptSerializer().Serialize(oStartup));
            ViewBag.VersionApp = oStartup.versionApp;
            return View();
        }
    }
}