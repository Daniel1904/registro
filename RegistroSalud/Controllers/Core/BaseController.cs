﻿using System;
using System.Net;
using System.Text;
using System.Web.Mvc;
using HARDCODE.Core.Entities;
using HARDCODE.Core.Helpers;

namespace PUNTOVENTA.SUNQUYUX_AppWeb.Controllers.Core
{
    public class BaseController : Controller
    {
        protected override void OnException(ExceptionContext filterContext)
        {
            GenerateTextFile<Exception>.FromObject(filterContext.Exception);
            filterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.OK;
            filterContext.HttpContext.Response.AddHeader("StatusResponse", "{\"success\":false}");
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            int n = (int)filterContext.HttpContext.Request.InputStream.Length;
            byte[] buffer = new byte[n];
            filterContext.HttpContext.Request.InputStream.Read(buffer, 0, n);
            string data = Encoding.UTF8.GetString(buffer);
            if (Convert.ToBoolean(AppSettings.Get("_IsEncoded")))
            {

                filterContext.ActionParameters["data"] = Encrypt.Run(data, false);
            }
            else
            {
                filterContext.ActionParameters["data"] = data;
            }
        }

        public string Ok(StatusResponse obj)
        {
            if (Convert.ToBoolean(AppSettings.Get("_IsEncoded")))
            {
                obj.Data = Encrypt.Run(obj.Data);
            }
            Response.StatusCode = (int)HttpStatusCode.OK;
            if (obj.Success)
            {
                Response.AddHeader("StatusResponse", "{\"success\":true}");
                //Response.StatusDescription = "{\"Success\":true}";
            }
            else
            {
                Response.AddHeader("StatusResponse", "{\"success\":false}");
                //Response.StatusDescription = "{\"Success\":false}";
            }
            return obj.Data;
        }
    }
}