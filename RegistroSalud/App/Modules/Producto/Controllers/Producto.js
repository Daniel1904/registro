﻿define([$appName,  //module
    "Proyecto" //namespace
], function (module, namespace, args) {
    "use strict";
    var service = function ($service, $http) {
        $service.ProyectoListar = function (item, callback) {
            return $http({
                method: 'get',
                url: "Proyecto001/Proyecto_Listar",
                callback: callback
            });
        };

        $service.ProyectoGrabar = function (item, callback) {
            return $http({
                method: 'post',
                data: item,
                url: "Proyecto001/Proyecto_Grabar",
                callback: callback
            });
        };
        $service.EliminarGenerico = function (item, callback) {
            return $http({
                method: 'post',
                data: item,
                url: "Seguridad/EliminarGenerico",
                callback: callback
            });
        };
    };
    var controller = function ($scope, $service) {
        var SepListas = "¯", SepFilas = "¬", SepCol = "¦";
        var _idProyecto = 0;
        var _IdeliminarMatriz;
        $scope.init = function () {
            $scope.configurarControles();
            $scope.Proyecto_Listar();
        };

        $scope.Proyecto_Listar = function () {
            $service.ProyectoListar(null, function (d) {
                if (d.success) {
                    var listas = (d.data != "" ? d.data.split(SepFilas) : []);
                    $$grid('grd_MC_Proyecto').setData(listas);
                }
            });
        }

        $scope.configurarControles = function () {
            $$grid('grd_MC_Proyecto').create({
                headers: ['', 'Descripción', 'Precio'],
                properties: ['IdProyecto', 'Descripcion', 'Precio'],
                typesData: ['S', 'S', 'S'],
                typesFilter: ['', 'I', 'I'],
                sortHeader: [false, true, true],
                showColumns: [false, true, true],
                widths: [45, 45],
                indexs: [1, 2],
                headerStyle: 'background-color: #f6f6f8; color:#333',
                data: [],
                borderTop: false,
                btnNew: true,
                btnEdit: true,
                btnDelete: true,
                btnDeleteMultiple: false,
                btnRefresh: true,
                btnExportExcel: true,
                filterPosition: 'UP',
                generalFilter: true,
                entriesPage: 15,
                rangePage: 5,
                separator: '¦',
                fnBtnNew: 'fnEvNuevoProyecto',
                fnBtnEdit: 'fnEvEModificarProyecto',
                fnBtnDelete: 'fnEvDeleteProyecto',
                fnBtnRefresh: 'fnEvRefreshProyecto',
                fnExtensions: [],
                fnRowEvent: null,
                namespace: namespace
            });

            $$modal('mdl_MC_ProyectoNuevoEditar').create({
                WithScrollY: true,
                Width: 'sm',
                Title: '',
                ButtonNames: ['Guardar', 'Cancelar'],
                ButtonClass: ['btn-save-ico', 'btn-cancel-ico'],
                FnEvents: ['fnEvMdlProyectoGuardar', 'fnEvMdlProyectoCancelar'],
                Namespace: namespace
            });

        };

        $scope.fnEvNuevoProyecto = function () {
            $fnSqx_limpiarFrm('js-frmCl');
            _idProyecto = 0;
            $$modal('mdl_MC_ProyectoNuevoEditar').setTitle('Registrar Proyecto');
            $$modal('mdl_MC_ProyectoNuevoEditar').show();
        };
        $scope.fnEvMdlProyectoGuardar = function () {
            var item = {
                containers: ['frmCabeceraProyecto']
            };
            if ($fnValidarForm(item).resultado == 0) {
                var strUse = sessionStorage.getItem($appName) + SepListas;
                var strData = strUse + _idProyecto + SepCol + getData('js-frmCl')
                $service.ProyectoGrabar(strData, function (d) {
                    if (d.success) {
                        $scope.Proyecto_Listar();
                        $alert.show('<i class="fa fa-check"></i> ' + $msg.response.success, 'I');
                        $$modal('mdl_MC_ProyectoNuevoEditar').hide();
                    } else {
                        $alert.show('<i class="fa fa-exclamation-triangle"></i> ' + $msg.response.error, 'E');
                    }
                });
            }
        };
        $scope.fnEvMdlProyectoCancelar = function () {
            $$modal('mdl_MC_ProyectoNuevoEditar').hide();
        };

        $scope.fnEvRefreshProyecto = function () {
            $scope.Proyecto_Listar();
        }
        $scope.fnEvEModificarProyecto = function (row) {
            $fnSqx_limpiarFrm('js-frmCl');
            var inps = document.getElementById("Descripcion");
            var inps1 = document.getElementById("Precio");

            _idProyecto = row.IdProyecto;

            inps.value = row.Descripcion;
            inps1.value = row.Precio;

            $$modal('mdl_MC_ProyectoNuevoEditar').setTitle('Editar Proyecto');
            $$modal('mdl_MC_ProyectoNuevoEditar').show();
        };

    };
    controller.$inject = ['$scope', '$service'];
    module.controller(namespace, controller, service);
});