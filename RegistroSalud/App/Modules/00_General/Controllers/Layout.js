﻿define([$appName,  //COPIA
	"Layout" //namespace
], function (module, namespace, args) {
	"use strict";
	var service = function ($service, $http) {
		$service.obtenerMenu = function (item, callback) {
			return $http({
				method: 'post',
				data: item,
				url: "Seguridad/listaMenu",
				callback: callback
			});
		};
		$service.listUserChat = function (item, callback) {
			return $http({
				method: 'get',
				url: "General/UserChat_Listar",
				callback: callback
			});
		};
	};
	var controller = function ($scope, $service) {
		var _aM = [], _lstdata = [], _lstUserChat = [], _lstAbc = [];
		$scope.init = function () {
			$scope.cargarDatos();
			$scope.configurarControles();
			$scope.configurarEventos();

			window.onbeforeunload = function (event) {
				sessionStorage.clear();
				history.pushState(null, null, module.getRoot());
			};
		};
		$scope.cargarDatos = function () {
			var tk = sessionStorage.getItem($appName);
			$service.obtenerMenu(tk, function (d) {
				if (d.success) {
					var listas = (d.data != "" ? d.data.split("¯") : []);
					_aM = listas[0].split('¬');
					$scope.mM();
					module.goSubPage('DashboardHome', 'view-Main');
					$scope.lstUserChat();
					$scope.configColor(listas[1]);
				}
			});
			//loadDatePicker('mm/dd/yyyy');
		};
		$scope.configurarControles = function () {
			$scope.mM = function () {
				var ss = 0;
				var nr = _aM.length, cm = [], ul, vl;
				var dm = document.getElementById("divMenu");
				var ctn = "<ul class='menu-items scroll-content scroll-scrolly_visible'>";
				for (var i = 0; i < nr; i++) {
					cm = _aM[i].split("¦");
					if (cm[3] == 0) {
						ctn += (i === 0 ? "<li class='m-t-30'>" : "<li>");
						vl = $scope.cH(cm[0]);
						if (vl) {
							ctn += "<a class='hand pdr menu-text-icon-c'>";
							ctn += "<span class='title'>";
							ctn += cm[1];
							ctn += "</span>";
							ctn += "<span class='arrow'><i class='fa fa-angle-left'></i></span></a>";
							ctn += "<span class='icon-thumbnail hand menu-text-icon-c'>";
							ctn += "<i class='fa ";
							ctn += cm[4];
							ctn += "'>";
							ctn += "</i></span>";
							ul = $scope.cM(cm[0], ss);
							ctn += (ul == "<ul class='menu-sub menu-arrow pd" + ss + "'></ul>" ? "" : ul);
						} else {
							ctn += "<a class='detailed menuview menu-text-icon-c' href='javascript:void(0)' data-view='";
							ctn += cm[2] + '¦' + cm[0];
							ctn += "' >";
							ctn += "<span class='title'>";
							ctn += cm[1];
							ctn += "</span>";
							ctn += "<span class='details'>" + cm[5] + "</span></a>";
							ctn += "<span class='icon-thumbnail menu-text-icon-c'>";
							ctn += "<i class='fa ";
							ctn += cm[4];
							ctn += "'>";
							ctn += "</i></span>";
						}
					}
					ctn += "</li>";
				}
				ctn += "</ul>";
				dm.innerHTML = ctn;
				$scope.cT();
			};
			$scope.cH = function (m) {
				var nr = _aM.length;
				var cm = [];
				for (var i = 0; i < nr; i++) {
					cm = _aM[i].split("¦");
					if (cm[3] == m) {
						return true;
					}
				}
			};
			$scope.cM = function (m, s) {
				var nr = _aM.length;
				var cm;
				var ctn = "<ul class='sub-menu menu-submenu-area-bc pd" + s + "'>";
				for (var i = 0; i < nr; i++) {
					cm = _aM[i].split("¦");
					if (cm[3] == m) {
						ctn += "<li class=''>";
						ctn += "<a class='menuview menu-text-icon-c' href='javascript:void(0)' data-view='";
						ctn += cm[2] + '¦' + cm[0];
						ctn += "' >";
						ctn += cm[1];
						ctn += "</a>";
						ctn += "<span class='icon-thumbnail menu-text-icon-c'>";
						//ctn += cm[1].charAt(0);
						ctn += cm[4];
						ctn += "</span>";
						ctn += "</li>";
					}
				}
				ctn += "</ul>";
				return ctn;
			};
			$scope.cT = function () {
				var pdr = document.getElementsByClassName("pdr"), n = pdr.length;
				var evt, li, ul, span;
				for (var i = 0; i < n; i++) {
					pdr[i].addEventListener("click", function (e) {
						evt = e.target || e.srcElement;
						if (evt.nodeName == "A") {
							li = evt.parentNode;
							ul = li.lastChild;
							span = li.firstChild.lastChild;
						}
						if (evt.nodeName == "SPAN") {
							li = evt.parentNode.parentNode;
							ul = li.lastChild;
							span = li.firstChild.lastChild;
						}
						if (evt.nodeName == "I") {
							li = evt.parentNode.parentNode.parentNode;
							ul = li.lastChild;
							span = li.firstChild.lastChild;
						}
						if (li != null && ul != null && span != null) {
							if (li.classList.contains('open') && li.classList.contains('active') && span.classList.contains('open')) {
								li.classList.remove('open');
								li.classList.remove('active');
								li.classList.remove('rm');
								span.classList.remove('open');
								span.classList.remove('active');
								ul.style.display = "none";
							} else {
								$scope.rM();
								li.classList.add('open');
								li.classList.add('active');
								li.classList.add('rm');
								span.classList.add('open');
								span.classList.add('active');
								ul.style.display = "block";
							}
						}

					});
				}
				$scope.evC();
			};
			$scope.rM = function () {
				var s = document.getElementsByClassName("rm");
				var n = s.length, l, span, ul;
				for (var i = 0; i < n; i++) {
					l = s[i];
					if (l.classList.contains("rm")) {
						span = l.firstChild.lastChild;
						ul = l.lastChild;
						l.classList.remove('open');
						l.classList.remove('active');
						l.classList.remove('rm');
						span.classList.remove('open');
						span.classList.remove('active');
						ul.style.display = "none";
					}
				}
			};
			$scope.evC = function () {
				var menuview = document.getElementsByClassName('menuview');
				var view, element, dt, lst;
				for (var m = 0; m < menuview.length; m++) {
					menuview[m].onclick = function (e) {
						element = e.target || e.srcElement;
						lst = this.getAttribute('data-view');
						dt = lst.split('¦');
						view = dt[0];
						if (view != '' && view != null) {
							sessionStorage.setItem('AppView', dt[1]);
							module.goSubPage(view, 'view-Main');
							
						}
					};
				}
			};
			$scope.userlist = function () {
				var spanItems = document.querySelectorAll(".chat-user-list");
				var nspanItems = spanItems.length, element, attrName;
				for (var i = 0; i < nspanItems; i++) {
					spanItems[i].addEventListener("click", function (e) {
						element = e.target || e.srcElement;
						attrName = element.getAttribute("data-nm");
						attrName += "<div class='fs-11 hint-text' style='color: green'>Online</div>";
						document.getElementById('idNameChat').innerHTML = attrName;
						var chat = document.getElementById('chat');
						if (chat.classList.contains('push-parrallax')) {
							chat.classList.remove('push-parrallax');
						} else {
							chat.classList.add('push-parrallax');
						}
					});
				}
			};
			$scope.retornarModal = function () {
				var retornar = document.querySelectorAll(".js-retornar");
				var nretornar = retornar.length, element;
				for (var i = 0; i < nretornar; i++) {
					retornar[i].addEventListener("click", function (e) {
						element = e.target || e.srcElement;
						var chat = document.getElementById('chat');
						if (chat.classList.contains('push-parrallax')) {
							chat.classList.remove('push-parrallax');
						} else {
							chat.classList.add('push-parrallax');
						}
					});
				}
			};
			$scope.lstUserChat = function () {
				$service.listUserChat(null, function (d) {
					if (d.success) {
						var lstTotal = (d.data != "" ? d.data.split("¯") : []);
						_lstUserChat = (lstTotal[0] != "" ? lstTotal[0].split("¬") : []);
						_lstAbc = (lstTotal[1] != "" ? lstTotal[1].split("¬") : []);
						var reg = _lstAbc.length, campos, ul, s = 0;
						var cht = document.getElementById("idContent");
						var ctn = "<div class='tab-pane no-padding active show'>";//inicio
						ctn += "<div class='view-port clearfix' id='chat'>";//inicio

						ctn += "<div class='view bg-white'>";//m
						ctn += "<div class='navbar navbar-default'>";
						ctn += "<div class='navbar-inner'>";
						ctn += "<div class='view-heading'>Chat List<div class='fs-11'>Show All</div></div>";
						ctn += "</div>";
						ctn += "</div>";
						ctn += "<div class='list-view boreded no-top-border'>";
						ctn += "<div class='scroll-wrapper list-view-wrapper' style='position: absolute;'>";
						ctn += "<div class='list-view-wrapper scroll-content scroll-scrolly_visible' style='height: auto; margin-bottom: 0px; margin-right: 0px; max-height: 94%;'>";
						for (var i = 0; i < reg; i++) {
							campos = _lstAbc[i];
							ctn += "<div class='list-view-group-container'>";
							ctn += "<div class='list-view-group-header text-uppercase'>" + campos + "</div>";
							ctn += "<ul class='ch" + s + "'>";
							ul = $scope.countchat(campos, s);
							ctn += ul;
							ctn += "</ul>";
							ctn += "</div>";
						}
						ctn += "</div>";
						ctn += "</div>";
						ctn += "</div>";
						ctn += "</div>";//m
						////////////////////////////coversacion///////////////////////////////////////
						ctn += "<div class='view chat-view bg-white clearfix'>";//a

						ctn += "<div class='navbar navbar-default'>";
						ctn += "<div class='navbar-inner'>";
						ctn += "<a class='js-retornar link text-master inline action p-l-10 p-r-10'>";
						ctn += "<i class='fa fa-chevron-left'></i>";
						ctn += "</a>";
						ctn += "<div id='idNameChat' class='view-heading'></div>";
						ctn += "</div>";
						ctn += "</div>";

						ctn += "<div class='chat-inner' id='my-conversation'>";//b
						//ctn += "<div class='message clearfix'>";
						//ctn += "<div class='chat-bubble from-me'>Hello there</div>";
						//ctn += "</div>";

						//ctn += "<div class='message clearfix'>";
						//ctn += "<div class='profile-img-wrapper m-t-5 inline'>";
						//ctn += "<img class='col-top' width='30' height='30' />";
						//ctn += "</div>";
						//ctn += "<div class='chat-bubble from-them'>Hey</div>";
						//ctn += "</div>";

						//ctn += "<div class='message clearfix'>";
						//ctn += "<div class='chat-bubble from-me'>Did you check out Pages framework ?</div>";
						//ctn += "</div>";

						//ctn += "<div class='message clearfix'>";
						//ctn += "<div class='chat-bubble from-me'>Its an awesome chat</div>";
						//ctn += "</div>";

						//ctn += "<div class='message clearfix'>";
						//ctn += "<div class='profile-img-wrapper m-t-5 inline'>";
						//ctn += "<img class='col-top' width='30' height='30' />";
						//ctn += "</div>";
						//ctn += "<div class='chat-bubble from-them'>Yea</div>";
						//ctn += "</div>";
						ctn += "</div>";//b

						ctn += "<div class='b-t b-grey bg-white clearfix p-l-10 p-r-10'>";
						ctn += "<div class='col-sm-20'>";
						ctn += "<input id='txtMensaje' type='text' class='form-control chat-input js-Mensaje' placeholder='Say something' />";
						ctn += "</div>";
						ctn += "<div class='col-sm-3 link text-master m-l-10 m-t-15 p-l-10 b-l b-grey col-top'>";
						ctn += "<a class='link text-master'><i class='fa fa-paper-plane'></i></a>";
						ctn += "</div>";
						ctn += "</div>";

						ctn += "</div>";//a
						//////////////////////////////////////////////////////////////
						ctn += "</div>";//final
						ctn += "</div>";//final
						cht.innerHTML = ctn;
						$scope.userlist();
						$scope.retornarModal();
						$scope.enterMensaje();
					}
				});
			};
			$scope.countchat = function (m, s) {
				var nlstReg = _lstUserChat.length;
				var cm, ctn = '';
				for (var i = 0; i < nlstReg; i++) {
					cm = _lstUserChat[i].split("¦");
					if (cm[1].toLowerCase().charAt(0) == m) {
						ctn += "<li data-nm='" + cm[1] + "' class='chat-user-list clearfix'>";
						ctn += "<a data-nm='" + cm[1] + "'>";
						ctn += "<span data-nm='" + cm[1] + "' class='thumbnail-wrapper d32 circular bg-success'>";
						ctn += "<img data-nm='" + cm[1] + "' width='34' height='34' alt='' class='col-top' />";
						ctn += "</span>";
						ctn += "<p data-nm='" + cm[1] + "' class='p-l-10 pfo'>";
						ctn += "<span data-nm='" + cm[1] + "' class='text-master'>" + cm[1] + "</span>";
						ctn += "<span data-nm='" + cm[1] + "' class='block text-master hint-text fs-12'>Online</span>";
						ctn += "</p>";
						ctn += "</a>";
						ctn += "</li>";
					}
				}
				return ctn;
			};
			$scope.enterMensaje = function () {
				var txtMensaje = document.getElementById("txtMensaje");
				txtMensaje.onkeyup = function (event) {
					var keyCode = ('which' in event) ? event.which : event.keyCode;
					var EvMensaje = event.target;
					if (keyCode == 13 && EvMensaje.value != "") {
						var mensaje = EvMensaje.value;
						var conversation = document.getElementById('my-conversation');
						var ctn = "<div id='divConver' class='message clearfix'>";
						ctn += "<div class='chat-bubble from-me'>" + mensaje + "</div>";
						ctn += "</div>";
						conversation.innerHTML += ctn;
						txtMensaje.value = '';
						var divConver = document.getElementById('divConver');
						//conversation.removeChild(divConver);
						//if (socket != null) {
						//	socket.send(mensaje);
						//	txtMensaje.value = "";
						//	txtMensaje.focus();
						//}
					}
				};
			};
		};

		$scope.configColor = function (data) {
			var parametros = data.split("¬");
			document.getElementById('idNameCompany1').innerHTML = parametros[0];
			document.getElementById('idNameCompany2').innerHTML = parametros[0];
			var reglas = (document.styleSheets[0].rules);
			var nreglas = reglas.length, obj, objCss;

			var colors = [".header-area-principal-bc", ".header-text-name-empresa-c", ".header-text-c", ".menu-header-area-principal-bc",
				".menu-header-name-empresa-c", ".menu-area-bc", ".menu-text-icon-c", ".menu-submenu-area-bc", ".modal-header-area-bc",
				".modal-header-text-c", ".modal-footer-area-bc"];
			var ncolors = colors.length, clase;
			var root = reglas[0];
			for (var j = 0; j < ncolors; j++) {
				clase = colors[j];
				objCss = null;
				for (var i = 0; i < nreglas; i++) {
					obj = reglas[i];
					if (obj.selectorText === clase) {
						objCss = obj;
						if (objCss) {
							switch (clase) {
								case ".header-area-principal-bc":
									objCss.style.setProperty("background-color", parametros[j + 1]);
									root.style.setProperty("--main-header-area-principal-bc", parametros[j + 1]);
									break;
								case ".header-text-name-empresa-c":
									objCss.style.setProperty("color", parametros[j + 1]);
									root.style.setProperty("--main-header-text-name-empresa-c", parametros[j + 1]);
									break;
								case ".header-text-c":
									objCss.style.setProperty("color", parametros[j + 1]);
									root.style.setProperty("--main-header-text-c", parametros[j + 1]);
									break;
								case ".menu-header-area-principal-bc":
									objCss.style.setProperty("background-color", parametros[j + 1]);
									root.style.setProperty("--main-menu-header-area-principal-bc", parametros[j + 1]);
									break;
								case ".menu-header-name-empresa-c":
									objCss.style.setProperty("color", parametros[j + 1]);
									root.style.setProperty("--main-menu-header-name-empresa-c", parametros[j + 1]);
									break;
								case ".menu-area-bc":
									objCss.style.setProperty("background-color", parametros[j + 1]);
									root.style.setProperty("--main-menu-area-bc", parametros[j + 1]);
									break;
								case ".menu-text-icon-c":
									objCss.style.setProperty("color", parametros[j + 1]);
									root.style.setProperty("--main-menu-text-icon-c", parametros[j + 1]);
									break;
								case ".menu-submenu-area-bc":
									objCss.style.setProperty("background-color", parametros[j + 1]);
									root.style.setProperty("--main-menu-submenu-area-bc", parametros[j + 1]);
									break;
								case ".modal-header-area-bc":
									objCss.style.setProperty("background-color", parametros[j + 1]);
									root.style.setProperty("--main-modal-header-area-bc", parametros[j + 1]);
									break;
								case ".modal-header-text-c":
									objCss.style.setProperty("color", parametros[j + 1]);
									root.style.setProperty("--main-modal-header-text-c", parametros[j + 1]);
									break;
								case ".modal-footer-area-bc":
									objCss.style.setProperty("background-color", parametros[j + 1]);
									root.style.setProperty("--main-modal-footer-area-bc", parametros[j + 1]);
									break;
							}
						}
						break;
					}
				}
			}
		};
		$scope.configurarEventos = function () {
			var btn_LA_SalirSistema = document.getElementById('btn_LA_SalirSistema');
			btn_LA_SalirSistema.onclick = function () {
				sessionStorage.clear();
				history.pushState(null, null, module.getRoot());
				module.goPage('Login');
			};

			var btn_LA_Chat = document.getElementById('btn_LA_Chat');
			btn_LA_Chat.onclick = function () {
				var quickview = document.getElementById('quickview');
				if (quickview.classList.contains('open')) {
					quickview.classList.remove('open');
				} else {
					quickview.classList.add('open');
				}
			};
			var btnCerrarChat = document.getElementById('btnCerrarChat');
			btnCerrarChat.onclick = function () {
				var quickview = document.getElementById('quickview');
				quickview.classList.remove('open');
			};

			//var btnRetornar = document.getElementById('btnRetornar');
			//btnRetornar.onclick = function () {
			//	var chat = document.getElementById('chat');
			//	if (chat.classList.contains('push-parrallax')) {
			//		chat.classList.remove('push-parrallax');
			//	} else {
			//		chat.classList.add('push-parrallax');
			//	}
			//};
			var divDashboard = document.getElementById('divDashboard');
			var idPageSidebar = document.getElementById('idPageSidebar');
			function mediaQueries980(a) {
				if (a.matches) {
					divDashboard.classList.remove('menu-pin');
					divDashboard.classList.add('sidebar-visible');
				}
			}
			function mediaQueries1200(b) {
				if (b.matches) {
					divDashboard.classList.add('menu-pin');
					divDashboard.classList.remove('sidebar-visible');
				} else {
					divDashboard.classList.add('sidebar-visible');
					divDashboard.classList.remove('menu-pin');
				}
			}
			var a = window.matchMedia("(min-width: 980px)");
			var b = window.matchMedia("(min-width: 1200px)");
			mediaQueries980(a);
			mediaQueries1200(b);
			a.addListener(mediaQueries980);
			b.addListener(mediaQueries1200);

			var divSiderBarMenu = document.getElementById("divSiderBarMenu");
			divSiderBarMenu.onmouseover = function () {
				if (divDashboard.classList.contains('sidebar-visible')) {
					idPageSidebar.style.transform = "translate(210px, 0px)";
				}

			};
			divSiderBarMenu.onmouseout = function () {
				if (divDashboard.classList.contains('sidebar-visible')) {
					idPageSidebar.style.transform = "translate3d(0px, 0px, 0px)";
				}
			};

			var idMenuBars = document.getElementById('idMenuBars');
			idMenuBars.onclick = function () {
				if (divDashboard.classList.contains('sidebar-open') && idPageSidebar.classList.contains('visible')) {
					divDashboard.classList.remove('sidebar-open');
					idPageSidebar.classList.remove('visible');
				} else {
					divDashboard.classList.add('sidebar-open');
					idPageSidebar.classList.add('visible');
				}
			};

			var idImgUser = document.getElementById('idImgUser');
			idImgUser.onclick = function () {
				var idToolTips = document.getElementById('idToolTips');
				if (idToolTips.classList.contains('show')) {
					idToolTips.classList.remove('show');
				} else {
					idToolTips.classList.add('show');
				}
			};

			var btnSettings = document.getElementById('btnSettings');
			btnSettings.onclick = function () {
				module.goSubPage('Cnf_Settings', 'view-Main');
			};

			var btnPerfil = document.getElementById('btnPerfil');
			btnPerfil.onclick = function () {
				module.goSubPage('Cnf_Perfil', 'view-Main');
			};
			var idSpanName = document.getElementById('idSpanName');
			idSpanName.innerHTML = sessionStorage.getItem('nameUser');
		};
	};
	controller.$inject = ['$scope', '$service'];
	module.controller(namespace, controller, service);
});
