﻿define([$appName,  //module
	"Login" //namespace
], function (module, namespace, args) {
	"use strict";
	var service = function ($service, $http) {
		$service.validarLogin = function (item, callback) {
			return $http({
				method: 'post',
				data: item,
				url: "Seguridad/validadLogin",
				callback: callback
			});
		};
	};
	var controller = function ($scope, $service) {
		$scope.init = function () {
			$scope.configurarControles();
			$scope.cargarDatos();
			$scope.configurarEventos();
		};

		$scope.cargarDatos = function () {
			$scope.fnValidarLogin = function () {
				if ($fnSqx_vLd('js-frml')) {
					var pw = document.getElementById('txt_LO_Contrasena').value;
					var p = $fnConvertStringToHex(pw);
					var resquest = {
						usuario: document.getElementById('txt_LO_Usuario').value,
						contrasena: p
					};
					$service.validarLogin(resquest, function (d) {
						if (d.success) {
							if (d.success !== "" && d.data !== '-1') {
								var dt = d.data.split("¦");
								sessionStorage.setItem("user", dt[0]);
								sessionStorage.setItem($appName, dt[1]);
								sessionStorage.setItem("nameUser", dt[2]);
								sessionStorage.setItem("AppView", '');
								module.goPage('Layout');
								//$alert.show($msg.response.successSession, 'I');
							} else {
								$alert.show($msg.response.errorSession, 'E');
							}
						} else {
							$alert.show($msg.response.errorSession, 'E');
						}
					});
				}
			};
		};

		$scope.configurarControles = function () {
			
		};

		$scope.configurarEventos = function () {
			var btn_LO_Ingresar = document.getElementById('btn_LO_Ingresar');
			if (btn_LO_Ingresar) {
				btn_LO_Ingresar.onclick = function () {
					$scope.fnValidarLogin();
				};
			}
			document.onkeypress = function (e) {
				if (window.event) {
					if (e.keyCode === 13) {
						if (btn_LO_Ingresar !== null) {
							$scope.fnValidarLogin();
						}
					}
					e.stopPropagation();
				}
			};
		};

	};
	controller.$inject = ['$scope', '$service'];
	module.controller(namespace, controller, service);
});