﻿define([$appName,  //module
    "Cliente" //namespace
], function (module, namespace, args) {
    "use strict";
    var service = function ($service, $http) {
        $service.ClienteListar = function (item, callback) {
            return $http({
                method: 'get',
                url: "Proyecto001/Cliente_Listar",
                callback: callback
            });
        };

        $service.ClienteGrabar = function (item, callback) {
            return $http({
                method: 'post',
                data: item,
                url: "Proyecto001/Cliente_Grabar",
                callback: callback
            });
        };
        $service.EliminarGenerico = function (item, callback) {
            return $http({
                method: 'post',
                data: item,
                url: "Seguridad/EliminarGenerico",
                callback: callback
            });
        };
    };
    var controller = function ($scope, $service) {
        var SepListas = "¯", SepFilas = "¬", SepCol = "¦";
        var _idCliente = 0;
        var _IdeliminarMatriz;
        $scope.init = function () {
            $scope.configurarControles();
            $scope.Cliente_Listar();
        };

        $scope.Cliente_Listar = function () {
            $service.ClienteListar(null, function (d) {
                if (d.success) {
                    var listas = (d.data != "" ? d.data.split(SepFilas) : []);
                    $$grid('grd_MC_Cliente').setData(listas);
                }
            });
        }

        $scope.configurarControles = function () {
            $$grid('grd_MC_Cliente').create({
                headers: ['', 'Nombre', 'Dirección', 'Correo', 'Telefono'],
                properties: ['IdCliente', 'Nombre', 'Direccion', 'Correo', 'Telefono'],
                typesData: ['S', 'S', 'S', 'S', 'S'],
                typesFilter: ['', 'I', 'I', 'I', 'I'],
                sortHeader: [false, true, true, true, true],
                showColumns: [false, true, true, true, true],
                widths: [22, 22, 22, 22],
                indexs: [1, 2, 3, 4],
                headerStyle: 'background-color: #f6f6f8; color:#333',
                data: [],
                borderTop: false,
                btnNew: true,
                btnEdit: true,
                btnDelete: true,
                btnDeleteMultiple: false,
                btnRefresh: true,
                btnExportExcel: true,
                filterPosition: 'UP',
                generalFilter: true,
                entriesPage: 15,
                rangePage: 5,
                separator: '¦',
                fnBtnNew: 'fnEvNuevoCliente',
                fnBtnEdit: 'fnEvEModificarCliente',
                fnBtnDelete: 'fnEvDeleteCliente',
                fnBtnRefresh: 'fnEvRefreshCliente',
                fnExtensions: [],
                fnRowEvent: null,
                namespace: namespace
            });

            $$modal('mdl_MC_ClienteNuevoEditar').create({
                WithScrollY: true,
                Width: 'sm',
                Title: '',
                ButtonNames: ['Guardar', 'Cancelar'],
                ButtonClass: ['btn-save-ico', 'btn-cancel-ico'],
                FnEvents: ['fnEvMdlClienteGuardar', 'fnEvMdlClienteCancelar'],
                Namespace: namespace
            });

        };

        $scope.fnEvNuevoCliente = function () {
            $fnSqx_limpiarFrm('js-frmCl');
            _idCliente = 0;
            $$modal('mdl_MC_ClienteNuevoEditar').setTitle('Registrar Cliente');
            $$modal('mdl_MC_ClienteNuevoEditar').show();
        };
        $scope.fnEvMdlClienteGuardar = function () {
            var item = {
                containers: ['frmCabeceraCliente']
            };
            if ($fnValidarForm(item).resultado == 0) {
                var strUse = sessionStorage.getItem($appName) + SepListas;
                var strData = strUse + _idCliente + SepCol + getData('js-frmCl')
                $service.ClienteGrabar(strData, function (d) {
                    if (d.success) {
                        $scope.Cliente_Listar();
                        $alert.show('<i class="fa fa-check"></i> ' + $msg.response.success, 'I');
                        $$modal('mdl_MC_ClienteNuevoEditar').hide();
                    } else {
                        $alert.show('<i class="fa fa-exclamation-triangle"></i> ' + $msg.response.error, 'E');
                    }
                });
            }
        };
        $scope.fnEvMdlClienteCancelar = function () {
            $$modal('mdl_MC_ClienteNuevoEditar').hide();
        };

        $scope.fnEvRefreshCliente = function () {
            $scope.Cliente_Listar();
        }
        $scope.fnEvEModificarCliente = function (row) {
            $fnSqx_limpiarFrm('js-frmCl');
            var inps = document.getElementById("Nombre");
            var inps1 = document.getElementById("Direccion");
            var inps2 = document.getElementById("Correo");
            var inps3 = document.getElementById("Telefono");

            _idCliente = row.IdCliente;

            inps.value = row.Nombre;
            inps1.value = row.Direccion;
            inps2.value = row.Correo;
            inps3.value = row.Telefono;

            $$modal('mdl_MC_ClienteNuevoEditar').setTitle('Editar Cliente');
            $$modal('mdl_MC_ClienteNuevoEditar').show();
        };

    };
    controller.$inject = ['$scope', '$service'];
    module.controller(namespace, controller, service);
});