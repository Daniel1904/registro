﻿define([$appName,  //module
    "Invoice" //namespace
], function (module, namespace, args) {
    "use strict";
    var service = function ($service, $http) {
        $service.InvoiceListas = function (item, callback) {
            return $http({
                method: 'get',
                url: "Proyecto001/Invoice_Listas",
                callback: callback
            });
        };
        $service.InvoiceListar = function (item, callback) {
            return $http({
                method: 'get',
                url: "Proyecto001/Invoice_Listar",
                callback: callback
            });
        };
        $service.InvoiceGrabar = function (item, callback) {
            return $http({
                method: 'post',
                data: item,
                url: "Proyecto001/Invoice_Grabar",
                callback: callback
            });
        };
        $service.InvoiceConsultar = function (item, callback) {
            return $http({
                method: 'post',
                data: item,
                url: "Proyecto001/Invoice_Consultar",
                callback: callback
            });
        };
        $service.IncidenteGenerarPDF = function (item, callback) {
            return $http({
                method: 'post',
                data: item,
                url: "Proyecto001/PDFSeguridadIncidente",
                callback: callback
            });
        };
        $service.EliminarGenerico = function (item, callback) {
            return $http({
                method: 'post',
                data: item,
                url: "Seguridad/EliminarGenerico",
                callback: callback
            });
        };
    };
    var controller = function ($scope, $service) {
        var SepListas = "¯", SepFilas = "¬", SepCol = "¦";
        var _document = document, _idInvoice = 0, _IdeliminarMatriz = 0;
        var listaDetalle = [];

        $scope.Invoice_Listar = function () {
            $service.InvoiceListar(null, function (d) {
                if (d.success) {
                    var listas = (d.data != "" ? d.data.split(SepFilas) : []);
                    $$grid('grd_MC_Invoice').setData(listas);
                }
            });

            var btn_MU_MdlClienteSearch = _document.getElementById('btn_MU_MdlClienteSearch');
            btn_MU_MdlClienteSearch.onclick = function () {
                $$modal('mdl_MC_ClienteNuevoEditar').setTitle('Lista Cliente');
                $$modal('mdl_MC_ClienteNuevoEditar').show();
            };

            var btn_MU_MdlClienteTrash = _document.getElementById('btn_MU_MdlClienteTrash');
            btn_MU_MdlClienteTrash.onclick = function () {
                _document.getElementById('Cliente').value = '';
            };
        };

        $scope.init = function () {
            $scope.configurarControles();
            $scope.Invoice_Listar();
            $scope.obtenerListas();

            vE();
        };

        $scope.configurarControles = function () {
            $$modal('mdl_MC_ClienteNuevoEditar').create({
                WithScrollY: true,
                Width: 'md',
                Title: 'Tabla Cliente',
                ButtonNames: ['Cancelar'],
                ButtonClass: ['btn-cancel-ico'],
                FnEvents: ['fnEvMdlClienteCancelar'],
                Namespace: namespace
            });

            $$modal('mdl_MC_ProductoNuevoEditar').create({
                WithScrollY: true,
                Width: 'md',
                Title: 'Tabla Producto',
                ButtonNames: ['Cancelar'],
                ButtonClass: ['btn-cancel-ico'],
                FnEvents: ['fnEvMdlProductoCancelar'],
                Namespace: namespace
            });

            $$grid('tablaCliente').create({
                headers: ['', 'Nombre', 'Direccion', 'Correo', 'Telefono'],
                properties: ['Idcliente', 'Nombre', 'Direccion', 'Correo', 'Telefono'],
                typesData: ['S', 'S', 'S', 'S', 'S'],
                typesFilter: ['I', 'I', 'I', 'I', 'I'],
                sortHeader: [false, true, true, true, true],
                showColumns: [false, true, true, true, true],
                widths: [22, 22, 22, 22],
                indexs: [1, 2, 3, 4],
                headerStyle: 'background-color: #f6f6f8; color:#333',
                data: [],
                borderTop: false,
                btnNew: false,
                btnEdit: false,
                btnDelete: false,
                btnDeleteMultiple: false,
                btnRefresh: true,
                btnExportExcel: false,
                filterPosition: 'UP',
                generalFilter: false,
                entriesPage: 15,
                rangePage: 5,
                separator: '¦',
                fnBtnNew: null,
                fnBtnEdit: null,
                fnBtnDelete: null,
                fnBtnRefresh: 'fnEvRefreshCliente',
                fnExtensions: [],
                fnRowEvent: 'fnEvBodyCliente',
                namespace: namespace
            });

            $$grid('tablaProducto').create({
                headers: ['', 'Descripcion', 'Precio'],
                properties: ['IdProducto', 'Descripcion', 'Precio'],
                typesData: ['S', 'S', 'S', 'S'],
                typesFilter: ['', 'I', 'I', 'I'],
                sortHeader: [false, true, true],
                showColumns: [false, true, true],
                widths: [45, 45],
                indexs: [1, 2, 3],
                headerStyle: 'background-color: #f6f6f8; color:#333',
                data: [],
                borderTop: false,
                btnNew: false,
                btnEdit: false,
                btnDelete: false,
                btnDeleteMultiple: false,
                btnRefresh: false,
                btnExportExcel: false,
                filterPosition: 'UP',
                generalFilter: false,
                entriesPage: 15,
                rangePage: 5,
                separator: '¦',
                fnBtnNew: null,
                fnBtnEdit: null,
                fnBtnDelete: null,
                fnBtnRefresh: null,
                fnExtensions: [],
                fnRowEvent: 'fnEvBodyProducto',
                namespace: namespace
            });

            $$grid('grd_MC_Invoice').create({
                headers: ['', 'Codigo', 'Cliente', 'Direccion', 'Correo', ''],
                properties: ['IdInvoice', 'Codigo', 'Cliente', 'Direccion', 'Correo', 'descarga'],
                typesData: ['S', 'S', 'S', 'S', 'S', 'E'],
                typesFilter: ['', 'I', 'I', 'I', 'I', ''],
                sortHeader: [false, true, true, true, true, false],
                showColumns: [false, true, true, true, true, true],
                widths: [0, 25, 17.5, 17.5, 27.5, 2.5],
                indexs: [1, 2, 3, 4, 5],
                headerStyle: 'background-color: #f6f6f8; color:#333',
                data: [],
                borderTop: false,
                btnNew: true,
                btnEdit: true,
                btnDelete: true,
                btnDeleteMultiple: false,
                btnRefresh: true,
                btnExportExcel: true,
                filterPosition: 'UP',
                generalFilter: true,
                entriesPage: 15,
                rangePage: 5,
                separator: '¦',
                fnBtnNew: 'fnEvNuevoInvoice',
                fnBtnEdit: 'fnEvEModificarInvoice',
                fnBtnDelete: 'fnEvDeleteInvoice',
                fnBtnRefresh: 'fnEvRefreshInvoice',
                fnExtensions: ['', '', '', '', '', 'fnExDownloadPDF'],
                fnRowEvent: 'fnEvServerDownloadPDF',
                namespace: namespace
            });

            $$grid('grd_MIP_OrdenDetalle').create({
                headers: ["", "Descripcion", "Cant.", "Precio", "Total"],
                properties: ['IdOcompraDet', 'Descripcion', 'Cantidad', 'PrecioU', 'Total'],
                typesData: ['S', 'E', 'E', 'E', 'E', 'E', 'E'],
                sortHeader: [false, false, false, false, false, false, false],
                showColumns: [false, true, true, true, true, true, true],
                widths: [0, 50, 17, 17, 16],
                indexs: [1, 2, 3, 4],
                headerStyle: 'background-color: #2c5a8d; color:#fff',
                data: [],
                borderTop: false,
                btnNew: false,
                btnEdit: false,
                btnDelete: true,
                btnDeleteMultiple: false,
                btnRefresh: false,
                btnExportExcel: false,
                filterPosition: '',
                generalFilter: false,
                entriesPage: 8,
                rangePage: 5,
                separator: '¦',
                fnBtnNew: '',
                fnBtnEdit: '',
                fnBtnDelete: 'fnEvDeleteMedidasCorrectivasAC',
                fnBtnRefresh: '',
                fnExtensions: ['', 'fnFormatoDescripcion', 'fnFormatoCantidad', 'fnFormatoPrecioU', 'fnFormatoTotal'],
                fnRowEvent: '',
                namespace: namespace
            });

            $$modal('mdl_MC_InvoiceNuevoEditar').create({
                WithScrollY: true,
                Width: 'lg',
                Title: '',
                ButtonNames: ['Guardar', 'Cancelar'],
                ButtonClass: ['btn-save-ico', 'btn-cancel-ico'],
                FnEvents: ['fnEvMdlInvoiceGuardar', 'fnEvMdlInvoiceCancelar'],
                Namespace: namespace
            });

            $scope.fnDatePicker();



            var btnAgregar = document.getElementById("btnAgregarDetalleAd");
            btnAgregar.onclick = function () {
                $$modal('mdl_MC_ProductoNuevoEditar').setTitle('Listar Producto');
                $$modal('mdl_MC_ProductoNuevoEditar').show();
            }

            var tbAdministracion = document.getElementById("grd_MIP_OrdenDetalle");
            tbAdministracion.onchange = function (e) {
                var element = e.target || e.srcElement;
                if (element.type == "text" || element.type == "number") {
                    var index = element.parentNode.cellIndex * 1 + 1;
                    var indexRow = element.getAttribute("data-index") * 1;
                    var camposfila = listaDetalle[indexRow].split(SepCol);
                    camposfila[index] = element.value;
                    if (index == 2) {
                        camposfila[4] = (element.value * 1) * (camposfila[3] * 1)
                    }
                    if (index == 3) {
                        camposfila[4] = (element.value * 1) * (camposfila[2] * 1)
                    }

                    listaDetalle[indexRow] = camposfila.join(SepCol);
                    $$grid('grd_MIP_OrdenDetalle').setData(listaDetalle);
                    calcularTotales();
                }
            }




        };

        $scope.fnDatePicker = function () {
            DatePicker({ contenedor: "Fecha01", id: "txt_Fecha01", form: "js-frmCl", requerido: false }).crear();
            DatePicker({ contenedor: "Fecha02", id: "txt_Fecha02", form: "js-frmCl", requerido: false }).crear();
        };


        $scope.obtenerListas = function () {
            $service.InvoiceListas(null, function (d) {
                if (d.success) {
                    var listas = d.data.split(SepListas)
                    var lstCodigo = (listas[0] != "" ? listas[0].split(SepFilas) : []);


                    $$grid('tablaCliente').setData(lstCodigo);

                }
            });
            $service.InvoiceListas(null, function (d) {
                if (d.success) {
                    var listas = d.data.split(SepListas)
                    var lstProducto = (listas[1] != "" ? listas[1].split(SepFilas) : []);


                    $$grid('tablaProducto').setData(lstProducto); s

                }
            });
        };

        $scope.fnExDownloadPDF = function (row) {
            return "<div><span id='iddownload' class='fa fa-cloud-download btn-grid-download hand js-opcion' title='Download PDF' /></span><div>";
        };

        $scope.fnEvNuevoInvoice = function () {
            _idInvoice = 0;
            $fnSqx_limpiarFrm('js-frmCl')
            $$grid('grd_MIP_OrdenDetalle').setData(listaDetalle = []);
            $$modal('mdl_MC_InvoiceNuevoEditar').setTitle('Registrar Invoice');
            $$modal('mdl_MC_InvoiceNuevoEditar').show();
        };
        $scope.fnEvMdlInvoiceGuardar = function () {
            var item = {
                containers: ['frmCabeceraInvoice']
            };
            if ($fnValidarForm(item).resultado == 0) {
                var strUse = sessionStorage.getItem($appName) + SepListas;
                var strData = strUse + _idInvoice + SepCol + getData("js-frmCl") + SepListas + listaDetalle.join(SepFilas);
                $service.InvoiceGrabar(strData, function (d) {
                    if (d.success) {
                        $scope.Invoice_Listar();
                        $alert.show('<i class="fa fa-check"></i> ' + $msg.response.success, 'I');
                        $$modal('mdl_MC_InvoiceNuevoEditar').hide();
                    } else {
                        $alert.show('<i class="fa fa-exclamation-triangle"></i> ' + $msg.response.error, 'E');
                    }
                });
            }
        };
        $scope.fnEvEModificarInvoice = function (row) {
            $fnSqx_limpiarFrm('js-frmCl')
            $$grid('grd_MIP_OrdenDetalle').setData(listaDetalle = []);
            $service.InvoiceConsultar(row.IdInvoice, function (d) {
                if (d.success) {
                    var dataM = d.data.split(SepListas);
                    var data = dataM[0].split(SepCol);
                    var inps = document.getElementsByClassName("js-frmCl");
                    $fnSqx_limpiarFrm('js-frmCl');
                    _idInvoice = data[13]
                    inps[0].value = data[0]
                    inps[1].value = data[1]
                    inps[1].setAttribute("data-v", data[1])
                    inps[2].value = data[2]
                    inps[3].value = data[3]
                    inps[4].value = data[4]
                    inps[5].value = data[5]
                    inps[6].value = data[6]
                    inps[7].value = data[7]
                    inps[8].value = data[8]
                    inps[9].value = data[9]
                    inps[10].value = data[10]
                    inps[11].value = data[11]
                    inps[12].value = data[12]



                    var listaMedidas = (dataM[1] != "" ? dataM[1].split(SepFilas) : []);
                    var nlstDetMedidas = listaMedidas.length;
                    for (var k = 0; k < nlstDetMedidas; k++) {
                        listaDetalle.push(listaMedidas[k]);
                        $$grid('grd_MIP_OrdenDetalle').setData(listaDetalle);
                    }



                    $$modal('mdl_MC_InvoiceNuevoEditar').show();
                }
            });
        };
        $scope.fnEvServerDownloadPDF = function (row) {
            var evt = window.event;
            var spn = evt.target || evt.srcElement;
            if (spn.classList.contains("js-opcion")) {
                var idPdf = row.IdInvoice;
                $service.IncidenteGenerarPDF(idPdf, function (d) {
                    if (d.success && d.data != "") {
                        var archivo = "Reporte" + " " + (row.TipoFormulario == "1" ? "Incidente" : "Accidente") + "-" + row.apellidoNombreMGIA + ".pdf";
                        var cadena = atob(d.data), tipo = "Application/pdf";
                        var byteArray = new Uint8Array(cadena.length);
                        for (var i = 0; i < byteArray.byteLength; i++) {
                            byteArray[i] = cadena.charCodeAt(i);
                        }
                        var blob = new Blob([byteArray], { "type": tipo });
                        var ca = document.createElement("a");
                        ca.download = archivo;
                        ca.href = window.URL.createObjectURL(blob);
                        var body = document.getElementsByTagName("body")[0];
                        body.insertAdjacentElement("beforeend", ca);
                        ca.click();
                        body.removeChild(ca);
                    } else {
                        $alert.show('<i class="fa fa-exclamation-triangle"></i> ' + $msg.response.errorProceso, 'E');
                    }
                });
            }

            if (spn.classList.contains("js-Correo")) {
                _document.getElementById('txt_MGA_MdlEnviarEmail').value = '';
                IdFormulario = row.idMGIA;
                tipoFormulario = row.TipoFormulario;
                $$modal('mdl_MGI_GestorIncidenteEnviarCorreo').show();
            }
        };

        $scope.fnEvMdlInvoiceCancelar = function () {
            $$modal('mdl_MC_InvoiceNuevoEditar').hide();
        };
        $scope.fnEvMdlClienteCancelar = function () {
            $$modal('mdl_MC_ClienteNuevoEditar').hide();
        };
        $scope.fnEvMdlProductoCancelar = function () {
            $$modal('mdl_MC_ProductoNuevoEditar').hide();
        };

        $scope.fnEvRefreshInvoice = function () {
            $scope.Invoice_Listar();
        }
        $scope.fnEvBodyCliente = function (row) {
            var txt_MU_MdlCodigo = _document.getElementById('Cliente');
            var txt_MU_MdlDireccion = _document.getElementById('Direccion');
            var txt_MU_MdlTelefono = _document.getElementById('Telefono');
            var txt_MU_MdlCorreo = _document.getElementById('Correo');

            txt_MU_MdlDireccion.value = row.Direccion;
            txt_MU_MdlTelefono.value = row.Telefono;
            txt_MU_MdlCorreo.value = row.Correo;
            txt_MU_MdlCodigo.setAttribute('data-v', row.Nombre);
            txt_MU_MdlCodigo.value = row.Nombre;
            $$modal('mdl_MC_ClienteNuevoEditar').hide();
        };




        $scope.fnFormatoDescripcion = function (row, index) {
            var html = "<input class='form-control' type='text' value='" + row.Descripcion + "' data-index='" + index + "' />";
            return html;
        };
        $scope.fnFormatoCantidad = function (row, index) {
            var html = "<input class='form-control' type='text'  value='" + row.Cantidad + "' data-index='" + index + "' />";
            return html;
        };
        $scope.fnFormatoPrecioU = function (row, index) {
            var html = "<input class='form-control' type='text'  value='" + row.PrecioU + "' data-index='" + index + "' />";
            return html;
        };
        $scope.fnFormatoTotal = function (row, index) {
            var html = "<input class='form-control' type='text'  value='" + row.Total + "' data-index='" + index + "' />";
            return html;
        };

        $scope.fnEvDeleteMedidasCorrectivasAC = function (row, index) {
            listaDetalle.splice(index, 1);
            $$grid('grd_MIP_OrdenDetalle').setData(listaDetalle);
        };

        $scope.fnEvBodyProducto = function (row) {
            var fila = module.jsonToString({
                'IdOcompraDet': '0',
                'Descripcion': row.Descripcion,
                'Cantidad': '',
                'PrecioU': row.Precio,
                'Total': ''
            });
            listaDetalle.push(fila);
            $$grid('grd_MIP_OrdenDetalle').setData(listaDetalle);
            $$modal('mdl_MC_ProductoNuevoEditar').hide();
        }
        function calcularTotales() {
            var tabla = document.getElementById("tbody_jtse-grid_grd_MIP_OrdenDetalle");
            var nregistro = tabla.rows.length;
            var subtotal1 = 0, igv = 0.12, descuento = 50;
            for (var i = 0; i < nregistro; i++) {
                subtotal1 += (tabla.rows[i].cells[3].childNodes[0].value * 1);
            }
            var subtotal = document.getElementById("subtotal");
            subtotal.value = subtotal1;
            var descuento1 = document.getElementById("descuento");
            descuento1.value = descuento;
            var descuento2 = document.getElementById("subdes");
            descuento2.value = ((subtotal.value * 1) - (descuento1.value * 1))
            var igv1 = document.getElementById("igv");
            igv1.value = igv;
            var igv2 = document.getElementById("totaligv");
            igv2.value = ((descuento2.value * 1) * (igv1.value * 1))
            var total1 = document.getElementById("total");
            total1.value = ((descuento2.value * 1) + (igv2.value * 1));
        }

    };
    controller.$inject = ['$scope', '$service'];
    module.controller(namespace, controller, service);
});