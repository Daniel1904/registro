﻿using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace HARDCODE.Core.Helpers
{
    public class GenerateTextFile<T>
    {
        public static void FromObject(T obj, string filePathSetting = "_LogPath", string fileNameSetting = "_LogName")
        {
            string filePath = String.Format("{0}{1}", AppSettings.Get(filePathSetting), TextFormat.YYMMDD(AppSettings.Get(fileNameSetting), ".txt"));
            PropertyInfo[] propiedades = obj.GetType().GetProperties();
            using (FileStream fs = new FileStream(filePath, FileMode.Append, FileAccess.Write, FileShare.Write))
            {
                using (StreamWriter sw = new StreamWriter(fs, Encoding.Default))
                {
                    foreach (PropertyInfo propiedad in propiedades)
                    {
                        sw.Write(propiedad.Name);
                        sw.Write(" = ");
                        sw.WriteLine(propiedad.GetValue(obj, null) == null ? "" : propiedad.GetValue(obj, null).ToString());
                    }
                    sw.WriteLine(new String('_', 50));
                }
            }
        }

        public static void FromString(string content, string filePathSetting = "_LogPath", string fileNameSetting = "_LogName")
        {
            string filePath = String.Format("{0}{1}", AppSettings.Get(filePathSetting), TextFormat.YYMMDD(AppSettings.Get(fileNameSetting), ".txt"));
            using (FileStream fs = new FileStream(filePath, FileMode.Append, FileAccess.Write, FileShare.Write))
            {
                using (StreamWriter sw = new StreamWriter(fs, Encoding.Default))
                {
                    sw.WriteLine(content);
                    sw.WriteLine(new String('_', 50));
                }
            }
        }
    }
}
