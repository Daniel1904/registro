﻿namespace HARDCODE.Core.Entities
{
    public class StatusResponse
    {
        public string Data { get; set; }
        public bool Success { get; set; }
    }
}