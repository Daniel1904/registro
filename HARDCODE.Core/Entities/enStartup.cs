﻿namespace HARDCODE.Core.Entities
{
	public class enStartup
	{
		public string root { get; set; }
		public string versionApp { get; set; }
		public bool isEncoded { get; set; }
		public bool isDeveloper { get; set; }
		public bool isAD { get; set; }
		public string charLocked { get; set; }
		public int timeoutXHR { get; set; }
		public string others { get; set; }
	}
}
