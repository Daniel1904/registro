﻿namespace HARDCODE.Core.Entities
{
    public class StatusResponseObject
    {
        public object Data { get; set; }
        public bool Success { get; set; }
    }
}